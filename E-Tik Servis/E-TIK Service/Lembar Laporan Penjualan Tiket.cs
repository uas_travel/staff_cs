﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Lembar_Laporan_Penjualan_Tiket : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Lembar_Laporan_Penjualan_Tiket()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Lembar_Laporan_Penjualan_Tiket_Load(object sender, EventArgs e)
        {
            Persiapan();
        }

        void Persiapan()
        {
            dGV1.ColumnCount = 8;
            dGV1.Columns[0].Name = "Kode Laporan";
            dGV1.Columns[1].Name = "Tanggal Laporan";
            dGV1.Columns[2].Name = "Kode Operator";
            dGV1.Columns[3].Name = "Nama Kereta";
            dGV1.Columns[4].Name = "Kota Asal";
            dGV1.Columns[5].Name = "Kota Tujuan";
            dGV1.Columns[6].Name = "Jam Berangkat";
            dGV1.Columns[7].Name = "Jam Tiba";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;
            dGV1.ReadOnly = true;
        }

        string UbahFormatDate(string tanggal)
        {
            return tanggal = DateTime.Parse(tanggal).ToString("yyyy-MM-dd");
        }

        bool cboxhendler = false;

        string vendor, tgl;

        #region Load Data CS //clear
        void LoadDataCSKA()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka, tb_travel_m_lapbeli_ka.tanggal_lapbeli_tiket_ka, tb_travel_m_kereta.nama_kereta, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_ka.jam_berangkat, tb_travel_m_sesi_ka.jam_keluar, tb_travel_r_lapbeli_tiket_ka.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_ka.jumlah_Pembayaran FROM tb_travel_m_lapbeli_ka INNER JOIN tb_travel_r_lapbeli_tiket_ka ON tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka = tb_travel_r_lapbeli_tiket_ka.kode_lapbeli_tiket_ka INNER JOIN tb_travel_m_kereta ON tb_travel_m_kereta.kode_kereta = tb_travel_r_lapbeli_tiket_ka.kode_kereta INNER JOIN tb_travel_m_sesi_ka ON tb_travel_m_sesi_ka.kode_sesi = tb_travel_r_lapbeli_tiket_ka.kode_sesi INNER JOIN tb_travel_m_keberangkatan_ka ON tb_travel_m_keberangkatan_ka.kode_keberangkatan = tb_travel_m_sesi_ka.kode_keberangkatan INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_ka.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_ka.kode_tujuan";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                     dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
        }
        void LoadDataCSKL()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl, tb_travel_m_lapbeli_kl.tanggal_lapbeli_tiket_kl, tb_travel_m_kapal_laut.nama_kl, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_kl.jam_berangkat_kl, tb_travel_m_sesi_kl.jam_keluar_kl, tb_travel_r_lapbeli_tiket_kl.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_kl.jumlah_Pembayaran FROM tb_travel_m_lapbeli_kl INNER JOIN tb_travel_r_lapbeli_tiket_kl ON tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl = tb_travel_r_lapbeli_tiket_kl.kode_lapbeli_tiket_kl INNER JOIN tb_travel_m_kapal_laut ON tb_travel_m_kapal_laut.kode_kl = tb_travel_r_lapbeli_tiket_kl.kode_kl INNER JOIN tb_travel_m_sesi_kl ON tb_travel_m_sesi_kl.kode_sesi_kl = tb_travel_r_lapbeli_tiket_kl.kode_sesi_kl INNER JOIN tb_travel_m_keberangkatan_kl ON tb_travel_m_keberangkatan_kl.kode_keberangkatan_kl = tb_travel_m_sesi_kl.kode_keberangkatan_kl INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_kl.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_kl.kode_tujuan";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                        r[6].ToString(), r[7].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
        }
        void LoadDataCSPSW()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat, tb_travel_m_lapbeli_pesawat.tanggal_lapbeli_tiket_pesawat, tb_travel_m_pesawat.nama_pesawat, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_pesawat.jam_berangkat, tb_travel_m_sesi_pesawat.jam_keluar, tb_travel_r_lapbeli_tiket_pesawat.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_pesawat.jumlah_Pembayaran FROM tb_travel_m_lapbeli_pesawat INNER JOIN tb_travel_r_lapbeli_tiket_pesawat ON tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_lapbeli_tiket_pesawat INNER JOIN tb_travel_m_pesawat ON tb_travel_m_pesawat.kode_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_pesawat INNER JOIN tb_travel_m_sesi_pesawat ON tb_travel_m_sesi_pesawat.kode_sesi_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_sesi_pesawat INNER JOIN tb_travel_m_keberangkatan_pesawat ON tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat = tb_travel_m_sesi_pesawat.kode_keberangkatan_pesawat INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_pesawat.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_pesawat.kode_tujuan";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                        r[6].ToString(), r[7].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Load comboBox Vendor //clear
        void LoadVendorKA()
        {
            cboxhendler = false;
            cBox_vendor.DataSource = null;
            string query = "SELECT kode_kereta, nama_kereta FROM tb_travel_m_kereta";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_vendor.ValueMember = "kode_kereta";
                    cBox_vendor.DisplayMember = "nama_kereta";
                    cBox_vendor.DataSource = dt;
                }

                cBox_vendor.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadVendorKL()
        {
            cboxhendler = false;
            cBox_vendor.DataSource = null;
            string query = "SELECT kode_kl, nama_kl FROM tb_travel_m_kapal_laut";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_vendor.ValueMember = "kode_kl";
                    cBox_vendor.DisplayMember = "nama_kl";
                    cBox_vendor.DataSource = dt;
                }

                cBox_vendor.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadVendorPSW()
        {
            cboxhendler = false;
            cBox_vendor.DataSource = null;
            string query = "SELECT kode_pesawat, nama_pesawat FROM tb_travel_m_pesawat";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_vendor.ValueMember = "kode_pesawat";
                    cBox_vendor.DisplayMember = "nama_pesawat";
                    cBox_vendor.DataSource = dt;
                }

                cBox_vendor.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region Load comboBox Tanggal
        void LoadTanggalKA()
        {
            cBox_filter_tgl.Items.Clear();
            string query = "SELECT tanggal_pembelian FROM tb_travel_r_pembelian_tiket_ka";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_tgl.Items.Add(UbahFormatDate(r[0].ToString()));
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadTanggalKL()
        {
            cBox_filter_tgl.Items.Clear();
            string query = "SELECT tanggal_pembelian FROM tb_travel_r_pembelian_tiket_kapal";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_tgl.Items.Add(UbahFormatDate(r[0].ToString()));
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadTanggalPSW()
        {
            cBox_filter_tgl.Items.Clear();
            string query = "SELECT tanggal_pembelian FROM tb_travel_r_pembelian_tiket_ka";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_tgl.Items.Add(UbahFormatDate(r[0].ToString()));
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Load Data Filter
        #region Kereta Api //clear
        string LoadDataKAVendor(string vendor)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka, tb_travel_m_lapbeli_ka.tanggal_lapbeli_tiket_ka, tb_travel_m_kereta.nama_kereta, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_ka.jam_berangkat, tb_travel_m_sesi_ka.jam_keluar, tb_travel_r_lapbeli_tiket_ka.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_ka.jumlah_Pembayaran FROM tb_travel_m_lapbeli_ka INNER JOIN tb_travel_r_lapbeli_tiket_ka ON tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka = tb_travel_r_lapbeli_tiket_ka.kode_lapbeli_tiket_ka INNER JOIN tb_travel_m_kereta ON tb_travel_m_kereta.kode_kereta = tb_travel_r_lapbeli_tiket_ka.kode_kereta INNER JOIN tb_travel_m_sesi_ka ON tb_travel_m_sesi_ka.kode_sesi = tb_travel_r_lapbeli_tiket_ka.kode_sesi INNER JOIN tb_travel_m_keberangkatan_ka ON tb_travel_m_keberangkatan_ka.kode_keberangkatan = tb_travel_m_sesi_ka.kode_keberangkatan INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_ka.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_ka.kode_tujuan WHERE tb_travel_m_kereta.nama_kereta = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return vendor;
        }
        string LoadDataKATgl(string tgl)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka, tb_travel_m_lapbeli_ka.tanggal_lapbeli_tiket_ka, tb_travel_m_kereta.nama_kereta, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_ka.jam_berangkat, tb_travel_m_sesi_ka.jam_keluar, tb_travel_r_lapbeli_tiket_ka.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_ka.jumlah_Pembayaran FROM tb_travel_m_lapbeli_ka INNER JOIN tb_travel_r_lapbeli_tiket_ka ON tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka = tb_travel_r_lapbeli_tiket_ka.kode_lapbeli_tiket_ka INNER JOIN tb_travel_m_kereta ON tb_travel_m_kereta.kode_kereta = tb_travel_r_lapbeli_tiket_ka.kode_kereta INNER JOIN tb_travel_m_sesi_ka ON tb_travel_m_sesi_ka.kode_sesi = tb_travel_r_lapbeli_tiket_ka.kode_sesi INNER JOIN tb_travel_m_keberangkatan_ka ON tb_travel_m_keberangkatan_ka.kode_keberangkatan = tb_travel_m_sesi_ka.kode_keberangkatan INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_ka.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_ka.kode_tujuan WHERE tb_travel_m_lapbeli_ka.tanggal_lapbeli_tiket_ka = '" + tgl + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return tgl;
        }
        string LoadDataKAVT( string vendor, string tgl)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka, tb_travel_m_lapbeli_ka.tanggal_lapbeli_tiket_ka, tb_travel_m_kereta.nama_kereta, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_ka.jam_berangkat, tb_travel_m_sesi_ka.jam_keluar, tb_travel_r_lapbeli_tiket_ka.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_ka.jumlah_Pembayaran FROM tb_travel_m_lapbeli_ka INNER JOIN tb_travel_r_lapbeli_tiket_ka ON tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka = tb_travel_r_lapbeli_tiket_ka.kode_lapbeli_tiket_ka INNER JOIN tb_travel_m_kereta ON tb_travel_m_kereta.kode_kereta = tb_travel_r_lapbeli_tiket_ka.kode_kereta INNER JOIN tb_travel_m_sesi_ka ON tb_travel_m_sesi_ka.kode_sesi = tb_travel_r_lapbeli_tiket_ka.kode_sesi INNER JOIN tb_travel_m_keberangkatan_ka ON tb_travel_m_keberangkatan_ka.kode_keberangkatan = tb_travel_m_sesi_ka.kode_keberangkatan INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_ka.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_ka.kode_tujuan WHERE tb_travel_m_kereta.nama_kereta = '" + vendor + "' AND tb_travel_m_lapbeli_ka.tanggal_lapbeli_tiket_ka = '" + tgl + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return vendor;
        }
        #endregion
        #region Kapal Laut
        string LoadDataKLVendor(string vendor)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl, tb_travel_m_lapbeli_kl.tanggal_lapbeli_tiket_kl, tb_travel_m_kapal_laut.nama_kl, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_kl.jam_berangkat_kl, tb_travel_m_sesi_kl.jam_keluar_kl, tb_travel_r_lapbeli_tiket_kl.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_kl.jumlah_Pembayaran FROM tb_travel_m_lapbeli_kl INNER JOIN tb_travel_r_lapbeli_tiket_kl ON tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl = tb_travel_r_lapbeli_tiket_kl.kode_lapbeli_tiket_kl INNER JOIN tb_travel_m_kapal_laut ON tb_travel_m_kapal_laut.kode_kl = tb_travel_r_lapbeli_tiket_kl.kode_kl INNER JOIN tb_travel_m_sesi_kl ON tb_travel_m_sesi_kl.kode_sesi_kl = tb_travel_r_lapbeli_tiket_kl.kode_sesi_kl INNER JOIN tb_travel_m_keberangkatan_kl ON tb_travel_m_keberangkatan_kl.kode_keberangkatan_kl = tb_travel_m_sesi_kl.kode_keberangkatan_kl INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_kl.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_kl.kode_tujuan WHERE tb_travel_m_kapal_laut.nama_kl = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return vendor;
        }
        string LoadDataKLTgl(string tgl)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl, tb_travel_m_lapbeli_kl.tanggal_lapbeli_tiket_kl, tb_travel_m_kapal_laut.nama_kl, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_kl.jam_berangkat_kl, tb_travel_m_sesi_kl.jam_keluar_kl, tb_travel_r_lapbeli_tiket_kl.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_kl.jumlah_Pembayaran FROM tb_travel_m_lapbeli_kl INNER JOIN tb_travel_r_lapbeli_tiket_kl ON tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl = tb_travel_r_lapbeli_tiket_kl.kode_lapbeli_tiket_kl INNER JOIN tb_travel_m_kapal_laut ON tb_travel_m_kapal_laut.kode_kl = tb_travel_r_lapbeli_tiket_kl.kode_kl INNER JOIN tb_travel_m_sesi_kl ON tb_travel_m_sesi_kl.kode_sesi_kl = tb_travel_r_lapbeli_tiket_kl.kode_sesi_kl INNER JOIN tb_travel_m_keberangkatan_kl ON tb_travel_m_keberangkatan_kl.kode_keberangkatan_kl = tb_travel_m_sesi_kl.kode_keberangkatan_kl INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_kl.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_kl.kode_tujuan WHERE tb_travel_m_lapbeli_kl.tanggal_lapbeli_tiket_kl = '" + tgl + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return tgl;
        }
        string LoadDataKLTV(string vendor, string tgl)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl, tb_travel_m_lapbeli_kl.tanggal_lapbeli_tiket_kl, tb_travel_m_kapal_laut.nama_kl, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_kl.jam_berangkat_kl, tb_travel_m_sesi_kl.jam_keluar_kl, tb_travel_r_lapbeli_tiket_kl.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_kl.jumlah_Pembayaran FROM tb_travel_m_lapbeli_kl INNER JOIN tb_travel_r_lapbeli_tiket_kl ON tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl = tb_travel_r_lapbeli_tiket_kl.kode_lapbeli_tiket_kl INNER JOIN tb_travel_m_kapal_laut ON tb_travel_m_kapal_laut.kode_kl = tb_travel_r_lapbeli_tiket_kl.kode_kl INNER JOIN tb_travel_m_sesi_kl ON tb_travel_m_sesi_kl.kode_sesi_kl = tb_travel_r_lapbeli_tiket_kl.kode_sesi_kl INNER JOIN tb_travel_m_keberangkatan_kl ON tb_travel_m_keberangkatan_kl.kode_keberangkatan_kl = tb_travel_m_sesi_kl.kode_keberangkatan_kl INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_kl.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_kl.kode_tujuan WHERE  tb_travel_m_kapal_laut.nama_kl = '" + vendor + "' AND tb_travel_m_lapbeli_kl.tanggal_lapbeli_tiket_kl = '" + tgl + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return vendor;
        }
        #endregion
        #region Pesawat Terbang
        string LoadDataPSWVendor(string vendor)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat, tb_travel_m_lapbeli_pesawat.tanggal_lapbeli_tiket_pesawat, tb_travel_m_pesawat.nama_pesawat, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_pesawat.jam_berangkat, tb_travel_m_sesi_pesawat.jam_keluar, tb_travel_r_lapbeli_tiket_pesawat.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_pesawat.jumlah_Pembayaran FROM tb_travel_m_lapbeli_pesawat INNER JOIN tb_travel_r_lapbeli_tiket_pesawat ON tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_lapbeli_tiket_pesawat INNER JOIN tb_travel_m_pesawat ON tb_travel_m_pesawat.kode_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_pesawat INNER JOIN tb_travel_m_sesi_pesawat ON tb_travel_m_sesi_pesawat.kode_sesi_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_sesi_pesawat INNER JOIN tb_travel_m_keberangkatan_pesawat ON tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat = tb_travel_m_sesi_pesawat.kode_keberangkatan_pesawat INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_pesawat.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_pesawat.kode_tujuan WHERE tb_travel_m_pesawat.nama_pesawat = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return vendor;
        }
        string LoadDataPSWTgl(string tgl)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat, tb_travel_m_lapbeli_pesawat.tanggal_lapbeli_tiket_pesawat, tb_travel_m_pesawat.nama_pesawat, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_pesawat.jam_berangkat, tb_travel_m_sesi_pesawat.jam_keluar, tb_travel_r_lapbeli_tiket_pesawat.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_pesawat.jumlah_Pembayaran FROM tb_travel_m_lapbeli_pesawat INNER JOIN tb_travel_r_lapbeli_tiket_pesawat ON tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_lapbeli_tiket_pesawat INNER JOIN tb_travel_m_pesawat ON tb_travel_m_pesawat.kode_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_pesawat INNER JOIN tb_travel_m_sesi_pesawat ON tb_travel_m_sesi_pesawat.kode_sesi_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_sesi_pesawat INNER JOIN tb_travel_m_keberangkatan_pesawat ON tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat = tb_travel_m_sesi_pesawat.kode_keberangkatan_pesawat INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_pesawat.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_pesawat.kode_tujuan WHERE tb_travel_m_lapbeli_pesawat.tanggal_lapbeli_tiket_pesawat = '" + tgl + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return tgl;
        }
        string LoadDataPSWTV(string vendor, string tgl)
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat, tb_travel_m_lapbeli_pesawat.tanggal_lapbeli_tiket_pesawat, tb_travel_m_pesawat.nama_pesawat, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan, tb_travel_m_sesi_pesawat.jam_berangkat, tb_travel_m_sesi_pesawat.jam_keluar, tb_travel_r_lapbeli_tiket_pesawat.jumlah_yang_terjual, tb_travel_r_lapbeli_tiket_pesawat.jumlah_Pembayaran FROM tb_travel_m_lapbeli_pesawat INNER JOIN tb_travel_r_lapbeli_tiket_pesawat ON tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_lapbeli_tiket_pesawat INNER JOIN tb_travel_m_pesawat ON tb_travel_m_pesawat.kode_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_pesawat INNER JOIN tb_travel_m_sesi_pesawat ON tb_travel_m_sesi_pesawat.kode_sesi_pesawat = tb_travel_r_lapbeli_tiket_pesawat.kode_sesi_pesawat INNER JOIN tb_travel_m_keberangkatan_pesawat ON tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat = tb_travel_m_sesi_pesawat.kode_keberangkatan_pesawat INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_pesawat.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_kota_tujuan.kode_tujuan = tb_travel_m_keberangkatan_pesawat.kode_tujuan WHERE tb_travel_m_pesawat.nama_pesawat = '" + vendor + "' AND  tb_travel_m_lapbeli_pesawat.tanggal_lapbeli_tiket_pesawat = '" + tgl + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString(),
                         r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return vendor;
        }
        #endregion
        #endregion

        #region Get Data Vendor + Tanggal
        #region Kereta Api
        void GetVendorKA()
        {
            string query = "SELECT nama_kereta FROM tb_travel_m_kereta WHERE kode_kereta = '"+cBox_vendor.SelectedValue.ToString()+"'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    vendor = r[0].ToString();
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void GetTanggalKA()
        {
            tgl = cBox_filter_tgl.SelectedItem.ToString();
        }
        #endregion
        #region Kapal Laut
        void GetVendorKL()
        {
            string query = "SELECT nama_kl FROM tb_travel_m_kapal_laut WHERE kode_kl = '" + cBox_vendor.SelectedValue.ToString() + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    vendor = r[0].ToString();
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void GetTanggalKL()
        {
            tgl = cBox_filter_tgl.SelectedItem.ToString();
        }
        #endregion
        #region Pesawat Terbang
        void GetVendorPSW()
        {
            string query = "SELECT nama_pesawat FROM tb_travel_m_pesawat WHERE kode_pesawat = '" +cBox_vendor.SelectedValue.ToString()+ "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    vendor = r[0].ToString();
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void GetTanggalPSW()
        {
            tgl = cBox_filter_tgl.SelectedItem.ToString();
        }
        #endregion
        #endregion

        void LoadFilter()
        {
            if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
            {
                if (checkBox_vendor.Checked && checkBox_tanggal.Checked)
                {
                    GetVendorKA(); GetTanggalKA();
                    //MessageBox.Show(vendor + " " + tgl);
                    LoadDataKAVT(vendor,tgl);
                }
                else if (checkBox_tanggal.Checked)
                {
                    GetTanggalKA();
                    //MessageBox.Show(tgl);
                    LoadDataKATgl(tgl);
                }
                else if (checkBox_vendor.Checked)
                {
                    GetVendorKA();
                    //MessageBox.Show(vendor);
                    LoadDataKAVendor(vendor);
                }
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
            {
                if (checkBox_tanggal.Checked && checkBox_vendor.Checked)
                {
                    GetTanggalKL(); GetVendorKL();
                    //MessageBox.Show("tgl + vendor");
                    LoadDataKLTV(vendor,tgl);
                }
                else if (checkBox_tanggal.Checked)
                {
                    GetTanggalKL();
                    //MessageBox.Show("tgl");
                    LoadDataKLTgl(tgl);
                }
                else if (checkBox_vendor.Checked)
                {
                    GetVendorKL();
                    //MessageBox.Show("vendor");
                    LoadDataKLVendor(vendor);
                }
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
            {
                if (checkBox_tanggal.Checked && checkBox_vendor.Checked)
                {
                    GetVendorPSW(); GetTanggalPSW();
                    //MessageBox.Show("tgl + vendor");
                    LoadDataPSWTV(vendor,tgl);
                }
                else if (checkBox_tanggal.Checked)
                {
                    GetTanggalPSW();
                    //MessageBox.Show("tgl");
                    LoadDataPSWTgl(tgl);
                }
                else if (checkBox_vendor.Checked)
                {
                    GetVendorPSW();
                    //MessageBox.Show("vendor");
                    LoadDataPSWVendor(vendor);
                }
            }
        }

        private void cBox_ktegori_tiket_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
            {
                checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
                //cBox_vendor.Items.Clear(); cBox_filter_tgl.Items.Clear();
                LoadDataCSKA();
                LoadVendorKA();
                LoadTanggalKA();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
            {
                checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
                //cBox_vendor.Items.Clear(); cBox_filter_tgl.Items.Clear();
                LoadDataCSKL();
                LoadVendorKL();
                LoadTanggalKL();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
            {
                checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
                //cBox_vendor.Items.Clear(); cBox_filter_tgl.Items.Clear();
                LoadDataCSPSW();
                LoadVendorPSW();
                LoadTanggalPSW();
            }
        }

        private void checkBox_vendor_CheckedChanged(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem == null)
            {
                checkBox_vendor.Checked = false;
            }
            else
            {
                if (checkBox_vendor.Checked)
                {
                    cBox_vendor.Enabled = true;
                }
                else
                {
                    cBox_vendor.Enabled = false;
                    if (cBox_ktegori_tiket.SelectedItem == null)
                    {
                        checkBox_vendor.Checked = false;
                    }
                    else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
                    {
                        //cBox_vendor.Enabled = true;
                        LoadDataCSKA();
                        LoadVendorKA();
                        LoadTanggalKA();
                    }
                    else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
                    {
                        //cBox_vendor.Enabled = true;
                        LoadDataCSKL();
                        LoadVendorKL();
                        LoadTanggalKL();
                    }
                    else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
                    {
                        //cBox_vendor.Enabled = true;
                        LoadDataCSPSW();
                        LoadVendorPSW();
                        LoadTanggalPSW();
                    }
                }
            }
        }

        private void btn_clear_Click()
        {
            checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
        }

        private void checkBox_tanggal_CheckedChanged(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem != null)
            {
                if (checkBox_tanggal.Checked)
                {
                    cBox_filter_tgl.Enabled = true;
                }
                else
                {
                    cBox_filter_tgl.Enabled = false;
                    if (cBox_ktegori_tiket.SelectedItem == null)
                    {
                        checkBox_tanggal.Checked = false;
                    }
                    else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
                    {
                        //cBox_vendor.Enabled = true;
                        LoadDataCSKA();
                        LoadVendorKA();
                        LoadTanggalKA();
                    }
                    else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
                    {
                        //cBox_vendor.Enabled = true;
                        LoadDataCSKL();
                        LoadVendorKL();
                        LoadTanggalKL();
                    }
                    else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
                    {
                        //cBox_vendor.Enabled = true;
                        LoadDataCSPSW();
                        LoadVendorPSW();
                        LoadTanggalPSW();
                    }
                }
            }
            else
            {
                checkBox_tanggal.Checked = false;
            }
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cBox_vendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxhendler)
            {
                LoadFilter();
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
            {
                checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
                LoadDataCSKA();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
            {
                checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
                LoadDataCSKL();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
            {
                checkBox_tanggal.Checked = false; checkBox_vendor.Checked = false;
                LoadDataCSPSW();
            }
        }

        private void cBox_filter_tgl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxhendler)
            {
                LoadFilter();
            }
        }

        private void dGV1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Lembar_Laporan_Penjualan_Tiket_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}
