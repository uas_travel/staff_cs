﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Lembar_Laporan_Upgrade : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Lembar_Laporan_Upgrade()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Lembar_Laporan_Upgrade_Load(object sender, EventArgs e)
        {
            Persiapan();
            LoadData();
            LoadTgl();
            LoadBgUp();
        }

        private void Persiapan()
        {
            dGV1.ColumnCount = 6;
            dGV1.Columns[0].Name = "Nomor Pengajuan";
            dGV1.Columns[1].Name = "Tanggal Pengajuan";
            dGV1.Columns[2].Name = "Kategori";
            dGV1.Columns[3].Name = "Alasan";
            dGV1.Columns[4].Name = "Estimasi";
            dGV1.Columns[5].Name = "Valisasi Pengajuan";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;
            dGV1.ReadOnly = true;

        }
        
        string UbahFormatDate(string tanggal)
        {
            return tanggal = DateTime.Parse(tanggal).ToString("yyyy-MM-dd");
        }

        void LoadData()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lappengajuan.no_pengajuan, tb_travel_m_tgl_lappengajuan.tgl_pengajuan, tb_travel_r_lappengajuan.kode_kateg, tb_travel_r_lappengajuan.alasan, tb_travel_r_lappengajuan.estimasi_harga, tb_travel_m_tgl_lappengajuan.validasi_pengajuan FROM tb_travel_m_tgl_lappengajuan INNER JOIN tb_travel_r_lappengajuan ON tb_travel_m_tgl_lappengajuan.no_pengajuan = tb_travel_r_lappengajuan.no_pengajuan ";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void dGV1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_nolap.Text = dGV1.SelectedRows[0].Cells[0].Value.ToString();
            txt_tgl_lap.Text = dGV1.SelectedRows[0].Cells[1].Value.ToString();

            string query = "SELECT nama_kateg FROM tb_travel_m_kategpengajuan WHERE kode_kateg = '"+ dGV1.SelectedRows[0].Cells[2].Value.ToString() +"'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                DataTable dt = new DataTable();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_kateg.Text = r[0].ToString();
                }
                
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            txt_alasam.Text = dGV1.SelectedRows[0].Cells[3].Value.ToString();
            txt_estimasi.Text = dGV1.SelectedRows[0].Cells[4].Value.ToString();
            txt_validsi.Text = dGV1.SelectedRows[0].Cells[5].Value.ToString();
        }

        private void checkBox_tanggal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_tanggal.Checked)
            {
                cBox_filter_tgl.Enabled = true;
            }
            else
            {
                cBox_filter_tgl.Enabled = false;
                LoadData();
            }
        }

        private void checkBox_bagian_upgrade_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_bagian_upgrade.Checked)
            {
                cBox_bagian_upgrade.Enabled = true;
            }
            else
            {
                cBox_bagian_upgrade.Enabled = false;
                LoadData();
            }
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cBox_filter_tgl_SelectedIndexChanged(object sender, EventArgs e)
        {
            tgl = cBox_filter_tgl.SelectedItem.ToString();
            FilterData();
        }

        private void cBox_bagian_upgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxhendler)
            {
                bgup = cBox_bagian_upgrade.SelectedValue.ToString();
                FilterData();
            }
        }

        #region Load cBox
        void LoadTgl()
        {
            string query = "SELECT tgl_pengajuan FROM tb_travel_m_tgl_lappengajuan";
            cmd = new MySqlCommand(query,kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_tgl.Items.Add(UbahFormatDate(r[0].ToString()));
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        bool cboxhendler = false;
        void LoadBgUp()
        {
            cboxhendler = false;
            string query = "SELECT kode_kateg, nama_kateg FROM tb_travel_m_kategpengajuan";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                DataTable dt = new DataTable();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_bagian_upgrade.ValueMember = "kode_kateg";
                    cBox_bagian_upgrade.DisplayMember = "nama_kateg";
                    cBox_bagian_upgrade.DataSource = dt;
                }

                cBox_bagian_upgrade.Text = null;
                cboxhendler = true;

                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        void FilterData()
        {
            if (checkBox_bagian_upgrade.Checked && checkBox_tanggal.Checked)
            {
                LoadDataTB();
            }
            else if (checkBox_tanggal.Checked)
            {
                LoadDataTgl();
            }
            else if (checkBox_bagian_upgrade.Checked)
            {
                LoadDataBgUp();
            }
        }

        #region Load Data Filter
        string tgl;
        void LoadDataTgl()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lappengajuan.no_pengajuan, tb_travel_m_tgl_lappengajuan.tgl_pengajuan, tb_travel_r_lappengajuan.kode_kateg, tb_travel_r_lappengajuan.alasan, tb_travel_r_lappengajuan.estimasi_harga, tb_travel_m_tgl_lappengajuan.validasi_pengajuan FROM tb_travel_m_tgl_lappengajuan INNER JOIN tb_travel_r_lappengajuan ON tb_travel_m_tgl_lappengajuan.no_pengajuan = tb_travel_r_lappengajuan.no_pengajuan WHERE tb_travel_m_tgl_lappengajuan.tgl_pengajuan = '"+ tgl +"'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        string bgup;
        void LoadDataBgUp()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lappengajuan.no_pengajuan, tb_travel_m_tgl_lappengajuan.tgl_pengajuan, tb_travel_r_lappengajuan.kode_kateg, tb_travel_r_lappengajuan.alasan, tb_travel_r_lappengajuan.estimasi_harga, tb_travel_m_tgl_lappengajuan.validasi_pengajuan FROM tb_travel_m_tgl_lappengajuan INNER JOIN tb_travel_r_lappengajuan ON tb_travel_m_tgl_lappengajuan.no_pengajuan = tb_travel_r_lappengajuan.no_pengajuan WHERE tb_travel_r_lappengajuan.kode_kateg = '"+ bgup +"'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadDataTB()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lappengajuan.no_pengajuan, tb_travel_m_tgl_lappengajuan.tgl_pengajuan, tb_travel_r_lappengajuan.kode_kateg, tb_travel_r_lappengajuan.alasan, tb_travel_r_lappengajuan.estimasi_harga, tb_travel_m_tgl_lappengajuan.validasi_pengajuan FROM tb_travel_m_tgl_lappengajuan INNER JOIN tb_travel_r_lappengajuan ON tb_travel_m_tgl_lappengajuan.no_pengajuan = tb_travel_r_lappengajuan.no_pengajuan WHERE tb_travel_m_tgl_lappengajuan.tgl_pengajuan = '"+ tgl +"' AND tb_travel_r_lappengajuan.kode_kateg = '" + bgup + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        #endregion

        private void btn_clear_Click(object sender, EventArgs e)
        {
            checkBox_tanggal.Checked = false; checkBox_bagian_upgrade.Checked = false;
            LoadData();
        }

        private void Lembar_Laporan_Upgrade_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}
