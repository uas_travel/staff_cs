﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Laporan_Absensi_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Laporan_Absensi_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Laporan_Absensi_Karyawan_Load(object sender, EventArgs e)
        {
            Persiapan();
            //LoadData();
        }

        private void Laporan_Absensi_Karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        void Persiapan()
        {
            dGV1.ColumnCount = 6;
            dGV1.Columns[0].Name = "Tanggal Absen";
            dGV1.Columns[1].Name = "NIP";
            dGV1.Columns[2].Name = "Jam Masuk";
            dGV1.Columns[3].Name = "Validasi Masuk";
            dGV1.Columns[4].Name = "Jam Keluar";
            dGV1.Columns[5].Name = "Validasi Keluar";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;
            dGV1.ReadOnly = true;
        }

        void LoadData()
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_m_absensi";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(),r[1].ToString(),r[2].ToString(),r[3].ToString(),r[4].ToString(),r[5].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void LoadDataTgl()
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_m_absensi WHERE tgl_absen = '"+ dTP_tgl_absen.Value.Date.ToString("yyyy-MM-dd") +"'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }   
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string InsertMaster(string no, string tgl, string val)
        {
            string query = "INSERT INTO tb_travel_m_tgl_lapabsen (no_lap_absen, tgl_lap_absen, validasi_lapabsen) VALUES (@no_lap,@tgl_lap,@validasi)";
            cmd = new MySqlCommand(query, kon);
            cmd.Parameters.AddWithValue("@no_lap",no);
            cmd.Parameters.AddWithValue("@tgl_lap",tgl);
            cmd.Parameters.AddWithValue("@validasi",val);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Master Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return no;
        }

        string InsertRelation(string no, string tgl)
        {
            string query = "INSERT INTO tb_travel_r_lapabsen (no_lap_absen, NIP, jam_msk, jam_keluar, validasi_msk, validasi_keluar) SELECT  tb_travel_m_tgl_lapabsen.no_lap_absen, tb_travel_m_absensi.NIP, tb_travel_m_absensi.jam_masuk, tb_travel_m_absensi.jam_keluar, tb_travel_m_absensi.jam_keluar, tb_travel_m_absensi.validasi_keluar FROM tb_travel_m_tgl_lapabsen JOIN tb_travel_m_absensi WHERE no_lap_absen = '" + no + "' AND tgl_absen = '" + tgl +"'";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() < 0)
                {
                    MessageBox.Show("Relation Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return no;
        }

        private void dTP_tgl_absen_ValueChanged(object sender, EventArgs e)
        {
            LoadDataTgl();
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_simpan_Click(object sender, EventArgs e)
        {
            InsertMaster(txt_no_laporan.Text, dTP_tgl_laporan.Value.Date.ToString("yyyy-MM-dd"),txt_operator.Text);
            InsertRelation(txt_no_laporan.Text, dTP_tgl_absen.Value.Date.ToString("yyyy-MM-dd"));
        }
    }
}
