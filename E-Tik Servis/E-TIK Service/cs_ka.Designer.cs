﻿namespace E_TIK_Service
{
    partial class cs_ka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_kodepembelian = new System.Windows.Forms.TextBox();
            this.txt_namapenumpang = new System.Windows.Forms.TextBox();
            this.txt_harga = new System.Windows.Forms.TextBox();
            this.btn_beli = new System.Windows.Forms.Button();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.cBox_kd_kereta = new System.Windows.Forms.ComboBox();
            this.radioButton_Ekonomi = new System.Windows.Forms.RadioButton();
            this.radioButton_Eksekutif = new System.Windows.Forms.RadioButton();
            this.cBox_kd_sesi = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_bangku = new System.Windows.Forms.TextBox();
            this.cBox_kd_kebrangkatan = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dTP_tgl_pembelian = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox_samapai = new System.Windows.Forms.TextBox();
            this.textBox_berangkat = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtbox_tujuan = new System.Windows.Forms.TextBox();
            this.txtbox_asal = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nama Penumpang";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Kode Pembelian";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kode Kereta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Kode Jenis";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nomor Kursi";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 331);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Sesi: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 422);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "Tanggal Pembelian";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 456);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 18);
            this.label9.TabIndex = 8;
            this.label9.Text = "Harga";
            // 
            // txt_kodepembelian
            // 
            this.txt_kodepembelian.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kodepembelian.Location = new System.Drawing.Point(191, 53);
            this.txt_kodepembelian.Name = "txt_kodepembelian";
            this.txt_kodepembelian.Size = new System.Drawing.Size(198, 26);
            this.txt_kodepembelian.TabIndex = 0;
            // 
            // txt_namapenumpang
            // 
            this.txt_namapenumpang.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_namapenumpang.Location = new System.Drawing.Point(191, 94);
            this.txt_namapenumpang.Name = "txt_namapenumpang";
            this.txt_namapenumpang.Size = new System.Drawing.Size(198, 26);
            this.txt_namapenumpang.TabIndex = 1;
            // 
            // txt_harga
            // 
            this.txt_harga.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_harga.Location = new System.Drawing.Point(191, 452);
            this.txt_harga.Name = "txt_harga";
            this.txt_harga.Size = new System.Drawing.Size(198, 26);
            this.txt_harga.TabIndex = 8;
            // 
            // btn_beli
            // 
            this.btn_beli.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_beli.Location = new System.Drawing.Point(233, 483);
            this.btn_beli.Name = "btn_beli";
            this.btn_beli.Size = new System.Drawing.Size(75, 34);
            this.btn_beli.TabIndex = 9;
            this.btn_beli.Text = "Beli";
            this.btn_beli.UseVisualStyleBackColor = true;
            this.btn_beli.Click += new System.EventHandler(this.btn_beli_Click);
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(314, 483);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 34);
            this.btn_tutup.TabIndex = 10;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // cBox_kd_kereta
            // 
            this.cBox_kd_kereta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_kereta.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_kereta.FormattingEnabled = true;
            this.cBox_kd_kereta.Location = new System.Drawing.Point(191, 133);
            this.cBox_kd_kereta.Name = "cBox_kd_kereta";
            this.cBox_kd_kereta.Size = new System.Drawing.Size(198, 26);
            this.cBox_kd_kereta.TabIndex = 2;
            // 
            // radioButton_Ekonomi
            // 
            this.radioButton_Ekonomi.AutoSize = true;
            this.radioButton_Ekonomi.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Ekonomi.Location = new System.Drawing.Point(191, 172);
            this.radioButton_Ekonomi.Name = "radioButton_Ekonomi";
            this.radioButton_Ekonomi.Size = new System.Drawing.Size(80, 22);
            this.radioButton_Ekonomi.TabIndex = 3;
            this.radioButton_Ekonomi.TabStop = true;
            this.radioButton_Ekonomi.Text = "Ekonomi";
            this.radioButton_Ekonomi.UseVisualStyleBackColor = true;
            this.radioButton_Ekonomi.CheckedChanged += new System.EventHandler(this.radioButton_Ekonomi_CheckedChanged);
            // 
            // radioButton_Eksekutif
            // 
            this.radioButton_Eksekutif.AutoSize = true;
            this.radioButton_Eksekutif.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Eksekutif.Location = new System.Drawing.Point(304, 172);
            this.radioButton_Eksekutif.Name = "radioButton_Eksekutif";
            this.radioButton_Eksekutif.Size = new System.Drawing.Size(83, 22);
            this.radioButton_Eksekutif.TabIndex = 4;
            this.radioButton_Eksekutif.TabStop = true;
            this.radioButton_Eksekutif.Text = "Eksekutif";
            this.radioButton_Eksekutif.UseVisualStyleBackColor = true;
            this.radioButton_Eksekutif.CheckedChanged += new System.EventHandler(this.radioButton_Eksekutif_CheckedChanged);
            // 
            // cBox_kd_sesi
            // 
            this.cBox_kd_sesi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_sesi.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_sesi.FormattingEnabled = true;
            this.cBox_kd_sesi.Location = new System.Drawing.Point(191, 327);
            this.cBox_kd_sesi.Name = "cBox_kd_sesi";
            this.cBox_kd_sesi.Size = new System.Drawing.Size(198, 26);
            this.cBox_kd_sesi.TabIndex = 6;
            this.cBox_kd_sesi.SelectedIndexChanged += new System.EventHandler(this.cBox_kd_sesi_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(387, 39);
            this.label10.TabIndex = 26;
            this.label10.Text = "Pemesanan Tiket Kereta Api";
            // 
            // txt_bangku
            // 
            this.txt_bangku.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_bangku.Location = new System.Drawing.Point(191, 200);
            this.txt_bangku.Name = "txt_bangku";
            this.txt_bangku.Size = new System.Drawing.Size(198, 26);
            this.txt_bangku.TabIndex = 4;
            // 
            // cBox_kd_kebrangkatan
            // 
            this.cBox_kd_kebrangkatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_kebrangkatan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_kebrangkatan.FormattingEnabled = true;
            this.cBox_kd_kebrangkatan.Location = new System.Drawing.Point(191, 240);
            this.cBox_kd_kebrangkatan.Name = "cBox_kd_kebrangkatan";
            this.cBox_kd_kebrangkatan.Size = new System.Drawing.Size(198, 26);
            this.cBox_kd_kebrangkatan.TabIndex = 5;
            this.cBox_kd_kebrangkatan.SelectedIndexChanged += new System.EventHandler(this.cBox_kd_kebrangkatan_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 18);
            this.label6.TabIndex = 28;
            this.label6.Text = "Keberangkatan: ";
            // 
            // dTP_tgl_pembelian
            // 
            this.dTP_tgl_pembelian.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_pembelian.Location = new System.Drawing.Point(191, 420);
            this.dTP_tgl_pembelian.Name = "dTP_tgl_pembelian";
            this.dTP_tgl_pembelian.Size = new System.Drawing.Size(200, 26);
            this.dTP_tgl_pembelian.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(189, 389);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 18);
            this.label13.TabIndex = 79;
            this.label13.Text = "Samapai: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(189, 363);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 18);
            this.label14.TabIndex = 78;
            this.label14.Text = "Berangkat:";
            // 
            // textBox_samapai
            // 
            this.textBox_samapai.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_samapai.Location = new System.Drawing.Point(293, 386);
            this.textBox_samapai.Name = "textBox_samapai";
            this.textBox_samapai.ReadOnly = true;
            this.textBox_samapai.Size = new System.Drawing.Size(96, 24);
            this.textBox_samapai.TabIndex = 11;
            // 
            // textBox_berangkat
            // 
            this.textBox_berangkat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_berangkat.Location = new System.Drawing.Point(293, 359);
            this.textBox_berangkat.Name = "textBox_berangkat";
            this.textBox_berangkat.ReadOnly = true;
            this.textBox_berangkat.Size = new System.Drawing.Size(96, 24);
            this.textBox_berangkat.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(191, 301);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 18);
            this.label12.TabIndex = 83;
            this.label12.Text = "Tujuan:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(191, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 18);
            this.label11.TabIndex = 82;
            this.label11.Text = "Asal:";
            // 
            // txtbox_tujuan
            // 
            this.txtbox_tujuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_tujuan.Location = new System.Drawing.Point(291, 298);
            this.txtbox_tujuan.Name = "txtbox_tujuan";
            this.txtbox_tujuan.ReadOnly = true;
            this.txtbox_tujuan.Size = new System.Drawing.Size(96, 24);
            this.txtbox_tujuan.TabIndex = 8;
            // 
            // txtbox_asal
            // 
            this.txtbox_asal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_asal.Location = new System.Drawing.Point(291, 272);
            this.txtbox_asal.Name = "txtbox_asal";
            this.txtbox_asal.ReadOnly = true;
            this.txtbox_asal.Size = new System.Drawing.Size(96, 24);
            this.txtbox_asal.TabIndex = 7;
            // 
            // cs_ka
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(404, 559);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtbox_tujuan);
            this.Controls.Add(this.txtbox_asal);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBox_samapai);
            this.Controls.Add(this.textBox_berangkat);
            this.Controls.Add(this.dTP_tgl_pembelian);
            this.Controls.Add(this.cBox_kd_kebrangkatan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_bangku);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cBox_kd_sesi);
            this.Controls.Add(this.radioButton_Eksekutif);
            this.Controls.Add(this.radioButton_Ekonomi);
            this.Controls.Add(this.cBox_kd_kereta);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.btn_beli);
            this.Controls.Add(this.txt_harga);
            this.Controls.Add(this.txt_namapenumpang);
            this.Controls.Add(this.txt_kodepembelian);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "cs_ka";
            this.Text = "Pembelian Tiket Kereeta Api";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.cs_ka_FormClosing);
            this.Load += new System.EventHandler(this.cs_ka_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_kodepembelian;
        private System.Windows.Forms.TextBox txt_namapenumpang;
        private System.Windows.Forms.TextBox txt_harga;
        private System.Windows.Forms.Button btn_beli;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.ComboBox cBox_kd_kereta;
        private System.Windows.Forms.RadioButton radioButton_Ekonomi;
        private System.Windows.Forms.RadioButton radioButton_Eksekutif;
        private System.Windows.Forms.ComboBox cBox_kd_sesi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_bangku;
        private System.Windows.Forms.ComboBox cBox_kd_kebrangkatan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dTP_tgl_pembelian;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox_samapai;
        private System.Windows.Forms.TextBox textBox_berangkat;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtbox_tujuan;
        private System.Windows.Forms.TextBox txtbox_asal;
    }
}