﻿namespace E_TIK_Service
{
    partial class Laporan_Kinerja_Karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton_Baik = new System.Windows.Forms.RadioButton();
            this.radioButton_Sedang = new System.Windows.Forms.RadioButton();
            this.radioButton_Buruk = new System.Windows.Forms.RadioButton();
            this.btn_simpan = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.dTP_tgl_laporan = new System.Windows.Forms.DateTimePicker();
            this.txt_no_laporan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_nip_operator = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cBox_nip_karyawan = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_nama_karyawan = new System.Windows.Forms.TextBox();
            this.txt_kode_jawbatan = new System.Windows.Forms.TextBox();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tanggal Laporan:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kode Jabatan:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tingkat Kinerja:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nama Karyawan:";
            // 
            // radioButton_Baik
            // 
            this.radioButton_Baik.AutoSize = true;
            this.radioButton_Baik.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Baik.Location = new System.Drawing.Point(162, 252);
            this.radioButton_Baik.Name = "radioButton_Baik";
            this.radioButton_Baik.Size = new System.Drawing.Size(59, 22);
            this.radioButton_Baik.TabIndex = 5;
            this.radioButton_Baik.TabStop = true;
            this.radioButton_Baik.Text = "Baik";
            this.radioButton_Baik.UseVisualStyleBackColor = true;
            // 
            // radioButton_Sedang
            // 
            this.radioButton_Sedang.AutoSize = true;
            this.radioButton_Sedang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Sedang.Location = new System.Drawing.Point(227, 252);
            this.radioButton_Sedang.Name = "radioButton_Sedang";
            this.radioButton_Sedang.Size = new System.Drawing.Size(82, 22);
            this.radioButton_Sedang.TabIndex = 10;
            this.radioButton_Sedang.TabStop = true;
            this.radioButton_Sedang.Text = "Sedang";
            this.radioButton_Sedang.UseVisualStyleBackColor = true;
            // 
            // radioButton_Buruk
            // 
            this.radioButton_Buruk.AutoSize = true;
            this.radioButton_Buruk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Buruk.Location = new System.Drawing.Point(315, 252);
            this.radioButton_Buruk.Name = "radioButton_Buruk";
            this.radioButton_Buruk.Size = new System.Drawing.Size(70, 22);
            this.radioButton_Buruk.TabIndex = 11;
            this.radioButton_Buruk.TabStop = true;
            this.radioButton_Buruk.Text = "Buruk";
            this.radioButton_Buruk.UseVisualStyleBackColor = true;
            // 
            // btn_simpan
            // 
            this.btn_simpan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_simpan.Location = new System.Drawing.Point(215, 333);
            this.btn_simpan.Name = "btn_simpan";
            this.btn_simpan.Size = new System.Drawing.Size(82, 30);
            this.btn_simpan.TabIndex = 7;
            this.btn_simpan.Text = "Simpan";
            this.btn_simpan.UseVisualStyleBackColor = true;
            this.btn_simpan.Click += new System.EventHandler(this.btn_simpan_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(365, 39);
            this.label6.TabIndex = 13;
            this.label6.Text = "Laporan Kinerja Karyawan";
            // 
            // dTP_tgl_laporan
            // 
            this.dTP_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_laporan.Location = new System.Drawing.Point(162, 101);
            this.dTP_tgl_laporan.Name = "dTP_tgl_laporan";
            this.dTP_tgl_laporan.Size = new System.Drawing.Size(223, 24);
            this.dTP_tgl_laporan.TabIndex = 1;
            // 
            // txt_no_laporan
            // 
            this.txt_no_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_no_laporan.Location = new System.Drawing.Point(162, 66);
            this.txt_no_laporan.Name = "txt_no_laporan";
            this.txt_no_laporan.Size = new System.Drawing.Size(223, 24);
            this.txt_no_laporan.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Nomor Laporan:";
            // 
            // txt_nip_operator
            // 
            this.txt_nip_operator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nip_operator.Location = new System.Drawing.Point(162, 288);
            this.txt_nip_operator.Name = "txt_nip_operator";
            this.txt_nip_operator.Size = new System.Drawing.Size(223, 24);
            this.txt_nip_operator.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 291);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 18);
            this.label7.TabIndex = 17;
            this.label7.Text = "NIP Operator:";
            // 
            // cBox_nip_karyawan
            // 
            this.cBox_nip_karyawan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_nip_karyawan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_nip_karyawan.FormattingEnabled = true;
            this.cBox_nip_karyawan.Location = new System.Drawing.Point(162, 140);
            this.cBox_nip_karyawan.Name = "cBox_nip_karyawan";
            this.cBox_nip_karyawan.Size = new System.Drawing.Size(223, 26);
            this.cBox_nip_karyawan.TabIndex = 2;
            this.cBox_nip_karyawan.SelectedIndexChanged += new System.EventHandler(this.cBox_nip_karyawan_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "NIP Karyawan:";
            // 
            // txt_nama_karyawan
            // 
            this.txt_nama_karyawan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nama_karyawan.Location = new System.Drawing.Point(162, 214);
            this.txt_nama_karyawan.Name = "txt_nama_karyawan";
            this.txt_nama_karyawan.Size = new System.Drawing.Size(223, 24);
            this.txt_nama_karyawan.TabIndex = 4;
            // 
            // txt_kode_jawbatan
            // 
            this.txt_kode_jawbatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_jawbatan.Location = new System.Drawing.Point(162, 177);
            this.txt_kode_jawbatan.Name = "txt_kode_jawbatan";
            this.txt_kode_jawbatan.Size = new System.Drawing.Size(223, 24);
            this.txt_kode_jawbatan.TabIndex = 3;
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(303, 333);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(82, 30);
            this.btn_tutup.TabIndex = 8;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // Laporan_Kinerja_Karyawan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(407, 380);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.txt_kode_jawbatan);
            this.Controls.Add(this.txt_nama_karyawan);
            this.Controls.Add(this.cBox_nip_karyawan);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_nip_operator);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_no_laporan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dTP_tgl_laporan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_simpan);
            this.Controls.Add(this.radioButton_Buruk);
            this.Controls.Add(this.radioButton_Sedang);
            this.Controls.Add(this.radioButton_Baik);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Laporan_Kinerja_Karyawan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Laporan Kinerja Karyawan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Laporan_Kinerja_Karyawan_FormClosing);
            this.Load += new System.EventHandler(this.Laporan_Kinerja_Karyawan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButton_Baik;
        private System.Windows.Forms.RadioButton radioButton_Sedang;
        private System.Windows.Forms.RadioButton radioButton_Buruk;
        private System.Windows.Forms.Button btn_simpan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dTP_tgl_laporan;
        private System.Windows.Forms.TextBox txt_no_laporan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_nip_operator;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cBox_nip_karyawan;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_nama_karyawan;
        private System.Windows.Forms.TextBox txt_kode_jawbatan;
        private System.Windows.Forms.Button btn_tutup;
    }
}