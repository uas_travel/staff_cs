﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class laporankerusakan : Form
    {
        public static MySqlConnection kon;
        public static MySqlCommand cmd;
        public static MySqlDataAdapter adapter;

        public laporankerusakan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void laporankerusakan_Load(object sender, EventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
            LoadKategori();
            
        }

        bool cboxHandler;
        private void LoadKategori()
        {
            cboxHandler = false;

            string query = "SELECT kode_kateg, nama_kateg FROM tb_travel_m_kategpengajuan";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_bagian_kerusakan.DisplayMember = "nama_kateg";
                    cBox_bagian_kerusakan.ValueMember = "kode_kateg";

                    cBox_bagian_kerusakan.DataSource = dt;
                }
                
                cBox_bagian_kerusakan.Text = null;
                cboxHandler = true;

                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void LoadAset()
        {
            cboxHandler = false;
            string kategori = cBox_bagian_kerusakan.SelectedValue.ToString();
            string query = "SELECT kode_aset, nama_aset FROM tb_travel_m_aset WHERE kode_kateg = '"+kategori+"'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_nama_aset.ValueMember = "kode_aset";
                    cBox_nama_aset.DisplayMember = "nama_aset";

                    cBox_nama_aset.DataSource = dt;
                }

                cBox_nama_aset.Text = null;
                cboxHandler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InsertMaster(string nolap, string tgl, string validasi)
        {
            string query = "INSERT INTO tb_travel_m_tgl_laprusak (no_laprusak, tgl_laprusak, validasi_laprusak) VALUES (@nolap,@tgl, @validasi)";
            cmd = new MySqlCommand(query,kon);
            cmd.Parameters.AddWithValue("@nolap",nolap);
            cmd.Parameters.AddWithValue("@tgl",tgl);
            cmd.Parameters.AddWithValue("@validasi",validasi);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted!");
                }
                
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
        
        }

        private void InsertRelasi(string nolap, string kd_aset, string tgl_mulai, string tgl_sesudah, string jenis_rusak,string status)
        {
            string query = "INSERT INTO tb_travel_r_laprusak (no_laprusak, kode_aset, tgl_mulai_perbaiki, tgl_selesai_perbaikan, jenis_kerusakan, status_sesudah) VALUES (@nolap,@kd_aset,@tgl_mulai,@tgl_sudah,@jenis,@status)";
            cmd = new MySqlCommand(query,kon);
            cmd.Parameters.AddWithValue("@nolap", nolap);
            cmd.Parameters.AddWithValue("@kd_aset", kd_aset);
            cmd.Parameters.AddWithValue("@tgl_mulai", tgl_mulai);
            cmd.Parameters.AddWithValue("@tgl_sudah", tgl_sesudah);
            cmd.Parameters.AddWithValue("@jenis", jenis_rusak);
            cmd.Parameters.AddWithValue("@status", status);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted!");
                }
                kon.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_simpan_Click(object sender, EventArgs e)
        {
            InsertMaster(txt_no_laporan.Text,dTP_tgl_laporan.Value.Date.ToString("yyyy-MM-dd"),txt_nip_operator.Text);
            InsertRelasi(txt_no_laporan.Text, cBox_nama_aset.SelectedValue.ToString().Substring(0,5),dTP_mulai_perbaikan.Value.Date.ToString("yyyy-MM-dd"),dTP_selsesai_perbaikan.Value.Date.ToString("yyyy-MM-dd"), cBox_jenis_kerusakan.SelectedItem.ToString(),txt_stauts_perbaikan.Text);
        }

        private void cBox_bagian_kerusakan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxHandler)
            {
                cBox_nama_aset.Enabled = true;
                LoadAset();
            }
        }

        private void cBox_jenis_kerusakan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(cBox_jenis_kerusakan.SelectedItem.ToString());
        }
    }
}
