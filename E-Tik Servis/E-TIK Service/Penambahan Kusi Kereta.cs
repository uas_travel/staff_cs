﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E_TIK_Service
{
    public partial class Penambahan_Kusi_Kereta : Form
    {
        public Penambahan_Kusi_Kereta()
        {
            InitializeComponent();
        }

        private void Penambahan_Kusi_Kereta_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}
