﻿namespace E_TIK_Service
{
    partial class Absensi_Karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txt_kode_posisi = new System.Windows.Forms.TextBox();
            this.lbl_clock = new System.Windows.Forms.Label();
            this.clock1 = new System.Windows.Forms.Timer(this.components);
            this.btn_baru = new System.Windows.Forms.Button();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.cBox_nip = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_jam_keluar = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dTP_tgl_absensi = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_kode_keluar = new System.Windows.Forms.TextBox();
            this.btn_keluar = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_nama = new System.Windows.Forms.TextBox();
            this.txt_jam_masuk = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_kode_masuk = new System.Windows.Forms.TextBox();
            this.btn_masuk = new System.Windows.Forms.Button();
            this.groupBox_keluar = new System.Windows.Forms.GroupBox();
            this.groupBox_masuk = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox_keluar.SuspendLayout();
            this.groupBox_masuk.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_kode_posisi
            // 
            this.txt_kode_posisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_posisi.Location = new System.Drawing.Point(479, 106);
            this.txt_kode_posisi.Name = "txt_kode_posisi";
            this.txt_kode_posisi.ReadOnly = true;
            this.txt_kode_posisi.Size = new System.Drawing.Size(179, 24);
            this.txt_kode_posisi.TabIndex = 31;
            // 
            // lbl_clock
            // 
            this.lbl_clock.AutoSize = true;
            this.lbl_clock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_clock.Location = new System.Drawing.Point(317, 34);
            this.lbl_clock.Name = "lbl_clock";
            this.lbl_clock.Size = new System.Drawing.Size(40, 18);
            this.lbl_clock.TabIndex = 30;
            this.lbl_clock.Text = "Jam";
            // 
            // clock1
            // 
            this.clock1.Enabled = true;
            this.clock1.Interval = 1000;
            this.clock1.Tick += new System.EventHandler(this.clock1_Tick);
            // 
            // btn_baru
            // 
            this.btn_baru.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_baru.Location = new System.Drawing.Point(509, 295);
            this.btn_baru.Name = "btn_baru";
            this.btn_baru.Size = new System.Drawing.Size(71, 25);
            this.btn_baru.TabIndex = 4;
            this.btn_baru.Text = "Baru";
            this.btn_baru.UseVisualStyleBackColor = true;
            this.btn_baru.Click += new System.EventHandler(this.btn_baru_Click);
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(602, 295);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(71, 25);
            this.btn_tutup.TabIndex = 5;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // cBox_nip
            // 
            this.cBox_nip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_nip.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_nip.FormattingEnabled = true;
            this.cBox_nip.Location = new System.Drawing.Point(137, 107);
            this.cBox_nip.Name = "cBox_nip";
            this.cBox_nip.Size = new System.Drawing.Size(179, 26);
            this.cBox_nip.TabIndex = 1;
            this.cBox_nip.SelectedIndexChanged += new System.EventHandler(this.cBox_nip_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 18);
            this.label4.TabIndex = 24;
            this.label4.Text = "NIP";
            // 
            // txt_jam_keluar
            // 
            this.txt_jam_keluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jam_keluar.Location = new System.Drawing.Point(134, 30);
            this.txt_jam_keluar.Name = "txt_jam_keluar";
            this.txt_jam_keluar.ReadOnly = true;
            this.txt_jam_keluar.Size = new System.Drawing.Size(168, 24);
            this.txt_jam_keluar.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(352, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 32;
            this.label6.Text = "Kode Posisi: ";
            // 
            // dTP_tgl_absensi
            // 
            this.dTP_tgl_absensi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_absensi.Location = new System.Drawing.Point(137, 71);
            this.dTP_tgl_absensi.Name = "dTP_tgl_absensi";
            this.dTP_tgl_absensi.Size = new System.Drawing.Size(179, 24);
            this.dTP_tgl_absensi.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 18);
            this.label3.TabIndex = 22;
            this.label3.Text = "Tanggal Absensi: ";
            // 
            // txt_kode_keluar
            // 
            this.txt_kode_keluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_keluar.Location = new System.Drawing.Point(134, 59);
            this.txt_kode_keluar.Name = "txt_kode_keluar";
            this.txt_kode_keluar.Size = new System.Drawing.Size(168, 24);
            this.txt_kode_keluar.TabIndex = 0;
            // 
            // btn_keluar
            // 
            this.btn_keluar.Location = new System.Drawing.Point(220, 90);
            this.btn_keluar.Name = "btn_keluar";
            this.btn_keluar.Size = new System.Drawing.Size(71, 25);
            this.btn_keluar.TabIndex = 1;
            this.btn_keluar.Text = "Keluar";
            this.btn_keluar.UseVisualStyleBackColor = true;
            this.btn_keluar.Click += new System.EventHandler(this.btn_keluar_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 18);
            this.label9.TabIndex = 14;
            this.label9.Text = "Jam Keluar: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "Kode Keluar: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(352, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 18);
            this.label5.TabIndex = 29;
            this.label5.Text = "Nama: ";
            // 
            // txt_nama
            // 
            this.txt_nama.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nama.Location = new System.Drawing.Point(479, 72);
            this.txt_nama.Name = "txt_nama";
            this.txt_nama.ReadOnly = true;
            this.txt_nama.Size = new System.Drawing.Size(179, 24);
            this.txt_nama.TabIndex = 27;
            // 
            // txt_jam_masuk
            // 
            this.txt_jam_masuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jam_masuk.Location = new System.Drawing.Point(137, 33);
            this.txt_jam_masuk.Name = "txt_jam_masuk";
            this.txt_jam_masuk.ReadOnly = true;
            this.txt_jam_masuk.Size = new System.Drawing.Size(168, 24);
            this.txt_jam_masuk.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 18);
            this.label8.TabIndex = 10;
            this.label8.Text = "Jam Masuk: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "Kode Masuk: ";
            // 
            // txt_kode_masuk
            // 
            this.txt_kode_masuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_masuk.Location = new System.Drawing.Point(136, 62);
            this.txt_kode_masuk.Name = "txt_kode_masuk";
            this.txt_kode_masuk.Size = new System.Drawing.Size(168, 24);
            this.txt_kode_masuk.TabIndex = 0;
            // 
            // btn_masuk
            // 
            this.btn_masuk.Location = new System.Drawing.Point(233, 93);
            this.btn_masuk.Name = "btn_masuk";
            this.btn_masuk.Size = new System.Drawing.Size(71, 25);
            this.btn_masuk.TabIndex = 1;
            this.btn_masuk.Text = "Masuk";
            this.btn_masuk.UseVisualStyleBackColor = true;
            this.btn_masuk.Click += new System.EventHandler(this.btn_masuk_Click);
            // 
            // groupBox_keluar
            // 
            this.groupBox_keluar.Controls.Add(this.txt_jam_keluar);
            this.groupBox_keluar.Controls.Add(this.txt_kode_keluar);
            this.groupBox_keluar.Controls.Add(this.btn_keluar);
            this.groupBox_keluar.Controls.Add(this.label9);
            this.groupBox_keluar.Controls.Add(this.label10);
            this.groupBox_keluar.Enabled = false;
            this.groupBox_keluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_keluar.Location = new System.Drawing.Point(355, 154);
            this.groupBox_keluar.Name = "groupBox_keluar";
            this.groupBox_keluar.Size = new System.Drawing.Size(318, 135);
            this.groupBox_keluar.TabIndex = 3;
            this.groupBox_keluar.TabStop = false;
            this.groupBox_keluar.Text = "Absen Keluar";
            // 
            // groupBox_masuk
            // 
            this.groupBox_masuk.Controls.Add(this.txt_jam_masuk);
            this.groupBox_masuk.Controls.Add(this.label8);
            this.groupBox_masuk.Controls.Add(this.label7);
            this.groupBox_masuk.Controls.Add(this.txt_kode_masuk);
            this.groupBox_masuk.Controls.Add(this.btn_masuk);
            this.groupBox_masuk.Enabled = false;
            this.groupBox_masuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_masuk.Location = new System.Drawing.Point(12, 148);
            this.groupBox_masuk.Name = "groupBox_masuk";
            this.groupBox_masuk.Size = new System.Drawing.Size(323, 138);
            this.groupBox_masuk.TabIndex = 2;
            this.groupBox_masuk.TabStop = false;
            this.groupBox_masuk.Text = "Absen Masuk";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(253, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 24);
            this.label1.TabIndex = 19;
            this.label1.Text = "Absensi Karyawan";
            // 
            // Absensi_Karyawan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(684, 349);
            this.Controls.Add(this.txt_kode_posisi);
            this.Controls.Add(this.lbl_clock);
            this.Controls.Add(this.btn_baru);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.cBox_nip);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dTP_tgl_absensi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_nama);
            this.Controls.Add(this.groupBox_keluar);
            this.Controls.Add(this.groupBox_masuk);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Absensi_Karyawan";
            this.Text = "Absensi_Karyawan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Absensi_Karyawan_FormClosing);
            this.Load += new System.EventHandler(this.Absensi_Karyawan_Load);
            this.groupBox_keluar.ResumeLayout(false);
            this.groupBox_keluar.PerformLayout();
            this.groupBox_masuk.ResumeLayout(false);
            this.groupBox_masuk.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_kode_posisi;
        private System.Windows.Forms.Label lbl_clock;
        private System.Windows.Forms.Timer clock1;
        private System.Windows.Forms.Button btn_baru;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.ComboBox cBox_nip;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_jam_keluar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dTP_tgl_absensi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_kode_keluar;
        private System.Windows.Forms.Button btn_keluar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_nama;
        private System.Windows.Forms.TextBox txt_jam_masuk;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_kode_masuk;
        private System.Windows.Forms.Button btn_masuk;
        private System.Windows.Forms.GroupBox groupBox_keluar;
        private System.Windows.Forms.GroupBox groupBox_masuk;
        private System.Windows.Forms.Label label1;
    }
}