﻿namespace E_TIK_Service
{
    partial class penambahan_maskapai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cBox_kd_pesawat = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cBox_kd_sesi_penerbangan = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox_VIP = new System.Windows.Forms.CheckBox();
            this.checkBox_Ekonomi = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_jumalh = new System.Windows.Forms.TextBox();
            this.btn_tambah = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbox_kodePenambahamMaskapai = new System.Windows.Forms.TextBox();
            this.txtbox_tanggalInput = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cBox_kd_pesawat
            // 
            this.cBox_kd_pesawat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_pesawat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_pesawat.FormattingEnabled = true;
            this.cBox_kd_pesawat.Location = new System.Drawing.Point(313, 103);
            this.cBox_kd_pesawat.Name = "cBox_kd_pesawat";
            this.cBox_kd_pesawat.Size = new System.Drawing.Size(162, 26);
            this.cBox_kd_pesawat.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(55, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kode Pesawat:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(55, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kode Sesi Penerbangan:";
            // 
            // cBox_kd_sesi_penerbangan
            // 
            this.cBox_kd_sesi_penerbangan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_sesi_penerbangan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_sesi_penerbangan.FormattingEnabled = true;
            this.cBox_kd_sesi_penerbangan.Location = new System.Drawing.Point(313, 142);
            this.cBox_kd_sesi_penerbangan.Name = "cBox_kd_sesi_penerbangan";
            this.cBox_kd_sesi_penerbangan.Size = new System.Drawing.Size(162, 26);
            this.cBox_kd_sesi_penerbangan.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(55, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Jenis Kursi:";
            // 
            // checkBox_VIP
            // 
            this.checkBox_VIP.AutoSize = true;
            this.checkBox_VIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_VIP.Location = new System.Drawing.Point(313, 186);
            this.checkBox_VIP.Name = "checkBox_VIP";
            this.checkBox_VIP.Size = new System.Drawing.Size(52, 22);
            this.checkBox_VIP.TabIndex = 5;
            this.checkBox_VIP.Text = "VIP";
            this.checkBox_VIP.UseVisualStyleBackColor = true;
            this.checkBox_VIP.CheckedChanged += new System.EventHandler(this.checkBox_VIP_CheckedChanged);
            // 
            // checkBox_Ekonomi
            // 
            this.checkBox_Ekonomi.AutoSize = true;
            this.checkBox_Ekonomi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Ekonomi.Location = new System.Drawing.Point(381, 186);
            this.checkBox_Ekonomi.Name = "checkBox_Ekonomi";
            this.checkBox_Ekonomi.Size = new System.Drawing.Size(94, 22);
            this.checkBox_Ekonomi.TabIndex = 6;
            this.checkBox_Ekonomi.Text = "Ekonomi";
            this.checkBox_Ekonomi.UseVisualStyleBackColor = true;
            this.checkBox_Ekonomi.CheckedChanged += new System.EventHandler(this.checkBox_Ekonomi_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(55, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Jumlah:";
            // 
            // txt_jumalh
            // 
            this.txt_jumalh.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jumalh.Location = new System.Drawing.Point(313, 216);
            this.txt_jumalh.Name = "txt_jumalh";
            this.txt_jumalh.Size = new System.Drawing.Size(162, 24);
            this.txt_jumalh.TabIndex = 8;
            // 
            // btn_tambah
            // 
            this.btn_tambah.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tambah.Location = new System.Drawing.Point(522, 285);
            this.btn_tambah.Name = "btn_tambah";
            this.btn_tambah.Size = new System.Drawing.Size(89, 26);
            this.btn_tambah.TabIndex = 9;
            this.btn_tambah.Text = "Tambah";
            this.btn_tambah.UseVisualStyleBackColor = true;
            this.btn_tambah.Click += new System.EventHandler(this.btn_tambah_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(94, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(453, 33);
            this.label5.TabIndex = 10;
            this.label5.Text = "Lembar Penambahan Maskapai";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(55, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(225, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Kode penambahan maskpai :";
            // 
            // txtbox_kodePenambahamMaskapai
            // 
            this.txtbox_kodePenambahamMaskapai.Location = new System.Drawing.Point(313, 71);
            this.txtbox_kodePenambahamMaskapai.Name = "txtbox_kodePenambahamMaskapai";
            this.txtbox_kodePenambahamMaskapai.Size = new System.Drawing.Size(162, 20);
            this.txtbox_kodePenambahamMaskapai.TabIndex = 12;
            // 
            // txtbox_tanggalInput
            // 
            this.txtbox_tanggalInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_tanggalInput.Location = new System.Drawing.Point(313, 250);
            this.txtbox_tanggalInput.Name = "txtbox_tanggalInput";
            this.txtbox_tanggalInput.Size = new System.Drawing.Size(162, 24);
            this.txtbox_tanggalInput.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(54, 253);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 18);
            this.label7.TabIndex = 37;
            this.label7.Text = "Tanggal Input :";
            // 
            // penambahan_maskapai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 323);
            this.Controls.Add(this.txtbox_tanggalInput);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtbox_kodePenambahamMaskapai);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_tambah);
            this.Controls.Add(this.txt_jumalh);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox_Ekonomi);
            this.Controls.Add(this.checkBox_VIP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cBox_kd_sesi_penerbangan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBox_kd_pesawat);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "penambahan_maskapai";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Penambahan Maskapai Penerbangan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.penambahan_maskapai_FormClosing);
            this.Load += new System.EventHandler(this.penambahan_maskapai_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cBox_kd_pesawat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cBox_kd_sesi_penerbangan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox_VIP;
        private System.Windows.Forms.CheckBox checkBox_Ekonomi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_jumalh;
        private System.Windows.Forms.Button btn_tambah;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbox_kodePenambahamMaskapai;
        private System.Windows.Forms.TextBox txtbox_tanggalInput;
        private System.Windows.Forms.Label label7;
    }
}