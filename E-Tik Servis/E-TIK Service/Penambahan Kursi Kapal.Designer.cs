﻿namespace E_TIK_Service
{
    partial class Penambahan_Kursi_Kapal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_tambah = new System.Windows.Forms.Button();
            this.checkBox_Ekonomi = new System.Windows.Forms.CheckBox();
            this.checkBox_Eksekutif = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cBox_kd_kapal = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbox_kodePenambahanKursi = new System.Windows.Forms.TextBox();
            this.cbb_kode_sesi = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbox_tanggalInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_jumlah = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_tambah
            // 
            this.btn_tambah.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tambah.Location = new System.Drawing.Point(370, 313);
            this.btn_tambah.Name = "btn_tambah";
            this.btn_tambah.Size = new System.Drawing.Size(83, 27);
            this.btn_tambah.TabIndex = 29;
            this.btn_tambah.Text = "Tambah";
            this.btn_tambah.UseVisualStyleBackColor = true;
            this.btn_tambah.Click += new System.EventHandler(this.btn_tambah_Click);
            // 
            // checkBox_Ekonomi
            // 
            this.checkBox_Ekonomi.AutoSize = true;
            this.checkBox_Ekonomi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Ekonomi.Location = new System.Drawing.Point(360, 207);
            this.checkBox_Ekonomi.Name = "checkBox_Ekonomi";
            this.checkBox_Ekonomi.Size = new System.Drawing.Size(94, 22);
            this.checkBox_Ekonomi.TabIndex = 26;
            this.checkBox_Ekonomi.Text = "Ekonomi";
            this.checkBox_Ekonomi.UseVisualStyleBackColor = true;
            this.checkBox_Ekonomi.CheckedChanged += new System.EventHandler(this.checkBox_Ekonomi_CheckedChanged);
            // 
            // checkBox_Eksekutif
            // 
            this.checkBox_Eksekutif.AutoSize = true;
            this.checkBox_Eksekutif.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Eksekutif.Location = new System.Drawing.Point(257, 207);
            this.checkBox_Eksekutif.Name = "checkBox_Eksekutif";
            this.checkBox_Eksekutif.Size = new System.Drawing.Size(97, 22);
            this.checkBox_Eksekutif.TabIndex = 25;
            this.checkBox_Eksekutif.Text = "Eksekutif";
            this.checkBox_Eksekutif.UseVisualStyleBackColor = true;
            this.checkBox_Eksekutif.CheckedChanged += new System.EventHandler(this.checkBox_Eksekutif_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 18);
            this.label3.TabIndex = 24;
            this.label3.Text = "Jenis Kursi :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 18);
            this.label1.TabIndex = 21;
            this.label1.Text = "Kode Kapal Laut :";
            // 
            // cBox_kd_kapal
            // 
            this.cBox_kd_kapal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_kapal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_kapal.FormattingEnabled = true;
            this.cBox_kd_kapal.Location = new System.Drawing.Point(257, 122);
            this.cBox_kd_kapal.Name = "cBox_kd_kapal";
            this.cBox_kd_kapal.Size = new System.Drawing.Size(196, 26);
            this.cBox_kd_kapal.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(474, 36);
            this.label5.TabIndex = 30;
            this.label5.Text = "Lembar Penambahan Kursi Kapal Laut";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 18);
            this.label6.TabIndex = 31;
            this.label6.Text = "Kode Penambahan Kursi :";
            // 
            // txtbox_kodePenambahanKursi
            // 
            this.txtbox_kodePenambahanKursi.Location = new System.Drawing.Point(257, 88);
            this.txtbox_kodePenambahanKursi.Name = "txtbox_kodePenambahanKursi";
            this.txtbox_kodePenambahanKursi.Size = new System.Drawing.Size(196, 20);
            this.txtbox_kodePenambahanKursi.TabIndex = 32;
            this.txtbox_kodePenambahanKursi.Leave += new System.EventHandler(this.txtbox_kodePenambahanKursi_Leave);
            // 
            // cbb_kode_sesi
            // 
            this.cbb_kode_sesi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_kode_sesi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_kode_sesi.FormattingEnabled = true;
            this.cbb_kode_sesi.Location = new System.Drawing.Point(257, 163);
            this.cbb_kode_sesi.Name = "cbb_kode_sesi";
            this.cbb_kode_sesi.Size = new System.Drawing.Size(196, 26);
            this.cbb_kode_sesi.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(43, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(179, 18);
            this.label7.TabIndex = 34;
            this.label7.Text = "Kode Sesi Kapal Laut :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 18);
            this.label2.TabIndex = 35;
            this.label2.Text = "Tanggal Input :";
            // 
            // txtbox_tanggalInput
            // 
            this.txtbox_tanggalInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_tanggalInput.Location = new System.Drawing.Point(257, 281);
            this.txtbox_tanggalInput.Name = "txtbox_tanggalInput";
            this.txtbox_tanggalInput.Size = new System.Drawing.Size(197, 24);
            this.txtbox_tanggalInput.TabIndex = 36;
            this.txtbox_tanggalInput.Leave += new System.EventHandler(this.txtbox_tanggalInput_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 18);
            this.label4.TabIndex = 27;
            this.label4.Text = "Jumlah :";
            // 
            // txt_jumlah
            // 
            this.txt_jumlah.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jumlah.Location = new System.Drawing.Point(256, 242);
            this.txt_jumlah.Name = "txt_jumlah";
            this.txt_jumlah.Size = new System.Drawing.Size(197, 24);
            this.txt_jumlah.TabIndex = 28;
            // 
            // Penambahan_Kursi_Kapal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 352);
            this.Controls.Add(this.txtbox_tanggalInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbb_kode_sesi);
            this.Controls.Add(this.txtbox_kodePenambahanKursi);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_tambah);
            this.Controls.Add(this.txt_jumlah);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox_Ekonomi);
            this.Controls.Add(this.checkBox_Eksekutif);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBox_kd_kapal);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Penambahan_Kursi_Kapal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Penambahan Kursi Kapal";
            this.Load += new System.EventHandler(this.Penambahan_Kursi_Kapal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_tambah;
        private System.Windows.Forms.CheckBox checkBox_Ekonomi;
        private System.Windows.Forms.CheckBox checkBox_Eksekutif;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBox_kd_kapal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbox_kodePenambahanKursi;
        private System.Windows.Forms.ComboBox cbb_kode_sesi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbox_tanggalInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_jumlah;
    }
}