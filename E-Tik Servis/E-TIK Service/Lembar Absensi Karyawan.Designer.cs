﻿namespace E_TIK_Service
{
    partial class Lembar_Absensi_Karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_no_laporan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_tgl_laporan = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_nip = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_jam_masuk = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_jam_keluar = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_validasi_keluar = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_validasi_masuk = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_nama = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_kode_jabatan = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_kode_posisi = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_operatro = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.checkBox_tanggal = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(386, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rekap Absensi Karyawan";
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(12, 72);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(635, 263);
            this.dGV1.TabIndex = 1;
            this.dGV1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGV1_CellClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lihat Berdasarkan: ";
            // 
            // txt_no_laporan
            // 
            this.txt_no_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_no_laporan.Location = new System.Drawing.Point(768, 72);
            this.txt_no_laporan.Name = "txt_no_laporan";
            this.txt_no_laporan.ReadOnly = true;
            this.txt_no_laporan.Size = new System.Drawing.Size(149, 24);
            this.txt_no_laporan.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(650, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nomor Laporan: ";
            // 
            // txt_tgl_laporan
            // 
            this.txt_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tgl_laporan.Location = new System.Drawing.Point(768, 112);
            this.txt_tgl_laporan.Name = "txt_tgl_laporan";
            this.txt_tgl_laporan.ReadOnly = true;
            this.txt_tgl_laporan.Size = new System.Drawing.Size(149, 24);
            this.txt_tgl_laporan.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(650, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Tanggal Laporan: ";
            // 
            // txt_nip
            // 
            this.txt_nip.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nip.Location = new System.Drawing.Point(768, 190);
            this.txt_nip.Name = "txt_nip";
            this.txt_nip.ReadOnly = true;
            this.txt_nip.Size = new System.Drawing.Size(149, 24);
            this.txt_nip.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(650, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 18);
            this.label5.TabIndex = 10;
            this.label5.Text = "NIP Karyawan: ";
            // 
            // txt_jam_masuk
            // 
            this.txt_jam_masuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jam_masuk.Location = new System.Drawing.Point(134, 342);
            this.txt_jam_masuk.Name = "txt_jam_masuk";
            this.txt_jam_masuk.ReadOnly = true;
            this.txt_jam_masuk.Size = new System.Drawing.Size(149, 24);
            this.txt_jam_masuk.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 345);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 18);
            this.label6.TabIndex = 12;
            this.label6.Text = "Jam Masuk: ";
            // 
            // txt_jam_keluar
            // 
            this.txt_jam_keluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jam_keluar.Location = new System.Drawing.Point(134, 385);
            this.txt_jam_keluar.Name = "txt_jam_keluar";
            this.txt_jam_keluar.ReadOnly = true;
            this.txt_jam_keluar.Size = new System.Drawing.Size(149, 24);
            this.txt_jam_keluar.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 388);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 18);
            this.label7.TabIndex = 14;
            this.label7.Text = "Jam Keluar";
            // 
            // txt_validasi_keluar
            // 
            this.txt_validasi_keluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_validasi_keluar.Location = new System.Drawing.Point(420, 385);
            this.txt_validasi_keluar.Name = "txt_validasi_keluar";
            this.txt_validasi_keluar.ReadOnly = true;
            this.txt_validasi_keluar.Size = new System.Drawing.Size(149, 24);
            this.txt_validasi_keluar.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(302, 388);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 18);
            this.label8.TabIndex = 18;
            this.label8.Text = "Validasi Keluar";
            // 
            // txt_validasi_masuk
            // 
            this.txt_validasi_masuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_validasi_masuk.Location = new System.Drawing.Point(420, 342);
            this.txt_validasi_masuk.Name = "txt_validasi_masuk";
            this.txt_validasi_masuk.ReadOnly = true;
            this.txt_validasi_masuk.Size = new System.Drawing.Size(149, 24);
            this.txt_validasi_masuk.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(302, 345);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 18);
            this.label9.TabIndex = 16;
            this.label9.Text = "Validasi Masuk: ";
            // 
            // txt_nama
            // 
            this.txt_nama.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nama.Location = new System.Drawing.Point(768, 232);
            this.txt_nama.Name = "txt_nama";
            this.txt_nama.ReadOnly = true;
            this.txt_nama.Size = new System.Drawing.Size(149, 24);
            this.txt_nama.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(650, 235);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "Nama Karyawan: ";
            // 
            // txt_kode_jabatan
            // 
            this.txt_kode_jabatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_jabatan.Location = new System.Drawing.Point(768, 274);
            this.txt_kode_jabatan.Name = "txt_kode_jabatan";
            this.txt_kode_jabatan.ReadOnly = true;
            this.txt_kode_jabatan.Size = new System.Drawing.Size(149, 24);
            this.txt_kode_jabatan.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(650, 277);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 18);
            this.label11.TabIndex = 22;
            this.label11.Text = "Kode Jabatan: ";
            // 
            // txt_kode_posisi
            // 
            this.txt_kode_posisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_posisi.Location = new System.Drawing.Point(768, 311);
            this.txt_kode_posisi.Name = "txt_kode_posisi";
            this.txt_kode_posisi.ReadOnly = true;
            this.txt_kode_posisi.Size = new System.Drawing.Size(149, 24);
            this.txt_kode_posisi.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(650, 314);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "Kode Posisi: ";
            // 
            // txt_operatro
            // 
            this.txt_operatro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_operatro.Location = new System.Drawing.Point(768, 149);
            this.txt_operatro.Name = "txt_operatro";
            this.txt_operatro.ReadOnly = true;
            this.txt_operatro.Size = new System.Drawing.Size(149, 24);
            this.txt_operatro.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(650, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 18);
            this.label14.TabIndex = 27;
            this.label14.Text = "NIP Operator: ";
            // 
            // btn_refresh
            // 
            this.btn_refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refresh.Location = new System.Drawing.Point(751, 380);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(75, 26);
            this.btn_refresh.TabIndex = 12;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(842, 380);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 26);
            this.btn_tutup.TabIndex = 13;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(263, 40);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(172, 24);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // checkBox_tanggal
            // 
            this.checkBox_tanggal.AutoSize = true;
            this.checkBox_tanggal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_tanggal.Location = new System.Drawing.Point(170, 44);
            this.checkBox_tanggal.Name = "checkBox_tanggal";
            this.checkBox_tanggal.Size = new System.Drawing.Size(87, 22);
            this.checkBox_tanggal.TabIndex = 38;
            this.checkBox_tanggal.Text = "Tanggal: ";
            this.checkBox_tanggal.UseVisualStyleBackColor = true;
            this.checkBox_tanggal.CheckedChanged += new System.EventHandler(this.checkBox_tanggal_CheckedChanged);
            // 
            // Lembar_Absensi_Karyawan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(929, 421);
            this.Controls.Add(this.checkBox_tanggal);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.txt_operatro);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txt_kode_posisi);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txt_kode_jabatan);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_nama);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txt_validasi_keluar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_validasi_masuk);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_jam_keluar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_jam_masuk);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_nip);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_tgl_laporan);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_no_laporan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dGV1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Lembar_Absensi_Karyawan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lembar Absensi Karyawan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Lembar_Absensi_Karyawan_FormClosing);
            this.Load += new System.EventHandler(this.Lembar_Absensi_Karyawan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_no_laporan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_tgl_laporan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_nip;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_jam_masuk;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_jam_keluar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_validasi_keluar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_validasi_masuk;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_nama;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_kode_jabatan;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_kode_posisi;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_operatro;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox checkBox_tanggal;
    }
}