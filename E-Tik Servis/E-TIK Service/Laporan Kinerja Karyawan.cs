﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Laporan_Kinerja_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;

        public Laporan_Kinerja_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
           
        }

        private void Laporan_Kinerja_Karyawan_Load(object sender, EventArgs e)
        {
            NIPKaryawan();
        }

        private void NIPKaryawan()
        {
            string query = "SELECT NIP FROM tb_travel_m_karyawan";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                DataTable dt = new DataTable();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_nip_karyawan.Items.Add(r[0].ToString());
                }

                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void KodeNNama()
        {
            string query = "SELECT kode_jabatan, nama_karyawan FROM tb_travel_m_karyawan WHERE NIP = '"+ cBox_nip_karyawan.SelectedItem.ToString() +"'";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                DataTable dt = new DataTable();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_kode_jawbatan.Text = r[0].ToString();
                    txt_nama_karyawan.Text = r[1].ToString();
                }

                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string pilihanRadioButton;

        private void GetRadioBtnValue()
        {
            if (radioButton_Baik.Checked)
            {
                //MessageBox.Show(radioButton_Baik.Text);
                pilihanRadioButton = radioButton_Baik.Text;
            }
            else if (radioButton_Buruk.Checked)
            {
                //MessageBox.Show(radioButton_Buruk.Text);
                pilihanRadioButton = radioButton_Buruk.Text;
            }
            else if (radioButton_Sedang.Checked)
            {
                //MessageBox.Show(radioButton_Baik.Text);
                pilihanRadioButton = radioButton_Sedang.Text;
            }

            //MessageBox.Show("hasil: " + pilihanRadioButton);
        }

        string InsertMaster(string no, string tgl, string validasi)
        {
            string query = "INSERT INTO tb_travel_m_tgl_lapkin (no_lapkin, tgl_lapkin, validasi_lapkin) VALUES (@1, @2, @3)";
            cmd = new MySqlCommand(query,kon);
            cmd.Parameters.AddWithValue("@1",no);
            cmd.Parameters.AddWithValue("@2",tgl);
            cmd.Parameters.AddWithValue("@3",validasi);
            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Master Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return no;
        }

        void InsertRelasi(string no, string kode, string nip, string kinerja)
        {
            string query = "INSERT INTO tb_travel_r_lapkin(no_lapkin, kode_jabatan, NIP, tingkat_kinerja) VALUES (@1, @2, @3, @4)";
            cmd = new MySqlCommand(query,kon);
            cmd.Parameters.AddWithValue("@1",no);
            cmd.Parameters.AddWithValue("@2",kode);
            cmd.Parameters.AddWithValue("@3",nip);
            cmd.Parameters.AddWithValue("@4",kinerja);
            
            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Relation Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_simpan_Click(object sender, EventArgs e)
        {
            GetRadioBtnValue();
            InsertMaster(txt_no_laporan.Text,dTP_tgl_laporan.Value.Date.ToString("yyyy-MM-dd"),txt_nip_operator.Text);
            InsertRelasi(txt_no_laporan.Text,txt_kode_jawbatan.Text,cBox_nip_karyawan.SelectedItem.ToString(),pilihanRadioButton);
        }

        private void cBox_nip_karyawan_SelectedIndexChanged(object sender, EventArgs e)
        {
            KodeNNama();
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Laporan_Kinerja_Karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}