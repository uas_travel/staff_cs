﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class cs_ka : Form
    {
        static MySqlConnection con;
        static MySqlCommand com;
        static MySqlDataAdapter adap;

        static string sJml_bangku;
        static int jml_bangku;
        static int sisa_bangku;
        string kode_bangku;
        bool hasil = false;
        public cs_ka()
        {
            InitializeComponent();
            Constanta.Server();
            con = new MySqlConnection(Constanta.conString);
           
        }

        private void cs_ka_Load(object sender, EventArgs e)
        {
            getDataToCbbKK();
            getDataToCbbKs();
            getDataToCbbKk();
            narikJumlahKursi();
        }

        private void cs_ka_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_cs.IsAnak = !mdi_cs.IsAnak;
        }

        private void LoadKodeKA()
        {
        }

        #region RegionComader
        void getDataToCbbKK()
        {
            string query = "SELECT kode_kereta, nama_kereta FROM tb_travel_m_kereta";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_kereta.ValueMember = "kode_kereta";
                    cBox_kd_kereta.DisplayMember = "nama_kereta";
                    cBox_kd_kereta.DataSource = dt;
                }
                cBox_kd_kereta.Text = null;

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToCbbKs()
        {
            string query = "SELECT kode_sesi FROM tb_travel_m_sesi_ka";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();
                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_sesi.Items.Add(r[0].ToString());
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToCbbKk()
        {
            string query = "SELECT kode_keberangkatan FROM tb_travel_m_keberangkatan_ka";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_kebrangkatan.Items.Add(r[0].ToString());
                }
                

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void narikJumlahKursi()
        {
            string query = "SELECT * FROM tb_travel_m_jml_bangku_ka";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    sJml_bangku = r[2].ToString();
                    jml_bangku = int.Parse(sJml_bangku);
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void upddateJmlKursi()
        {
            string query = "UPDATE tb_travel_m_jml_bangku_ka SET jml_bangku = '"+sisa_bangku+"' WHERE kode_bangku = '"+kode_bangku+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                adap = new MySqlDataAdapter(com);
                adap.UpdateCommand = con.CreateCommand();
                adap.UpdateCommand.CommandText = query;

                if (adap.UpdateCommand.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("wokay");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void input()
        {
            string query = "INSERT INTO tb_travel_r_pembelian_tiket_ka (kode_pembelian, nama_penumpang, kode_kereta, kode_bangku, nomor_kursi, kode_keberangkatan, kode_sesi, tanggal_pembelian, harga) VALUES (@1,@2,@3,@4,@5,@6,@7,@8,@9)";
            com = new MySqlCommand(query, con);

            com.Parameters.AddWithValue("@1", txt_kodepembelian.Text);
            com.Parameters.AddWithValue("@2", txt_namapenumpang.Text);
            com.Parameters.AddWithValue("@3", cBox_kd_kereta.SelectedValue.ToString());
            com.Parameters.AddWithValue("@4", kode_bangku);
            com.Parameters.AddWithValue("@5", txt_bangku.Text);
            com.Parameters.AddWithValue("@6", cBox_kd_kebrangkatan.SelectedItem.ToString().Substring(0,5));
            com.Parameters.AddWithValue("@7", cBox_kd_sesi.SelectedItem.ToString().Substring(0,3));
            com.Parameters.AddWithValue("@8", dTP_tgl_pembelian.Value.Date.ToString("yyyy-MM-dd"));
            com.Parameters.AddWithValue("@9", txt_harga.Text);

            try
            {
                con.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted!");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        void Clear()
        {
            txt_bangku.Clear(); txt_harga.Clear(); txt_kodepembelian.Clear(); txt_namapenumpang.Clear();
            txtbox_asal.Clear(); txtbox_tujuan.Clear(); textBox_berangkat.Clear(); textBox_samapai.Clear();
        }

        void getradiobtn()
        {
            if (radioButton_Ekonomi.Checked)
            {
                kode_bangku = "EKN";
            }
            else if (radioButton_Eksekutif.Checked)
            {
                kode_bangku = "EKS";
            }
        }

        private void radioButton_Ekonomi_CheckedChanged(object sender, EventArgs e)
        {
            //if (hasil == true)
            //{
            //    hasil = false;
            //    kode_bangku = "EKN";
            //    //MessageBox.Show("ekn");
            //}      
        }

        private void radioButton_Eksekutif_CheckedChanged(object sender, EventArgs e)
        {
            //if (hasil != true)
            //{
            //    hasil = true;
            //    kode_bangku = "EKS";
            //    //MessageBox.Show("eks");
            //}
        }

        private void btn_beli_Click(object sender, EventArgs e)
        {
            getradiobtn();
            //MessageBox.Show(kode_bangku);
            int i = int.Parse(txt_bangku.Text);
            sisa_bangku = jml_bangku - i;
            upddateJmlKursi();
            input();
            Clear();
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cBox_kd_kebrangkatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT tb_travel_m_keberangkatan_ka.kode_keberangkatan, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan FROM tb_travel_m_keberangkatan_ka INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_ka.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_keberangkatan_ka.kode_tujuan = tb_travel_m_kota_tujuan.kode_tujuan WHERE tb_travel_m_keberangkatan_ka.kode_keberangkatan = '"+ cBox_kd_kebrangkatan.SelectedItem.ToString() +"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txtbox_asal.Text = r[1].ToString();
                    txtbox_tujuan.Text = r[2].ToString();
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cBox_kd_sesi_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT jam_berangkat, jam_keluar FROM tb_travel_m_sesi_ka WHERE kode_sesi = '"+ cBox_kd_sesi.SelectedItem.ToString() +"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();
                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    textBox_berangkat.Text = r[0].ToString();
                    textBox_samapai.Text = r[1].ToString();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
