﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Absensi_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;

        public Absensi_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Absensi_Karyawan_Load(object sender, EventArgs e)
        {
            GetNIP();
        }

        int sec;
        void timesys()
        {
            if (sec % 2 == 0)
            {
                //txt_jam_masuk.Text = DateTime.Now.ToLongTimeString();
                lbl_clock.Text = DateTime.Now.ToLongTimeString();
                //txt_jam_keluar.Text = DateTime.Now.ToLongTimeString();
            }
            else
            {
                //txt_jam_masuk.Text = DateTime.Now.ToLongTimeString();
                lbl_clock.Text = DateTime.Now.ToLongTimeString();
                //txt_jam_keluar.Text = DateTime.Now.ToLongTimeString();
            } 
        }

        void GetNIP()
        {
            string query = "SELECT NIP FROM tb_travel_m_karyawan";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_nip.Items.Add(r[0].ToString());
                }
                kon.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void InsertMaster(string tgl, string nip, string jam_masuk, string validasi_masuk, string jam_keluar, string validasi_keluar)
        {
            string query = "INSERT INTO tb_travel_m_absensi (tgl_absen, NIP, jam_masuk, validasi_masuk, jam_keluar, validasi_keluar) VALUES                             (@tgl_absen,@nip,@jam_masuk,@validasi_masuk,@jam_keluar,@validasi_keluar)";
            cmd = new MySqlCommand(query, kon);
            cmd.Parameters.AddWithValue("@tgl_absen", tgl);
            cmd.Parameters.AddWithValue("@nip",nip);
            cmd.Parameters.AddWithValue("@jam_masuk",jam_masuk);
            cmd.Parameters.AddWithValue("@validasi_masuk",validasi_masuk);
            cmd.Parameters.AddWithValue("@jam_keluar", jam_keluar);
            cmd.Parameters.AddWithValue("@validasi_keluar", validasi_keluar);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                    MessageBox.Show("Inserted!");
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_baru_Click(object sender, EventArgs e)
        {
            txt_kode_keluar.Clear(); txt_jam_keluar.Clear();
            txt_kode_masuk.Clear(); txt_jam_masuk.Clear();
            txt_nama.Clear();
        }

        private void clock1_Tick(object sender, EventArgs e)
        {
            timesys();
        }

        private void btn_keluar_Click(object sender, EventArgs e)
        {
            txt_jam_keluar.Text = lbl_clock.Text;
            MessageBox.Show(txt_kode_keluar.Text + " " + txt_jam_keluar.Text);
            groupBox_keluar.Enabled = false;
            InsertMaster(dTP_tgl_absensi.Value.Date.ToString("yyyy-MM-dd"),cBox_nip.SelectedItem.ToString(), txt_jam_masuk.Text, txt_kode_masuk.Text, txt_jam_keluar.Text, txt_kode_keluar.Text);
        }

        private void btn_masuk_Click(object sender, EventArgs e)
        {
            groupBox_masuk.Enabled = false;
            groupBox_keluar.Enabled = true;
            txt_jam_masuk.Text = lbl_clock.Text;
            MessageBox.Show(txt_kode_masuk.Text + " " + txt_jam_masuk.Text);
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cBox_nip_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT nama_karyawan, kode_posisi FROM tb_travel_m_karyawan WHERE NIP = '" + cBox_nip.SelectedItem.ToString() + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_nama.Text = r[0].ToString();
                    txt_kode_posisi.Text = r[1].ToString();
                }
                kon.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            groupBox_masuk.Enabled = true;
        }

        private void Absensi_Karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}
