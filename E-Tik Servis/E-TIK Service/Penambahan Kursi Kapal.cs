﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace E_TIK_Service
{
    public partial class Penambahan_Kursi_Kapal : Form
    {
        static MySqlConnection con;
        static MySqlDataAdapter adap;
        static MySqlCommand com;

        static string sJml_bangku_eks;
        static string sJml_bangku_ekn;
        static int jml_bangku_eks;
        static int jml_bangku_ekn;
        static int sisa_bangku_eks;
        static int sisa_bangku_ekn;
        static string kode_bangku_eks;
        static string kode_bangku_ekn;
        static string kode_bangku;
        //static string kode_bangku;

        #region RegionComander
        void getDataToCbbKKL()
        {
            string query = "SELECT * FROM tb_travel_m_kapal_laut";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                cBox_kd_kapal.DataSource = dt;
                cBox_kd_kapal.ValueMember = "kode_kl";
                cBox_kd_kapal.DisplayMember = "nama_kl";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToCbbKS()
        {
            string query = "SELECT * FROM tb_travel_m_sesi_kl";
            com = new MySqlCommand(query, con);
            
            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                cbb_kode_sesi.DataSource = dt;
                cbb_kode_sesi.DisplayMember = "kode_sesi_kl";
                cbb_kode_sesi.ValueMember = "kode_sesi_kl";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void inputIntoTbTgl()
        {
            string query = "INSERT INTO tb_travel_m_tgl_penambahan_kursi_kl (kode_penambahan_kl1, tgl_penambahan_kl) VALUES (@1,@2)";
            com = new MySqlCommand(query, con);

            com.Parameters.AddWithValue("@1", txtbox_kodePenambahanKursi.Text);
            com.Parameters.AddWithValue("@2", txtbox_tanggalInput.Text);

            try
            {
                con.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Input ke tabel tanggal Berhasil");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            con.Close();
        }
        void inputTotblRPenambahanKursi()
        {
            string query = "INSERT INTO tb_travel_r_penambahan_kursi_kl  (kode_penambahan_kl1, kode_kl, kode_sesi_kl, kode_bangku_kl, jumlah_bangku_input) VALUES (@1, @2, @3, @4, @5)";
            com = new MySqlCommand(query, con);

            com.Parameters.AddWithValue("@1", txtbox_kodePenambahanKursi.Text);
            com.Parameters.AddWithValue("@2", cBox_kd_kapal.SelectedValue);
            com.Parameters.AddWithValue("@3", cbb_kode_sesi.SelectedValue);
            com.Parameters.AddWithValue("@4", kode_bangku);
            com.Parameters.AddWithValue("@5", txt_jumlah.Text);

            try
            {
                con.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("insert tabel penambahan kursi berhasil");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void updateJmlhBangkuEkn()
        {
            string query = "UPDATE tb_travel_m_jml_bangku_kl SET jml_bangku_kl = '"+sisa_bangku_ekn+"' WHERE kode_bangku_kl = '"+kode_bangku_ekn+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                adap = new MySqlDataAdapter();
                adap.UpdateCommand = con.CreateCommand();
                adap.UpdateCommand.CommandText = query;

                if (adap.UpdateCommand.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Horay, EKN berhasil");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void updateJumlahBangkuEks()
        {
            string query = "UPDATE tb_travel_m_jml_bangku_kl SET jml_bangku_kl ='"+sisa_bangku_eks+"' WHERE kode_bangku_kl = '"+kode_bangku_eks+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                adap = new MySqlDataAdapter();
                adap.UpdateCommand = con.CreateCommand();
                adap.UpdateCommand.CommandText = query;

                if (adap.UpdateCommand.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("HURAY eks bisa");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataJmlKursiEKS()
        {
            string query = "SELECT * FROM tb_travel_m_jml_bangku_kl WHERE kode_bangku_kl = '"+kode_bangku_eks+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    sJml_bangku_eks = r[2].ToString();
                    jml_bangku_eks = int.Parse(sJml_bangku_eks);
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataJmlKursiEKN()
        {
            string query = "SELECT * FROM tb_travel_m_jml_bangku_kl WHERE kode_bangku_kl = '" + kode_bangku_ekn + "'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    sJml_bangku_ekn = r[2].ToString();
                    jml_bangku_ekn = int.Parse(sJml_bangku_ekn);
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void g1()
        {
            if (checkBox_Ekonomi.Checked == true && checkBox_Eksekutif.Checked == true)
            {
                int i = int.Parse(txt_jumlah.Text);

                sisa_bangku_ekn = jml_bangku_ekn + i;
                sisa_bangku_eks = jml_bangku_eks + i;
                updateJmlhBangkuEkn();
                updateJumlahBangkuEks();
            }
            else if (checkBox_Eksekutif.Checked == true)
            {
                int i = int.Parse(txt_jumlah.Text);
                sisa_bangku_eks = jml_bangku_eks + i;
                updateJumlahBangkuEks();
            }
            else if (checkBox_Ekonomi.Checked == true)
            {
                int i = int.Parse(txt_jumlah.Text);
                sisa_bangku_ekn = jml_bangku_ekn + i;
                updateJmlhBangkuEkn();
            }
            
            //MessageBox.Show(kode_bangku);
        }
        #endregion

        public Penambahan_Kursi_Kapal()
        {
            InitializeComponent();
            Constanta.Server();
            con = new MySqlConnection(Constanta.conString);
            getDataToCbbKKL();
            getDataToCbbKS();
            //getdatatonyoba();
        }

        private void Penambahan_Kursi_Kapal_Load(object sender, EventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        private void checkBox_Ekonomi_CheckedChanged(object sender, EventArgs e)
        {
            kode_bangku = "EKN";
            kode_bangku_ekn = "EKN";
            getDataJmlKursiEKN();
            //MessageBox.Show(sJml_bangku_ekn);
        }

        private void checkBox_Eksekutif_CheckedChanged(object sender, EventArgs e)
        {
            kode_bangku = "EKS";
            kode_bangku_eks = "EKS";
            getDataJmlKursiEKS();
            //MessageBox.Show(sJml_bangku_eks);
            //MessageBox.Show(kode_bangku);
        }

        private void btn_tambah_Click(object sender, EventArgs e)
        {
            g1();
            //inputIntoTbTgl();
            inputTotblRPenambahanKursi();
        }

        private void txtbox_kodePenambahanKursi_Leave(object sender, EventArgs e)
        {
            
        }

        private void txtbox_tanggalInput_Leave(object sender, EventArgs e)
        {
            //inputIntoTbTgl();
        }
        
    }
}
