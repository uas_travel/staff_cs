﻿namespace E_TIK_Service
{
    partial class Laporan_Penjualan_Tiket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_operator = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.btn_simpan = new System.Windows.Forms.Button();
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.dTP_tgl_laporan = new System.Windows.Forms.DateTimePicker();
            this.txt_no_laporan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cBox_ktegori_tiket = new System.Windows.Forms.ComboBox();
            this.cBox_vendor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_operator
            // 
            this.txt_operator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_operator.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txt_operator.Location = new System.Drawing.Point(172, 80);
            this.txt_operator.Name = "txt_operator";
            this.txt_operator.Size = new System.Drawing.Size(171, 24);
            this.txt_operator.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 18);
            this.label5.TabIndex = 22;
            this.label5.Text = "ID Operator: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 18);
            this.label4.TabIndex = 20;
            this.label4.Text = "Kategori Tiket: ";
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(893, 459);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 30);
            this.btn_tutup.TabIndex = 6;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // btn_simpan
            // 
            this.btn_simpan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_simpan.Location = new System.Drawing.Point(802, 459);
            this.btn_simpan.Name = "btn_simpan";
            this.btn_simpan.Size = new System.Drawing.Size(75, 30);
            this.btn_simpan.TabIndex = 5;
            this.btn_simpan.Text = "Simpan";
            this.btn_simpan.UseVisualStyleBackColor = true;
            this.btn_simpan.Click += new System.EventHandler(this.btn_simpan_Click);
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(12, 151);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(956, 302);
            this.dGV1.TabIndex = 17;
            // 
            // dTP_tgl_laporan
            // 
            this.dTP_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_laporan.Location = new System.Drawing.Point(524, 48);
            this.dTP_tgl_laporan.Name = "dTP_tgl_laporan";
            this.dTP_tgl_laporan.Size = new System.Drawing.Size(200, 24);
            this.dTP_tgl_laporan.TabIndex = 4;
            // 
            // txt_no_laporan
            // 
            this.txt_no_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_no_laporan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txt_no_laporan.Location = new System.Drawing.Point(172, 50);
            this.txt_no_laporan.Name = "txt_no_laporan";
            this.txt_no_laporan.Size = new System.Drawing.Size(171, 24);
            this.txt_no_laporan.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(375, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 18);
            this.label3.TabIndex = 14;
            this.label3.Text = "Tanggal Laporan: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nomor Laporan: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(265, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Laporan Penjualan Tiket";
            // 
            // cBox_ktegori_tiket
            // 
            this.cBox_ktegori_tiket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_ktegori_tiket.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_ktegori_tiket.FormattingEnabled = true;
            this.cBox_ktegori_tiket.Items.AddRange(new object[] {
            "Kereta Api",
            "Kapal Laut",
            "Pesawat Terbang"});
            this.cBox_ktegori_tiket.Location = new System.Drawing.Point(172, 118);
            this.cBox_ktegori_tiket.Name = "cBox_ktegori_tiket";
            this.cBox_ktegori_tiket.Size = new System.Drawing.Size(171, 26);
            this.cBox_ktegori_tiket.TabIndex = 0;
            this.cBox_ktegori_tiket.SelectedIndexChanged += new System.EventHandler(this.cBox_ktegori_tiket_SelectedIndexChanged);
            // 
            // cBox_vendor
            // 
            this.cBox_vendor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_vendor.Enabled = false;
            this.cBox_vendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_vendor.FormattingEnabled = true;
            this.cBox_vendor.Location = new System.Drawing.Point(524, 118);
            this.cBox_vendor.Name = "cBox_vendor";
            this.cBox_vendor.Size = new System.Drawing.Size(200, 26);
            this.cBox_vendor.TabIndex = 1;
            this.cBox_vendor.SelectedIndexChanged += new System.EventHandler(this.cBox_vendor_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(375, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 18);
            this.label6.TabIndex = 26;
            this.label6.Text = "Nama Vendor: ";
            // 
            // Laporan_Penjualan_Tiket
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(980, 494);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cBox_vendor);
            this.Controls.Add(this.cBox_ktegori_tiket);
            this.Controls.Add(this.txt_operator);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.btn_simpan);
            this.Controls.Add(this.dGV1);
            this.Controls.Add(this.dTP_tgl_laporan);
            this.Controls.Add(this.txt_no_laporan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Laporan_Penjualan_Tiket";
            this.Text = "Laporan Penjualan Tiket";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Laporan_Penjualan_Tiket_FormClosing);
            this.Load += new System.EventHandler(this.Laporan_Penjualan_Tiket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_operator;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.Button btn_simpan;
        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.DateTimePicker dTP_tgl_laporan;
        private System.Windows.Forms.TextBox txt_no_laporan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBox_ktegori_tiket;
        private System.Windows.Forms.ComboBox cBox_vendor;
        private System.Windows.Forms.Label label6;
    }
}