﻿namespace E_TIK_Service
{
    partial class Lembar_Laporan_Upgrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox_bagian_upgrade = new System.Windows.Forms.CheckBox();
            this.cBox_bagian_upgrade = new System.Windows.Forms.ComboBox();
            this.cBox_filter_tgl = new System.Windows.Forms.ComboBox();
            this.checkBox_tanggal = new System.Windows.Forms.CheckBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.txt_validsi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_estimasi = new System.Windows.Forms.TextBox();
            this.txt_alasam = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_nolap = new System.Windows.Forms.TextBox();
            this.txt_tgl_lap = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.txt_kateg = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox_bagian_upgrade
            // 
            this.checkBox_bagian_upgrade.AutoSize = true;
            this.checkBox_bagian_upgrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_bagian_upgrade.Location = new System.Drawing.Point(13, 138);
            this.checkBox_bagian_upgrade.Name = "checkBox_bagian_upgrade";
            this.checkBox_bagian_upgrade.Size = new System.Drawing.Size(140, 22);
            this.checkBox_bagian_upgrade.TabIndex = 83;
            this.checkBox_bagian_upgrade.Text = "Bagian Upgrade: ";
            this.checkBox_bagian_upgrade.UseVisualStyleBackColor = true;
            this.checkBox_bagian_upgrade.CheckedChanged += new System.EventHandler(this.checkBox_bagian_upgrade_CheckedChanged);
            // 
            // cBox_bagian_upgrade
            // 
            this.cBox_bagian_upgrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_bagian_upgrade.Enabled = false;
            this.cBox_bagian_upgrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_bagian_upgrade.FormattingEnabled = true;
            this.cBox_bagian_upgrade.Items.AddRange(new object[] {
            "Baik",
            "Sedang",
            "Buruk"});
            this.cBox_bagian_upgrade.Location = new System.Drawing.Point(172, 136);
            this.cBox_bagian_upgrade.Name = "cBox_bagian_upgrade";
            this.cBox_bagian_upgrade.Size = new System.Drawing.Size(151, 26);
            this.cBox_bagian_upgrade.TabIndex = 1;
            this.cBox_bagian_upgrade.SelectedIndexChanged += new System.EventHandler(this.cBox_bagian_upgrade_SelectedIndexChanged);
            // 
            // cBox_filter_tgl
            // 
            this.cBox_filter_tgl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_filter_tgl.Enabled = false;
            this.cBox_filter_tgl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_filter_tgl.FormattingEnabled = true;
            this.cBox_filter_tgl.Location = new System.Drawing.Point(172, 104);
            this.cBox_filter_tgl.Name = "cBox_filter_tgl";
            this.cBox_filter_tgl.Size = new System.Drawing.Size(151, 26);
            this.cBox_filter_tgl.TabIndex = 0;
            this.cBox_filter_tgl.SelectedIndexChanged += new System.EventHandler(this.cBox_filter_tgl_SelectedIndexChanged);
            // 
            // checkBox_tanggal
            // 
            this.checkBox_tanggal.AutoSize = true;
            this.checkBox_tanggal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_tanggal.Location = new System.Drawing.Point(13, 106);
            this.checkBox_tanggal.Name = "checkBox_tanggal";
            this.checkBox_tanggal.Size = new System.Drawing.Size(87, 22);
            this.checkBox_tanggal.TabIndex = 79;
            this.checkBox_tanggal.Text = "Tanggal: ";
            this.checkBox_tanggal.UseVisualStyleBackColor = true;
            this.checkBox_tanggal.CheckedChanged += new System.EventHandler(this.checkBox_tanggal_CheckedChanged);
            // 
            // btn_clear
            // 
            this.btn_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clear.Location = new System.Drawing.Point(599, 129);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(84, 31);
            this.btn_clear.TabIndex = 2;
            this.btn_clear.Text = "Clear";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 18);
            this.label7.TabIndex = 76;
            this.label7.Text = "Lihat Berdasarkan: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(162, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(370, 37);
            this.label5.TabIndex = 75;
            this.label5.Text = "Rekap Laporan Upgrde";
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(601, 520);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(85, 31);
            this.btn_tutup.TabIndex = 10;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refresh.Location = new System.Drawing.Point(510, 520);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(85, 31);
            this.btn_refresh.TabIndex = 9;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            // 
            // txt_validsi
            // 
            this.txt_validsi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_validsi.Location = new System.Drawing.Point(172, 485);
            this.txt_validsi.Name = "txt_validsi";
            this.txt_validsi.ReadOnly = true;
            this.txt_validsi.Size = new System.Drawing.Size(173, 24);
            this.txt_validsi.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 485);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 18);
            this.label6.TabIndex = 71;
            this.label6.Text = "Validasi Laporan: ";
            // 
            // txt_estimasi
            // 
            this.txt_estimasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_estimasi.Location = new System.Drawing.Point(510, 482);
            this.txt_estimasi.Name = "txt_estimasi";
            this.txt_estimasi.ReadOnly = true;
            this.txt_estimasi.Size = new System.Drawing.Size(173, 24);
            this.txt_estimasi.TabIndex = 8;
            // 
            // txt_alasam
            // 
            this.txt_alasam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_alasam.Location = new System.Drawing.Point(442, 381);
            this.txt_alasam.Multiline = true;
            this.txt_alasam.Name = "txt_alasam";
            this.txt_alasam.ReadOnly = true;
            this.txt_alasam.Size = new System.Drawing.Size(241, 95);
            this.txt_alasam.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(367, 488);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 18);
            this.label3.TabIndex = 68;
            this.label3.Text = "Estimasi Harga: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(368, 387);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 18);
            this.label4.TabIndex = 67;
            this.label4.Text = "Alasan: ";
            // 
            // txt_nolap
            // 
            this.txt_nolap.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nolap.Location = new System.Drawing.Point(172, 422);
            this.txt_nolap.Name = "txt_nolap";
            this.txt_nolap.ReadOnly = true;
            this.txt_nolap.Size = new System.Drawing.Size(173, 24);
            this.txt_nolap.TabIndex = 4;
            // 
            // txt_tgl_lap
            // 
            this.txt_tgl_lap.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tgl_lap.Location = new System.Drawing.Point(172, 387);
            this.txt_tgl_lap.Name = "txt_tgl_lap";
            this.txt_tgl_lap.ReadOnly = true;
            this.txt_tgl_lap.Size = new System.Drawing.Size(173, 24);
            this.txt_tgl_lap.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 425);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 18);
            this.label2.TabIndex = 64;
            this.label2.Text = "Nomor Laporan:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 390);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 18);
            this.label1.TabIndex = 63;
            this.label1.Text = "Tanggal Laporan:";
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(12, 168);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(672, 199);
            this.dGV1.TabIndex = 62;
            this.dGV1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGV1_CellClick);
            // 
            // txt_kateg
            // 
            this.txt_kateg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kateg.Location = new System.Drawing.Point(172, 455);
            this.txt_kateg.Name = "txt_kateg";
            this.txt_kateg.ReadOnly = true;
            this.txt_kateg.Size = new System.Drawing.Size(173, 24);
            this.txt_kateg.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 458);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 18);
            this.label8.TabIndex = 85;
            this.label8.Text = "Kategori: ";
            // 
            // Lembar_Laporan_Upgrade
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(698, 563);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_kateg);
            this.Controls.Add(this.checkBox_bagian_upgrade);
            this.Controls.Add(this.cBox_bagian_upgrade);
            this.Controls.Add(this.cBox_filter_tgl);
            this.Controls.Add(this.checkBox_tanggal);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.txt_validsi);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_estimasi);
            this.Controls.Add(this.txt_alasam);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_nolap);
            this.Controls.Add(this.txt_tgl_lap);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dGV1);
            this.Name = "Lembar_Laporan_Upgrade";
            this.Text = "Lembar_Laporan_Upgrade";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Lembar_Laporan_Upgrade_FormClosing);
            this.Load += new System.EventHandler(this.Lembar_Laporan_Upgrade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox_bagian_upgrade;
        private System.Windows.Forms.ComboBox cBox_bagian_upgrade;
        private System.Windows.Forms.ComboBox cBox_filter_tgl;
        private System.Windows.Forms.CheckBox checkBox_tanggal;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.TextBox txt_validsi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_estimasi;
        private System.Windows.Forms.TextBox txt_alasam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_nolap;
        private System.Windows.Forms.TextBox txt_tgl_lap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.TextBox txt_kateg;
        private System.Windows.Forms.Label label8;
    }
}