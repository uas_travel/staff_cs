﻿namespace E_TIK_Service
{
    partial class Penambahan_Kusi_Kereta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_tambah = new System.Windows.Forms.Button();
            this.txt_jumalh = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox_Ekonomi = new System.Windows.Forms.CheckBox();
            this.checkBox_Eksekutif = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cBox_kd_keberangkatan = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cBox_kd_kereta = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_tambah
            // 
            this.btn_tambah.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tambah.Location = new System.Drawing.Point(296, 213);
            this.btn_tambah.Name = "btn_tambah";
            this.btn_tambah.Size = new System.Drawing.Size(85, 34);
            this.btn_tambah.TabIndex = 19;
            this.btn_tambah.Text = "Tambah";
            this.btn_tambah.UseVisualStyleBackColor = true;
            // 
            // txt_jumalh
            // 
            this.txt_jumalh.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jumalh.Location = new System.Drawing.Point(200, 183);
            this.txt_jumalh.Name = "txt_jumalh";
            this.txt_jumalh.Size = new System.Drawing.Size(181, 24);
            this.txt_jumalh.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "Jumlah:";
            // 
            // checkBox_Ekonomi
            // 
            this.checkBox_Ekonomi.AutoSize = true;
            this.checkBox_Ekonomi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Ekonomi.Location = new System.Drawing.Point(294, 154);
            this.checkBox_Ekonomi.Name = "checkBox_Ekonomi";
            this.checkBox_Ekonomi.Size = new System.Drawing.Size(94, 22);
            this.checkBox_Ekonomi.TabIndex = 16;
            this.checkBox_Ekonomi.Text = "Ekonomi";
            this.checkBox_Ekonomi.UseVisualStyleBackColor = true;
            // 
            // checkBox_Eksekutif
            // 
            this.checkBox_Eksekutif.AutoSize = true;
            this.checkBox_Eksekutif.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_Eksekutif.Location = new System.Drawing.Point(200, 154);
            this.checkBox_Eksekutif.Name = "checkBox_Eksekutif";
            this.checkBox_Eksekutif.Size = new System.Drawing.Size(97, 22);
            this.checkBox_Eksekutif.TabIndex = 15;
            this.checkBox_Eksekutif.Text = "Eksekutif";
            this.checkBox_Eksekutif.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 14;
            this.label3.Text = "Jenis Kursi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "Kode Keberangkatan:";
            // 
            // cBox_kd_keberangkatan
            // 
            this.cBox_kd_keberangkatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_keberangkatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_keberangkatan.FormattingEnabled = true;
            this.cBox_kd_keberangkatan.Location = new System.Drawing.Point(201, 110);
            this.cBox_kd_keberangkatan.Name = "cBox_kd_keberangkatan";
            this.cBox_kd_keberangkatan.Size = new System.Drawing.Size(180, 26);
            this.cBox_kd_keberangkatan.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kode Kereta:";
            // 
            // cBox_kd_kereta
            // 
            this.cBox_kd_kereta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_kd_kereta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_kd_kereta.FormattingEnabled = true;
            this.cBox_kd_kereta.Location = new System.Drawing.Point(201, 71);
            this.cBox_kd_kereta.Name = "cBox_kd_kereta";
            this.cBox_kd_kereta.Size = new System.Drawing.Size(180, 26);
            this.cBox_kd_kereta.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(433, 33);
            this.label5.TabIndex = 20;
            this.label5.Text = "Lembar Penambahan Lursi Kereta Api";
            // 
            // Penambahan_Kusi_Kereta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 275);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_tambah);
            this.Controls.Add(this.txt_jumalh);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox_Ekonomi);
            this.Controls.Add(this.checkBox_Eksekutif);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cBox_kd_keberangkatan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBox_kd_kereta);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Penambahan_Kusi_Kereta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Penambahan Kursi Kereta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Penambahan_Kusi_Kereta_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_tambah;
        private System.Windows.Forms.TextBox txt_jumalh;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox_Ekonomi;
        private System.Windows.Forms.CheckBox checkBox_Eksekutif;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cBox_kd_keberangkatan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBox_kd_kereta;
        private System.Windows.Forms.Label label5;
    }
}