﻿namespace E_TIK_Service
{
    partial class Master_Karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_nip = new System.Windows.Forms.TextBox();
            this.txt_nama = new System.Windows.Forms.TextBox();
            this.txt_posisi = new System.Windows.Forms.TextBox();
            this.txt_jabatan = new System.Windows.Forms.TextBox();
            this.rBtn_posisi = new System.Windows.Forms.RadioButton();
            this.rBtn_jabatan = new System.Windows.Forms.RadioButton();
            this.cBox_filter_jabatan = new System.Windows.Forms.ComboBox();
            this.cBox_filter_posisi = new System.Windows.Forms.ComboBox();
            this.btn_clr = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_baru = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_hapus = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(12, 96);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(659, 270);
            this.dGV1.TabIndex = 0;
            this.dGV1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGV1_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(258, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Karyawan";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "NIP: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(377, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Posisi:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(377, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Jabatan: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nama: ";
            // 
            // txt_nip
            // 
            this.txt_nip.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nip.Location = new System.Drawing.Point(90, 19);
            this.txt_nip.Name = "txt_nip";
            this.txt_nip.ReadOnly = true;
            this.txt_nip.Size = new System.Drawing.Size(196, 24);
            this.txt_nip.TabIndex = 0;
            // 
            // txt_nama
            // 
            this.txt_nama.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nama.Location = new System.Drawing.Point(90, 54);
            this.txt_nama.Name = "txt_nama";
            this.txt_nama.ReadOnly = true;
            this.txt_nama.Size = new System.Drawing.Size(196, 24);
            this.txt_nama.TabIndex = 1;
            // 
            // txt_posisi
            // 
            this.txt_posisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_posisi.Location = new System.Drawing.Point(457, 51);
            this.txt_posisi.Name = "txt_posisi";
            this.txt_posisi.ReadOnly = true;
            this.txt_posisi.Size = new System.Drawing.Size(196, 24);
            this.txt_posisi.TabIndex = 3;
            // 
            // txt_jabatan
            // 
            this.txt_jabatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jabatan.Location = new System.Drawing.Point(457, 16);
            this.txt_jabatan.Name = "txt_jabatan";
            this.txt_jabatan.ReadOnly = true;
            this.txt_jabatan.Size = new System.Drawing.Size(196, 24);
            this.txt_jabatan.TabIndex = 2;
            // 
            // rBtn_posisi
            // 
            this.rBtn_posisi.AutoSize = true;
            this.rBtn_posisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBtn_posisi.Location = new System.Drawing.Point(240, 22);
            this.rBtn_posisi.Name = "rBtn_posisi";
            this.rBtn_posisi.Size = new System.Drawing.Size(69, 20);
            this.rBtn_posisi.TabIndex = 2;
            this.rBtn_posisi.TabStop = true;
            this.rBtn_posisi.Text = "Posisi: ";
            this.rBtn_posisi.UseVisualStyleBackColor = true;
            this.rBtn_posisi.CheckedChanged += new System.EventHandler(this.rBtn_posisi_CheckedChanged);
            // 
            // rBtn_jabatan
            // 
            this.rBtn_jabatan.AutoSize = true;
            this.rBtn_jabatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBtn_jabatan.Location = new System.Drawing.Point(5, 22);
            this.rBtn_jabatan.Name = "rBtn_jabatan";
            this.rBtn_jabatan.Size = new System.Drawing.Size(81, 20);
            this.rBtn_jabatan.TabIndex = 0;
            this.rBtn_jabatan.TabStop = true;
            this.rBtn_jabatan.Text = "Jabatan: ";
            this.rBtn_jabatan.UseVisualStyleBackColor = true;
            this.rBtn_jabatan.CheckedChanged += new System.EventHandler(this.rBtn_jabatan_CheckedChanged);
            // 
            // cBox_filter_jabatan
            // 
            this.cBox_filter_jabatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_filter_jabatan.Enabled = false;
            this.cBox_filter_jabatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_filter_jabatan.FormattingEnabled = true;
            this.cBox_filter_jabatan.Location = new System.Drawing.Point(90, 21);
            this.cBox_filter_jabatan.Name = "cBox_filter_jabatan";
            this.cBox_filter_jabatan.Size = new System.Drawing.Size(121, 24);
            this.cBox_filter_jabatan.TabIndex = 1;
            this.cBox_filter_jabatan.SelectedIndexChanged += new System.EventHandler(this.cBox_filter_jabatan_SelectedIndexChanged);
            // 
            // cBox_filter_posisi
            // 
            this.cBox_filter_posisi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_filter_posisi.Enabled = false;
            this.cBox_filter_posisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_filter_posisi.FormattingEnabled = true;
            this.cBox_filter_posisi.Location = new System.Drawing.Point(315, 21);
            this.cBox_filter_posisi.Name = "cBox_filter_posisi";
            this.cBox_filter_posisi.Size = new System.Drawing.Size(121, 24);
            this.cBox_filter_posisi.TabIndex = 3;
            this.cBox_filter_posisi.SelectedIndexChanged += new System.EventHandler(this.cBox_filter_posisi_SelectedIndexChanged);
            // 
            // btn_clr
            // 
            this.btn_clr.Location = new System.Drawing.Point(578, 19);
            this.btn_clr.Name = "btn_clr";
            this.btn_clr.Size = new System.Drawing.Size(75, 23);
            this.btn_clr.TabIndex = 4;
            this.btn_clr.Text = "Clear";
            this.btn_clr.UseVisualStyleBackColor = true;
            this.btn_clr.Click += new System.EventHandler(this.btn_clr_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_tutup
            // 
            this.btn_tutup.Location = new System.Drawing.Point(578, 16);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 28);
            this.btn_tutup.TabIndex = 2;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_hapus);
            this.groupBox1.Controls.Add(this.btn_baru);
            this.groupBox1.Controls.Add(this.btn_tutup);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(12, 474);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(659, 53);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // btn_baru
            // 
            this.btn_baru.Location = new System.Drawing.Point(110, 16);
            this.btn_baru.Name = "btn_baru";
            this.btn_baru.Size = new System.Drawing.Size(75, 28);
            this.btn_baru.TabIndex = 1;
            this.btn_baru.Text = "Baru";
            this.btn_baru.UseVisualStyleBackColor = true;
            this.btn_baru.Click += new System.EventHandler(this.btn_baru_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_posisi);
            this.groupBox2.Controls.Add(this.txt_jabatan);
            this.groupBox2.Controls.Add(this.txt_nama);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txt_nip);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 381);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(659, 87);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_clr);
            this.groupBox3.Controls.Add(this.rBtn_posisi);
            this.groupBox3.Controls.Add(this.rBtn_jabatan);
            this.groupBox3.Controls.Add(this.cBox_filter_jabatan);
            this.groupBox3.Controls.Add(this.cBox_filter_posisi);
            this.groupBox3.Location = new System.Drawing.Point(12, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(659, 53);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // btn_hapus
            // 
            this.btn_hapus.Location = new System.Drawing.Point(202, 16);
            this.btn_hapus.Name = "btn_hapus";
            this.btn_hapus.Size = new System.Drawing.Size(75, 28);
            this.btn_hapus.TabIndex = 3;
            this.btn_hapus.Text = "Hapus";
            this.btn_hapus.UseVisualStyleBackColor = true;
            this.btn_hapus.Click += new System.EventHandler(this.btn_hapus_Click);
            // 
            // Master_Karyawan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(692, 539);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dGV1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Master_Karyawan";
            this.Text = "Master Karyawan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Master_Karyawan_FormClosing);
            this.Load += new System.EventHandler(this.Master_Karyawan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_nip;
        private System.Windows.Forms.TextBox txt_nama;
        private System.Windows.Forms.TextBox txt_posisi;
        private System.Windows.Forms.TextBox txt_jabatan;
        private System.Windows.Forms.RadioButton rBtn_posisi;
        private System.Windows.Forms.RadioButton rBtn_jabatan;
        private System.Windows.Forms.ComboBox cBox_filter_jabatan;
        private System.Windows.Forms.ComboBox cBox_filter_posisi;
        private System.Windows.Forms.Button btn_clr;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_baru;
        private System.Windows.Forms.Button btn_hapus;
    }
}