﻿namespace E_TIK_Service
{
    partial class Laporan_Absensi_Karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_no_laporan = new System.Windows.Forms.TextBox();
            this.dTP_tgl_laporan = new System.Windows.Forms.DateTimePicker();
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.btn_simpan = new System.Windows.Forms.Button();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.dTP_tgl_absen = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_operator = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(313, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Laporan Absensi Karyawan";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nomor Laporan: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(349, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tanggal Laporan: ";
            // 
            // txt_no_laporan
            // 
            this.txt_no_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_no_laporan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txt_no_laporan.Location = new System.Drawing.Point(172, 53);
            this.txt_no_laporan.Name = "txt_no_laporan";
            this.txt_no_laporan.Size = new System.Drawing.Size(145, 24);
            this.txt_no_laporan.TabIndex = 0;
            // 
            // dTP_tgl_laporan
            // 
            this.dTP_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_laporan.Location = new System.Drawing.Point(498, 51);
            this.dTP_tgl_laporan.Name = "dTP_tgl_laporan";
            this.dTP_tgl_laporan.Size = new System.Drawing.Size(200, 24);
            this.dTP_tgl_laporan.TabIndex = 2;
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(12, 153);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(740, 220);
            this.dGV1.TabIndex = 5;
            // 
            // btn_simpan
            // 
            this.btn_simpan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_simpan.Location = new System.Drawing.Point(589, 394);
            this.btn_simpan.Name = "btn_simpan";
            this.btn_simpan.Size = new System.Drawing.Size(75, 30);
            this.btn_simpan.TabIndex = 4;
            this.btn_simpan.Text = "Simpan";
            this.btn_simpan.UseVisualStyleBackColor = true;
            this.btn_simpan.Click += new System.EventHandler(this.btn_simpan_Click);
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(680, 394);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 30);
            this.btn_tutup.TabIndex = 5;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // dTP_tgl_absen
            // 
            this.dTP_tgl_absen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_absen.Location = new System.Drawing.Point(172, 123);
            this.dTP_tgl_absen.Name = "dTP_tgl_absen";
            this.dTP_tgl_absen.Size = new System.Drawing.Size(200, 24);
            this.dTP_tgl_absen.TabIndex = 3;
            this.dTP_tgl_absen.ValueChanged += new System.EventHandler(this.dTP_tgl_absen_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Tanggal Absensi: ";
            // 
            // txt_operator
            // 
            this.txt_operator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_operator.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txt_operator.Location = new System.Drawing.Point(172, 83);
            this.txt_operator.Name = "txt_operator";
            this.txt_operator.Size = new System.Drawing.Size(145, 24);
            this.txt_operator.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 18);
            this.label5.TabIndex = 10;
            this.label5.Text = "ID Operator: ";
            // 
            // Laporan_Absensi_Karyawan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(775, 435);
            this.Controls.Add(this.txt_operator);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dTP_tgl_absen);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.btn_simpan);
            this.Controls.Add(this.dGV1);
            this.Controls.Add(this.dTP_tgl_laporan);
            this.Controls.Add(this.txt_no_laporan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Laporan_Absensi_Karyawan";
            this.Text = "Laporan Absensi Karyawan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Laporan_Absensi_Karyawan_FormClosing);
            this.Load += new System.EventHandler(this.Laporan_Absensi_Karyawan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_no_laporan;
        private System.Windows.Forms.DateTimePicker dTP_tgl_laporan;
        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.Button btn_simpan;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.DateTimePicker dTP_tgl_absen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_operator;
        private System.Windows.Forms.Label label5;
    }
}