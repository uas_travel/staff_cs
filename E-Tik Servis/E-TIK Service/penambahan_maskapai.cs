﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace E_TIK_Service
{
    public partial class penambahan_maskapai : Form
    {
        static MySqlConnection con;
        static MySqlCommand com;
        static MySqlDataAdapter adap;

        static string kode_bangku_pesawat;
        static string sjumlah_bangku;
        static int jumlah_bangku;
        static int sisa_bangku;
        public penambahan_maskapai()
        {
            Constanta.Server();
            con = new MySqlConnection(Constanta.conString);
            InitializeComponent();
        }

        private void penambahan_maskapai_Load(object sender, EventArgs e)
        {
            getDataToCbbKp();
            getDataToCbbKsp();
        }

        private void penambahan_maskapai_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        private void checkBox_VIP_CheckedChanged(object sender, EventArgs e)
        {
            kode_bangku_pesawat = "VIP";
            getJmlBangku();
        }

        private void checkBox_Ekonomi_CheckedChanged(object sender, EventArgs e)
        {
            kode_bangku_pesawat = "EKN";
            getJmlBangku();
        }

        private void btn_tambah_Click(object sender, EventArgs e)
        {
            int i = int.Parse(txt_jumalh.Text);
            sisa_bangku = jumlah_bangku + i;
            updateJmlbangku();
            inputIntoTbPenambahanKursi();
        }
        #region regionComander
        void getDataToCbbKp()
        {
            string query = "SELECT * FROM tb_travel_m_pesawat";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                cBox_kd_pesawat.DataSource = dt;
                cBox_kd_pesawat.DisplayMember = "nama_pesawat";
                cBox_kd_pesawat.ValueMember = "kode_pesawat";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToCbbKsp()
        {
            string query = "SELECT * FROM tb_travel_m_sesi_pesawat";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                cBox_kd_sesi_penerbangan.DataSource = dt;
                cBox_kd_sesi_penerbangan.DisplayMember = "kode_sesi_pesawat";
                cBox_kd_sesi_penerbangan.ValueMember = "kode_sesi_pesawat";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void inputIntoTbPenambahanKursi()
        {
            string query = "INSERT INTO tb_travel_r_penambahan_kursi_pesawat (kode_penambahan_pesawat, kode_pesawat, kode_sesi_pesawat, kode_bangku_pesawat, jumlah_bangku_input, tgl_penambahan) VALUES (@1, @2, @3, @4, @5, @6)";
            com = new MySqlCommand(query, con);

            com.Parameters.AddWithValue("@1", txtbox_kodePenambahamMaskapai.Text);
            com.Parameters.AddWithValue("@2", cBox_kd_pesawat.SelectedValue);
            com.Parameters.AddWithValue("@3", cBox_kd_sesi_penerbangan.SelectedValue);
            com.Parameters.AddWithValue("@4", kode_bangku_pesawat);
            com.Parameters.AddWithValue("@5", txt_jumalh.Text);
            com.Parameters.AddWithValue("@6", txtbox_tanggalInput.Text);


            try
            {
                con.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("SUKSES");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getJmlBangku()
        {
            string query = "SELECT * FROM tb_travel_m_jml_bangku_pesawat WHERE kode_bangku_pesawat = '"+kode_bangku_pesawat+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    sjumlah_bangku = r[2].ToString();
                    jumlah_bangku = int.Parse(sjumlah_bangku);
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                con.Close();
            }
        }
        void updateJmlbangku()
        {
            string query = "UPDATE tb_travel_m_jml_bangku_pesawat SET jml_bangku_pesawat = '"+sisa_bangku+"' WHERE kode_bangku_pesawat = '"+kode_bangku_pesawat+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                adap = new MySqlDataAdapter(com);
                adap.UpdateCommand = con.CreateCommand();
                adap.UpdateCommand.CommandText = query;

                if (adap.UpdateCommand.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Berhasil");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
