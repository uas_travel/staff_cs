﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Master_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;

        public Master_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        void Persiapan()
        {
            dGV1.ColumnCount = 5;
            dGV1.Columns[0].Name = "NIP";
            dGV1.Columns[1].Name = "Kode Jabatan";
            dGV1.Columns[2].Name = "Kode Posisi";
            dGV1.Columns[3].Name = "Nama Karyawan";
            dGV1.Columns[4].Name = "Pwd";
            dGV1.Columns[4].Visible = false;

            dGV1.AllowUserToAddRows = false;
            dGV1.ReadOnly = true;
            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void Master_Karyawan_Load(object sender, EventArgs e)
        {
            Persiapan();
            LoadData();
            LoadJabatan();
            LoadPosisi();
        }
        
        #region Load Data
        void LoadData()
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_m_karyawan";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(),r[1].ToString(),r[2].ToString(),r[3].ToString(),r[4].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        bool cboxHandler;
        void LoadJabatan()
        {
            cBox_filter_jabatan.Items.Clear();

            cboxHandler = false;
            string query = "SELECT kode_jabatan, nama_jabatan FROM tb_travel_m_jabkaryawan";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_jabatan.DataSource = dt;
                    cBox_filter_jabatan.ValueMember = "kode_jabatan";
                    cBox_filter_jabatan.DisplayMember = "nama_jabatan";
                }

                cBox_filter_jabatan.Text = null;
                cboxHandler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadPosisi()
        {
            cBox_filter_posisi.Items.Clear();
            cboxHandler = false;
            string query = "SELECT kode_posisi, nama_posisi FROM tb_travel_m_posisi";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_posisi.DataSource = dt;
                    cBox_filter_posisi.ValueMember = "kode_posisi";
                    cBox_filter_posisi.DisplayMember = "nama_posisi";
                }

                cBox_filter_posisi.Text = null;
                cboxHandler = true;

                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void GetRBtn()
        {
            if (rBtn_jabatan.Checked)
            {
                LoadData();
                cBox_filter_posisi.Enabled = false;
                cBox_filter_jabatan.Enabled = true;
            }
            else if (rBtn_posisi.Checked)
            {
                LoadData();
                cBox_filter_jabatan.Enabled = false;
                cBox_filter_posisi.Enabled = true;
            }
        }
        #endregion

        #region Load Data Filter
        string LoadDataJabatan(string jabatan)
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_m_karyawan WHERE kode_jabatan = '"+ jabatan +"'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return jabatan;
        }
        string LoadDataPosisi(string posisi)
        {
            
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_m_karyawan WHERE kode_posisi = '"+ posisi +"'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString());
                }

                
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return posisi;
        }
        #endregion

        string DeletData(string nip)
        {
            string query = "DELETE FROM tb_travel_m_karyawan WHERE NIP = '"+nip+"'";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.DeleteCommand = kon.CreateCommand();
                adapter.DeleteCommand.CommandText = query;
                if (MessageBox.Show("Hapus?") == DialogResult.Yes)
                {
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("Data Terhapus!");
                    }
                }
                kon.Close();
                LoadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return nip;
        }

        string FillData(string nip, string nama, string jabatan, string posisi)
        {
            string query = "SELECT nama_jabatan FROM tb_travel_m_jabkaryawan WHERE kode_jabatan = '" + dGV1.SelectedRows[0].Cells[1].Value.ToString() + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_jabatan.Text = r[0].ToString();
                }

                //MessageBox.Show(txt_jabatan.Text);
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            txt_nip.Text = dGV1.SelectedRows[0].Cells[0].Value.ToString();
            txt_nama.Text = dGV1.SelectedRows[0].Cells[3].Value.ToString();

            string query1 = "SELECT nama_posisi FROM tb_travel_m_posisi WHERE kode_posisi = '" + dGV1.SelectedRows[0].Cells[2].Value.ToString() + "'";
            cmd = new MySqlCommand(query1, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_posisi.Text = r[0].ToString();
                }
                //MessageBox.Show(txt_posisi.Text);
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return nip;
        }

        private void rBtn_jabatan_CheckedChanged(object sender, EventArgs e)
        {
            GetRBtn();
        }

        private void rBtn_posisi_CheckedChanged(object sender, EventArgs e)
        {
            GetRBtn();
        }

        private void btn_clr_Click(object sender, EventArgs e)
        {
            rBtn_jabatan.Checked = false; rBtn_posisi.Checked = false;
            cBox_filter_posisi.Enabled = false; cBox_filter_jabatan.Enabled = false;
            LoadData();
        }

        private void cBox_filter_jabatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxHandler)
            {
                LoadDataJabatan(cBox_filter_jabatan.SelectedValue.ToString());
            }
            
        }

        private void cBox_filter_posisi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxHandler)
            {
                LoadDataPosisi(cBox_filter_posisi.SelectedValue.ToString());
            }
            
        }

        private void dGV1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FillData(txt_nip.Text, txt_nama.Text, txt_jabatan.Text, txt_posisi.Text);
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Master_Karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        private void btn_baru_Click(object sender, EventArgs e)
        {
            Tambah_Karyawan TK = new Tambah_Karyawan();
            TK.Show();
        }

        private void btn_hapus_Click(object sender, EventArgs e)
        {
            DeletData(dGV1.SelectedRows[0].Cells[0].ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
