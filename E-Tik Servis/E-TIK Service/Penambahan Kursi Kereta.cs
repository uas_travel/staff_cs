﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Penambahan_Kusi_Kereta : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;

        public Penambahan_Kusi_Kereta()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Penambahan_Kusi_Kereta_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        private void LoadKdKereta()
        {
            string query = "SELECT * FROM tb_travel_m_kereta";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_kereta.Items.Add(r[0].ToString());
                }
                kon.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadKdKeberangkatan()
        {
            string query = "SELECT * FROM tb_travel_m_keberangkatan_ka";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_keberangkatan.Items.Add(r[0].ToString());
                }
                kon.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string CheckBoxValue;

        private void GetCheckBoxValue()
        {
            CheckBoxValue = "";
            if (checkBox_Eksekutif.Checked && checkBox_Ekonomi.Checked)
            {
                CheckBoxValue = "EKN/EKS";
            }
            else if (checkBox_Ekonomi.Checked)
            {
                CheckBoxValue = "EKN";
            }
            else if (checkBox_Eksekutif.Checked)
            {
                CheckBoxValue = "EKS";
            }
        }

        private void Penambahan_Kusi_Kereta_Load(object sender, EventArgs e)
        {
            LoadKdKereta();
            LoadKdKeberangkatan();
        }

        private void btn_tambah_Click(object sender, EventArgs e)
        {
            GetCheckBoxValue();
            MessageBox.Show(CheckBoxValue);
        }

    }
}
