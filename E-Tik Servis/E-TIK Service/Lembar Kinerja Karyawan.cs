﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Lembar_Kinerja_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Lembar_Kinerja_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Lembar_Kinerja_Karyawan_Load(object sender, EventArgs e)
        {
            Persiapan();
            LoadData();
            LoadTgl();
        }

        private void Persiapan()
        {
            dGV1.ColumnCount = 6;
            dGV1.Columns[0].Name = "Nomor Laporan";
            dGV1.Columns[1].Name = "Tanggal Laporan";
            dGV1.Columns[2].Name = "NIP";
            dGV1.Columns[3].Name = "Kode Jabatan";
            dGV1.Columns[4].Name = "Tingkat Kinerja";
            dGV1.Columns[5].Name = "Valisasi Laporan";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;
            
        }

        string UbahFormatDate(string tanggal)
        {
            return tanggal = DateTime.Parse(tanggal).ToString("yyyy-MM-dd");
        }

        private void LoadData()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lapkin.no_lapkin, tb_travel_m_tgl_lapkin.tgl_lapkin, tb_travel_r_lapkin.NIP, tb_travel_r_lapkin.kode_jabatan,                                        tb_travel_r_lapkin.tingkat_kinerja, tb_travel_m_tgl_lapkin.validasi_lapkin FROM tb_travel_m_tgl_lapkin INNER JOIN tb_travel_r_lapkin ON                                      tb_travel_m_tgl_lapkin.no_lapkin = tb_travel_r_lapkin.no_lapkin";
            cmd = new MySqlCommand(query,kon);
            
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(),UbahFormatDate(r[1].ToString()),r[2].ToString(),r[3].ToString(),r[4].ToString(),r[5].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        string tgl; 
        void LoadDataTgl()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lapkin.no_lapkin, tb_travel_m_tgl_lapkin.tgl_lapkin, tb_travel_r_lapkin.NIP, tb_travel_r_lapkin.kode_jabatan,                           tb_travel_r_lapkin.tingkat_kinerja, tb_travel_m_tgl_lapkin.validasi_lapkin FROM tb_travel_m_tgl_lapkin INNER JOIN tb_travel_r_lapkin ON                                      tb_travel_m_tgl_lapkin.no_lapkin = tb_travel_r_lapkin.no_lapkin WHERE tb_travel_m_tgl_lapkin.tgl_lapkin = '"+ tgl +"'";
            cmd = new MySqlCommand(query,kon);
            
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(),UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string kinerja;
        void LoadDataKinerja()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lapkin.no_lapkin, tb_travel_m_tgl_lapkin.tgl_lapkin, tb_travel_r_lapkin.NIP, tb_travel_r_lapkin.kode_jabatan,                           tb_travel_r_lapkin.tingkat_kinerja, tb_travel_m_tgl_lapkin.validasi_lapkin FROM tb_travel_m_tgl_lapkin INNER JOIN tb_travel_r_lapkin ON                                      tb_travel_m_tgl_lapkin.no_lapkin = tb_travel_r_lapkin.no_lapkin WHERE tb_travel_r_lapkin.tingkat_kinerja = '" + kinerja + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void LoadDataKinerjaTgl()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lapkin.no_lapkin, tb_travel_m_tgl_lapkin.tgl_lapkin, tb_travel_r_lapkin.NIP, tb_travel_r_lapkin.kode_jabatan,                           tb_travel_r_lapkin.tingkat_kinerja, tb_travel_m_tgl_lapkin.validasi_lapkin FROM tb_travel_m_tgl_lapkin INNER JOIN tb_travel_r_lapkin ON                                      tb_travel_m_tgl_lapkin.no_lapkin = tb_travel_r_lapkin.no_lapkin WHERE tb_travel_m_tgl_lapkin.tgl_lapkin = '" + tgl + "' AND tb_travel_r_lapkin.tingkat_kinerja                  = '" + kinerja + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), r[4].ToString(), r[5].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void LoadTgl()
        {
            string query = "SELECT DISTINCT tgl_lapkin FROM tb_travel_m_tgl_lapkin";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_tgl.Items.Add(UbahFormatDate(r[0].ToString()));
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        void FilterData()
        {
            if (checkBox_tanggal.Checked && checkBox_kinerja.Checked)
            {
                LoadDataKinerjaTgl();
            }
            else if (checkBox_kinerja.Checked)
            {
                LoadDataKinerja();
            }
            else if (checkBox_tanggal.Checked)
            {
                LoadDataTgl();
            }

        }

        private void Lembar_Kinerja_Karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            kinerja = comboBox1.SelectedItem.ToString();
            //MessageBox.Show(kinerja);
            //LoadDataKinerja()
            FilterData();
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            comboBox1.Enabled = true; cBox_filter_tgl.Enabled = true;
            checkBox_kinerja.Checked = false; checkBox_tanggal.Checked = false;
            
            LoadData();
            
        }

        private void checkBox_tanggal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_tanggal.Checked)
            {
                
                cBox_filter_tgl.Enabled = true;
            }
            else
            {
                
                cBox_filter_tgl.Enabled = false;
                LoadData();
            }
            
        }

        private void checkBox_kinerja_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_kinerja.Checked)
            {
                comboBox1.Enabled = true;
            }
            else
            {
                comboBox1.Enabled = false;
                LoadData();
            }
            
        }

        private void cBox_filter_tgl_SelectedIndexChanged(object sender, EventArgs e)
        {

            tgl = cBox_filter_tgl.SelectedItem.ToString();
            FilterData();
        }
    }
}
