﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class main : Form
    {
     
        public main()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            login_cs goLoginCs = new login_cs();
            goLoginCs.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            login_staff goLoginStaff = new login_staff();
            goLoginStaff.Show();
            this.Hide();
        }

        private void main_Load(object sender, EventArgs e)
        {

        }
    }
}
