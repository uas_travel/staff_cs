﻿namespace E_TIK_Service
{
    partial class mdi_staff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.staffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kerjasamaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.penambahanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kursiKeretaApiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kursiKapalLautToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maskapaiPenerbanganToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teknologiInformasiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanKerusakanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pengajuanUpgradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lembarLaporanKerusakanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lembarLaporanUpgradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hRDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanKinerjaKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lembarKinerjaKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personaliaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.absensiKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanAbsensiKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lembarAbsensiKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staffToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1230, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // staffToolStripMenuItem
            // 
            this.staffToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kerjasamaToolStripMenuItem1,
            this.teknologiInformasiToolStripMenuItem1,
            this.hRDToolStripMenuItem1,
            this.personaliaToolStripMenuItem});
            this.staffToolStripMenuItem.Name = "staffToolStripMenuItem";
            this.staffToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.staffToolStripMenuItem.Text = "Staff";
            // 
            // kerjasamaToolStripMenuItem1
            // 
            this.kerjasamaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penambahanToolStripMenuItem});
            this.kerjasamaToolStripMenuItem1.Name = "kerjasamaToolStripMenuItem1";
            this.kerjasamaToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.kerjasamaToolStripMenuItem1.Text = "Kerjasama";
            // 
            // penambahanToolStripMenuItem
            // 
            this.penambahanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kursiKeretaApiToolStripMenuItem,
            this.kursiKapalLautToolStripMenuItem,
            this.maskapaiPenerbanganToolStripMenuItem});
            this.penambahanToolStripMenuItem.Name = "penambahanToolStripMenuItem";
            this.penambahanToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.penambahanToolStripMenuItem.Text = "Penambahan";
            // 
            // kursiKeretaApiToolStripMenuItem
            // 
            this.kursiKeretaApiToolStripMenuItem.Name = "kursiKeretaApiToolStripMenuItem";
            this.kursiKeretaApiToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.kursiKeretaApiToolStripMenuItem.Text = "Kursi Kereta Api";
            this.kursiKeretaApiToolStripMenuItem.Click += new System.EventHandler(this.kursiKeretaApiToolStripMenuItem_Click);
            // 
            // kursiKapalLautToolStripMenuItem
            // 
            this.kursiKapalLautToolStripMenuItem.Name = "kursiKapalLautToolStripMenuItem";
            this.kursiKapalLautToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.kursiKapalLautToolStripMenuItem.Text = "Kursi Kapal Laut";
            this.kursiKapalLautToolStripMenuItem.Click += new System.EventHandler(this.kursiKapalLautToolStripMenuItem_Click);
            // 
            // maskapaiPenerbanganToolStripMenuItem
            // 
            this.maskapaiPenerbanganToolStripMenuItem.Name = "maskapaiPenerbanganToolStripMenuItem";
            this.maskapaiPenerbanganToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.maskapaiPenerbanganToolStripMenuItem.Text = "Maskapai Penerbangan";
            this.maskapaiPenerbanganToolStripMenuItem.Click += new System.EventHandler(this.maskapaiPenerbanganToolStripMenuItem_Click);
            // 
            // teknologiInformasiToolStripMenuItem1
            // 
            this.teknologiInformasiToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanKerusakanToolStripMenuItem,
            this.pengajuanUpgradeToolStripMenuItem,
            this.lembarLaporanKerusakanToolStripMenuItem,
            this.lembarLaporanUpgradeToolStripMenuItem});
            this.teknologiInformasiToolStripMenuItem1.Name = "teknologiInformasiToolStripMenuItem1";
            this.teknologiInformasiToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.teknologiInformasiToolStripMenuItem1.Text = "Teknologi Informasi";
            // 
            // laporanKerusakanToolStripMenuItem
            // 
            this.laporanKerusakanToolStripMenuItem.Name = "laporanKerusakanToolStripMenuItem";
            this.laporanKerusakanToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.laporanKerusakanToolStripMenuItem.Text = "Laporan Kerusakan";
            this.laporanKerusakanToolStripMenuItem.Click += new System.EventHandler(this.laporanKerusakanToolStripMenuItem_Click_1);
            // 
            // pengajuanUpgradeToolStripMenuItem
            // 
            this.pengajuanUpgradeToolStripMenuItem.Name = "pengajuanUpgradeToolStripMenuItem";
            this.pengajuanUpgradeToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.pengajuanUpgradeToolStripMenuItem.Text = "Pengajuan Upgrade";
            this.pengajuanUpgradeToolStripMenuItem.Click += new System.EventHandler(this.pengajuanUpgradeToolStripMenuItem_Click_1);
            // 
            // lembarLaporanKerusakanToolStripMenuItem
            // 
            this.lembarLaporanKerusakanToolStripMenuItem.Name = "lembarLaporanKerusakanToolStripMenuItem";
            this.lembarLaporanKerusakanToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.lembarLaporanKerusakanToolStripMenuItem.Text = "Lembar Laporan Kerusakan";
            this.lembarLaporanKerusakanToolStripMenuItem.Click += new System.EventHandler(this.lembarLaporanKerusakanToolStripMenuItem_Click);
            // 
            // lembarLaporanUpgradeToolStripMenuItem
            // 
            this.lembarLaporanUpgradeToolStripMenuItem.Name = "lembarLaporanUpgradeToolStripMenuItem";
            this.lembarLaporanUpgradeToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.lembarLaporanUpgradeToolStripMenuItem.Text = "Lembar Laporan Upgrade";
            this.lembarLaporanUpgradeToolStripMenuItem.Click += new System.EventHandler(this.lembarLaporanUpgradeToolStripMenuItem_Click);
            // 
            // hRDToolStripMenuItem1
            // 
            this.hRDToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanKinerjaKaryawanToolStripMenuItem,
            this.lembarKinerjaKaryawanToolStripMenuItem,
            this.masterKaryawanToolStripMenuItem});
            this.hRDToolStripMenuItem1.Name = "hRDToolStripMenuItem1";
            this.hRDToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.hRDToolStripMenuItem1.Text = "HRD";
            // 
            // laporanKinerjaKaryawanToolStripMenuItem
            // 
            this.laporanKinerjaKaryawanToolStripMenuItem.Name = "laporanKinerjaKaryawanToolStripMenuItem";
            this.laporanKinerjaKaryawanToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.laporanKinerjaKaryawanToolStripMenuItem.Text = "Laporan Kinerja Karyawan";
            this.laporanKinerjaKaryawanToolStripMenuItem.Click += new System.EventHandler(this.laporanKinerjaKaryawanToolStripMenuItem_Click);
            // 
            // lembarKinerjaKaryawanToolStripMenuItem
            // 
            this.lembarKinerjaKaryawanToolStripMenuItem.Name = "lembarKinerjaKaryawanToolStripMenuItem";
            this.lembarKinerjaKaryawanToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.lembarKinerjaKaryawanToolStripMenuItem.Text = "Lembar Kinerja Karyawan";
            this.lembarKinerjaKaryawanToolStripMenuItem.Click += new System.EventHandler(this.lembarKinerjaKaryawanToolStripMenuItem_Click);
            // 
            // masterKaryawanToolStripMenuItem
            // 
            this.masterKaryawanToolStripMenuItem.Name = "masterKaryawanToolStripMenuItem";
            this.masterKaryawanToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.masterKaryawanToolStripMenuItem.Text = "Master Karyawan";
            this.masterKaryawanToolStripMenuItem.Click += new System.EventHandler(this.masterKaryawanToolStripMenuItem_Click);
            // 
            // personaliaToolStripMenuItem
            // 
            this.personaliaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.absensiKaryawanToolStripMenuItem,
            this.laporanAbsensiKaryawanToolStripMenuItem,
            this.lembarAbsensiKaryawanToolStripMenuItem});
            this.personaliaToolStripMenuItem.Name = "personaliaToolStripMenuItem";
            this.personaliaToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.personaliaToolStripMenuItem.Text = "Personalia";
            // 
            // absensiKaryawanToolStripMenuItem
            // 
            this.absensiKaryawanToolStripMenuItem.Name = "absensiKaryawanToolStripMenuItem";
            this.absensiKaryawanToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.absensiKaryawanToolStripMenuItem.Text = "Absensi Karyawan";
            this.absensiKaryawanToolStripMenuItem.Click += new System.EventHandler(this.absensiKaryawanToolStripMenuItem_Click);
            // 
            // laporanAbsensiKaryawanToolStripMenuItem
            // 
            this.laporanAbsensiKaryawanToolStripMenuItem.Name = "laporanAbsensiKaryawanToolStripMenuItem";
            this.laporanAbsensiKaryawanToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.laporanAbsensiKaryawanToolStripMenuItem.Text = "Laporan Absensi Karyawan";
            this.laporanAbsensiKaryawanToolStripMenuItem.Click += new System.EventHandler(this.laporanAbsensiKaryawanToolStripMenuItem_Click);
            // 
            // lembarAbsensiKaryawanToolStripMenuItem
            // 
            this.lembarAbsensiKaryawanToolStripMenuItem.Name = "lembarAbsensiKaryawanToolStripMenuItem";
            this.lembarAbsensiKaryawanToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.lembarAbsensiKaryawanToolStripMenuItem.Text = "Lembar Absensi Karyawan";
            this.lembarAbsensiKaryawanToolStripMenuItem.Click += new System.EventHandler(this.lembarAbsensiKaryawanToolStripMenuItem_Click);
            // 
            // mdi_staff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1230, 422);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "mdi_staff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form Staff";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.mdi_staff_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem staffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kerjasamaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem teknologiInformasiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hRDToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem penambahanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kursiKeretaApiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kursiKapalLautToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maskapaiPenerbanganToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pengajuanUpgradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanKerusakanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanKinerjaKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lembarKinerjaKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lembarLaporanKerusakanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lembarLaporanUpgradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personaliaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem absensiKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanAbsensiKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lembarAbsensiKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterKaryawanToolStripMenuItem;

    }
}