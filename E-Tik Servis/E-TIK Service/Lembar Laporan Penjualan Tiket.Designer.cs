﻿namespace E_TIK_Service
{
    partial class Lembar_Laporan_Penjualan_Tiket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_status_perbaikan = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_validasi = new System.Windows.Forms.TextBox();
            this.txt_nama_aset = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cBox_filter_tgl = new System.Windows.Forms.ComboBox();
            this.checkBox_tanggal = new System.Windows.Forms.CheckBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.txt_jenis_kerusakan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_selesai_perbaikan = new System.Windows.Forms.TextBox();
            this.txt_mulai_perbaikan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_kode_pembelian = new System.Windows.Forms.TextBox();
            this.txt_tgl_laporan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.cBox_vendor = new System.Windows.Forms.ComboBox();
            this.cBox_ktegori_tiket = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox_vendor = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_status_perbaikan
            // 
            this.txt_status_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_status_perbaikan.Location = new System.Drawing.Point(510, 453);
            this.txt_status_perbaikan.Name = "txt_status_perbaikan";
            this.txt_status_perbaikan.ReadOnly = true;
            this.txt_status_perbaikan.Size = new System.Drawing.Size(173, 24);
            this.txt_status_perbaikan.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(346, 417);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 18);
            this.label9.TabIndex = 95;
            this.label9.Text = "Kota Asal: ";
            // 
            // txt_validasi
            // 
            this.txt_validasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_validasi.Location = new System.Drawing.Point(874, 417);
            this.txt_validasi.Name = "txt_validasi";
            this.txt_validasi.ReadOnly = true;
            this.txt_validasi.Size = new System.Drawing.Size(173, 24);
            this.txt_validasi.TabIndex = 13;
            // 
            // txt_nama_aset
            // 
            this.txt_nama_aset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nama_aset.Location = new System.Drawing.Point(150, 454);
            this.txt_nama_aset.Name = "txt_nama_aset";
            this.txt_nama_aset.ReadOnly = true;
            this.txt_nama_aset.Size = new System.Drawing.Size(173, 24);
            this.txt_nama_aset.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(710, 420);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 18);
            this.label10.TabIndex = 92;
            this.label10.Text = "Jam Sampai: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 457);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(129, 18);
            this.label11.TabIndex = 91;
            this.label11.Text = "Kode Operator: ";
            // 
            // cBox_filter_tgl
            // 
            this.cBox_filter_tgl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_filter_tgl.Enabled = false;
            this.cBox_filter_tgl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_filter_tgl.FormattingEnabled = true;
            this.cBox_filter_tgl.Location = new System.Drawing.Point(808, 81);
            this.cBox_filter_tgl.Name = "cBox_filter_tgl";
            this.cBox_filter_tgl.Size = new System.Drawing.Size(151, 26);
            this.cBox_filter_tgl.TabIndex = 4;
            this.cBox_filter_tgl.SelectedIndexChanged += new System.EventHandler(this.cBox_filter_tgl_SelectedIndexChanged);
            // 
            // checkBox_tanggal
            // 
            this.checkBox_tanggal.AutoSize = true;
            this.checkBox_tanggal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_tanggal.Location = new System.Drawing.Point(713, 83);
            this.checkBox_tanggal.Name = "checkBox_tanggal";
            this.checkBox_tanggal.Size = new System.Drawing.Size(96, 22);
            this.checkBox_tanggal.TabIndex = 3;
            this.checkBox_tanggal.Text = "Tanggal: ";
            this.checkBox_tanggal.UseVisualStyleBackColor = true;
            this.checkBox_tanggal.CheckedChanged += new System.EventHandler(this.checkBox_tanggal_CheckedChanged);
            // 
            // btn_clear
            // 
            this.btn_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clear.Location = new System.Drawing.Point(1034, 79);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(84, 28);
            this.btn_clear.TabIndex = 5;
            this.btn_clear.Text = "Clear";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 18);
            this.label7.TabIndex = 85;
            this.label7.Text = "Lihat Berdasarkan: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(358, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(494, 37);
            this.label5.TabIndex = 84;
            this.label5.Text = "Rekap Laporan Penjualan Tiket";
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(963, 451);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(85, 31);
            this.btn_tutup.TabIndex = 14;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // txt_jenis_kerusakan
            // 
            this.txt_jenis_kerusakan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jenis_kerusakan.Location = new System.Drawing.Point(874, 379);
            this.txt_jenis_kerusakan.Name = "txt_jenis_kerusakan";
            this.txt_jenis_kerusakan.ReadOnly = true;
            this.txt_jenis_kerusakan.Size = new System.Drawing.Size(173, 24);
            this.txt_jenis_kerusakan.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(710, 382);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 18);
            this.label6.TabIndex = 80;
            this.label6.Text = "Jam Berangkat: ";
            // 
            // txt_selesai_perbaikan
            // 
            this.txt_selesai_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_selesai_perbaikan.Location = new System.Drawing.Point(510, 414);
            this.txt_selesai_perbaikan.Name = "txt_selesai_perbaikan";
            this.txt_selesai_perbaikan.ReadOnly = true;
            this.txt_selesai_perbaikan.Size = new System.Drawing.Size(173, 24);
            this.txt_selesai_perbaikan.TabIndex = 10;
            // 
            // txt_mulai_perbaikan
            // 
            this.txt_mulai_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mulai_perbaikan.Location = new System.Drawing.Point(510, 379);
            this.txt_mulai_perbaikan.Name = "txt_mulai_perbaikan";
            this.txt_mulai_perbaikan.ReadOnly = true;
            this.txt_mulai_perbaikan.Size = new System.Drawing.Size(173, 24);
            this.txt_mulai_perbaikan.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(346, 456);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 18);
            this.label3.TabIndex = 77;
            this.label3.Text = "Kota Tujuan: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(346, 382);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 18);
            this.label4.TabIndex = 76;
            this.label4.Text = "Nama Kereta: ";
            // 
            // txt_kode_pembelian
            // 
            this.txt_kode_pembelian.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_kode_pembelian.Location = new System.Drawing.Point(150, 382);
            this.txt_kode_pembelian.Name = "txt_kode_pembelian";
            this.txt_kode_pembelian.ReadOnly = true;
            this.txt_kode_pembelian.Size = new System.Drawing.Size(173, 24);
            this.txt_kode_pembelian.TabIndex = 6;
            // 
            // txt_tgl_laporan
            // 
            this.txt_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tgl_laporan.Location = new System.Drawing.Point(150, 417);
            this.txt_tgl_laporan.Name = "txt_tgl_laporan";
            this.txt_tgl_laporan.ReadOnly = true;
            this.txt_tgl_laporan.Size = new System.Drawing.Size(173, 24);
            this.txt_tgl_laporan.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 385);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 18);
            this.label2.TabIndex = 73;
            this.label2.Text = "Kode Laporan: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 420);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 18);
            this.label1.TabIndex = 72;
            this.label1.Text = "Tanggl Laporan: ";
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(16, 116);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(1106, 257);
            this.dGV1.TabIndex = 71;
            this.dGV1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGV1_CellClick);
            // 
            // cBox_vendor
            // 
            this.cBox_vendor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_vendor.Enabled = false;
            this.cBox_vendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_vendor.FormattingEnabled = true;
            this.cBox_vendor.Location = new System.Drawing.Point(481, 81);
            this.cBox_vendor.Name = "cBox_vendor";
            this.cBox_vendor.Size = new System.Drawing.Size(200, 26);
            this.cBox_vendor.TabIndex = 2;
            this.cBox_vendor.SelectedIndexChanged += new System.EventHandler(this.cBox_vendor_SelectedIndexChanged);
            // 
            // cBox_ktegori_tiket
            // 
            this.cBox_ktegori_tiket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_ktegori_tiket.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_ktegori_tiket.FormattingEnabled = true;
            this.cBox_ktegori_tiket.Items.AddRange(new object[] {
            "Kereta Api",
            "Kapal Laut",
            "Pesawat Terbang"});
            this.cBox_ktegori_tiket.Location = new System.Drawing.Point(162, 81);
            this.cBox_ktegori_tiket.Name = "cBox_ktegori_tiket";
            this.cBox_ktegori_tiket.Size = new System.Drawing.Size(171, 26);
            this.cBox_ktegori_tiket.TabIndex = 0;
            this.cBox_ktegori_tiket.SelectedIndexChanged += new System.EventHandler(this.cBox_ktegori_tiket_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 18);
            this.label12.TabIndex = 97;
            this.label12.Text = "Kategori Tiket: ";
            // 
            // checkBox_vendor
            // 
            this.checkBox_vendor.AutoSize = true;
            this.checkBox_vendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_vendor.Location = new System.Drawing.Point(375, 83);
            this.checkBox_vendor.Name = "checkBox_vendor";
            this.checkBox_vendor.Size = new System.Drawing.Size(95, 22);
            this.checkBox_vendor.TabIndex = 1;
            this.checkBox_vendor.Text = "Vendor:  ";
            this.checkBox_vendor.UseVisualStyleBackColor = true;
            this.checkBox_vendor.CheckedChanged += new System.EventHandler(this.checkBox_vendor_CheckedChanged);
            // 
            // Lembar_Laporan_Penjualan_Tiket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 517);
            this.Controls.Add(this.checkBox_vendor);
            this.Controls.Add(this.cBox_vendor);
            this.Controls.Add(this.cBox_ktegori_tiket);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txt_status_perbaikan);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_validasi);
            this.Controls.Add(this.txt_nama_aset);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cBox_filter_tgl);
            this.Controls.Add(this.checkBox_tanggal);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.txt_jenis_kerusakan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_selesai_perbaikan);
            this.Controls.Add(this.txt_mulai_perbaikan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_kode_pembelian);
            this.Controls.Add(this.txt_tgl_laporan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dGV1);
            this.Name = "Lembar_Laporan_Penjualan_Tiket";
            this.Text = "Lembar_Laporan_Penjualan_Tiket";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Lembar_Laporan_Penjualan_Tiket_FormClosing);
            this.Load += new System.EventHandler(this.Lembar_Laporan_Penjualan_Tiket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_status_perbaikan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_validasi;
        private System.Windows.Forms.TextBox txt_nama_aset;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cBox_filter_tgl;
        private System.Windows.Forms.CheckBox checkBox_tanggal;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.TextBox txt_jenis_kerusakan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_selesai_perbaikan;
        private System.Windows.Forms.TextBox txt_mulai_perbaikan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_kode_pembelian;
        private System.Windows.Forms.TextBox txt_tgl_laporan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.ComboBox cBox_vendor;
        private System.Windows.Forms.ComboBox cBox_ktegori_tiket;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBox_vendor;
    }
}