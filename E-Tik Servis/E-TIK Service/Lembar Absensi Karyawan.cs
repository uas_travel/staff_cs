﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Lembar_Absensi_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Lembar_Absensi_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        void PersiapanData()
        {
            dGV1.ColumnCount = 8;
            dGV1.Columns[0].Name = "Nomor Laporan";
            dGV1.Columns[1].Name = "Tanggal Laporan";
            dGV1.Columns[2].Name = "NIP Karyawan";
            dGV1.Columns[3].Name = "Jam Masuk";
            dGV1.Columns[3].Visible = false;
            dGV1.Columns[4].Name = "Jam Keluar";
            dGV1.Columns[4].Visible = false;
            dGV1.Columns[5].Name = "Validasi Masuk";
            dGV1.Columns[5].Visible = false;
            dGV1.Columns[6].Name = "Validasi Keluar";
            dGV1.Columns[6].Visible = false;
            dGV1.Columns[7].Name = "Validasi Laporan";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;

        }

        string UbahFormatDate(string tanggal)
        {
            return tanggal = DateTime.Parse(tanggal).ToString("yyyy-MM-dd");
        }

        void LoadData()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lapabsen.no_lap_absen, tb_travel_m_tgl_lapabsen.tgl_lap_absen, tb_travel_r_lapabsen.NIP, tb_travel_r_lapabsen.jam_msk,                               tb_travel_r_lapabsen.jam_keluar, tb_travel_r_lapabsen.validasi_msk, tb_travel_r_lapabsen.validasi_keluar,                                                                    tb_travel_m_tgl_lapabsen.validasi_lapabsen FROM  tb_travel_m_tgl_lapabsen INNER JOIN tb_travel_r_lapabsen ON tb_travel_r_lapabsen.no_lap_absen =                             tb_travel_m_tgl_lapabsen.no_lap_absen";

            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(),
                        r[4].ToString(), r[5].ToString(), r[6].ToString(), r[7].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        void LoadDataTgl()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_lapabsen.no_lap_absen, tb_travel_m_tgl_lapabsen.tgl_lap_absen, tb_travel_r_lapabsen.NIP, tb_travel_r_lapabsen.jam_msk,                               tb_travel_r_lapabsen.jam_keluar, tb_travel_r_lapabsen.validasi_msk, tb_travel_r_lapabsen.validasi_keluar,                                                                    tb_travel_m_tgl_lapabsen.validasi_lapabsen FROM  tb_travel_m_tgl_lapabsen INNER JOIN tb_travel_r_lapabsen ON tb_travel_r_lapabsen.no_lap_absen =                             tb_travel_m_tgl_lapabsen.no_lap_absen WHERE tb_travel_m_tgl_lapabsen.tgl_lap_absen = '" + tgl + "'";

            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(),
                        r[3].ToString(), r[4].ToString(), r[5].ToString(), r[6].ToString(), r[7].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void FilterData()
        {
            //if (checkBox_tanggal.Checked && checkBox_kinerja.Checked)
            //{
            //    LoadDataKinerjaTgl();
            //}
            //else if (checkBox_kinerja.Checked)
            //{
            //    LoadDataKinerja();
            //}
            //else
            if (checkBox_tanggal.Checked)
            {
                LoadDataTgl();
            }

        }

        private void Lembar_Absensi_Karyawan_Load(object sender, EventArgs e)
        {
            PersiapanData();
            LoadData();
        }

        private void dGV1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_no_laporan.Text = dGV1.SelectedRows[0].Cells[0].Value.ToString();
            txt_tgl_laporan.Text = dGV1.SelectedRows[0].Cells[1].Value.ToString().Substring(0,10);
            txt_nip.Text = dGV1.SelectedRows[0].Cells[2].Value.ToString();
            txt_jam_masuk.Text = dGV1.SelectedRows[0].Cells[3].Value.ToString();
            txt_jam_keluar.Text = dGV1.SelectedRows[0].Cells[4].Value.ToString();
            txt_validasi_masuk.Text = dGV1.SelectedRows[0].Cells[5].Value.ToString();
            txt_validasi_keluar.Text = dGV1.SelectedRows[0].Cells[6].Value.ToString();
            txt_operatro.Text = dGV1.SelectedRows[0].Cells[7].Value.ToString();

            string query = "SELECT kode_jabatan, kode_posisi, nama_karyawan FROM tb_travel_m_karyawan WHERE NIP = '"+ dGV1.SelectedRows[0].Cells[2].Value.ToString() +"'";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_kode_jabatan.Text = r[0].ToString();
                    txt_kode_posisi.Text = r[1].ToString();
                    txt_nama.Text = r[2].ToString();
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        string tgl;
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            tgl = dateTimePicker1.Value.Date.ToString("yyyy-MM-dd");
            //MessageBox.Show(tgl);
            //LoadDataTgl();
            FilterData();
        }

        private void checkBox_tanggal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_tanggal.Checked)
            {
                dateTimePicker1.Enabled = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                LoadData();
            }
        }

        private void Lembar_Absensi_Karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

    }
}
