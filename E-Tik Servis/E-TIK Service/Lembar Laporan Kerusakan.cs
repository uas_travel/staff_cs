﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Lembar_Laporan_Kerusakan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Lembar_Laporan_Kerusakan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Lembar_Laporan_Kerusakan_Load(object sender, EventArgs e)
        {
            Persiapan();
            LoadData();
            LoadTgl();
            LoadBagianRusak();
        }

        private void Persiapan()
        {
            dGV1.ColumnCount = 9;
            dGV1.Columns[0].Name = "Nomor Laporan";
            dGV1.Columns[1].Name = "Tanggal Laporan";
            dGV1.Columns[2].Name = "Kode Aset";
            dGV1.Columns[3].Name = "Kode Kategori";
            dGV1.Columns[3].Visible = false;
            dGV1.Columns[4].Name = "Mulai Peraikan";
            dGV1.Columns[5].Name = "Selesai Peraikan";
            dGV1.Columns[6].Name = "Jenis Kerusakan";
            dGV1.Columns[7].Name = "Setatus Kesluruhan";
            dGV1.Columns[8].Name = "Valisasi Pengajuan";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;
            dGV1.ReadOnly = true;

        }

        #region LoadcBox
        void LoadTgl()
        {
            cBox_filter_tgl.Items.Clear();
            string query = "SELECT tgl_laprusak FROM tb_travel_m_tgl_laprusak";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                DataTable dt = new DataTable();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_filter_tgl.Items.Add(UbahFormatDate(r[0].ToString()));
                    
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        bool cboxhendler = false;
        void LoadBagianRusak()
        {
            cboxhendler = false;
            cBox_bagian_kerusakan.Items.Clear();
            string query = "SELECT kode_kateg, nama_kateg FROM tb_travel_m_kategpengajuan";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                DataTable dt = new DataTable();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_bagian_kerusakan.ValueMember = "kode_kateg";
                    cBox_bagian_kerusakan.DisplayMember = "nama_kateg";
                    cBox_bagian_kerusakan.DataSource = dt;
                }

                cBox_bagian_kerusakan.Text = null;
                cboxhendler = true;
                
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        void FilterData()
        {
            if (checkBox_tanggal.Checked && checkBox_bagian_rusak.Checked && checkBox_jenis_rusak.Checked)
            {
                LoadDataTBJ();
            }
            else if (checkBox_tanggal.Checked && checkBox_bagian_rusak.Checked)
            {
                LoadDataTB();
            }
            else if (checkBox_tanggal.Checked && checkBox_jenis_rusak.Checked)
            {
                LoadDataTJ();
            }
            else if (checkBox_bagian_rusak.Checked && checkBox_jenis_rusak.Checked)
            {
                LoadDataBJ();
            }
            else if (checkBox_bagian_rusak.Checked)
            {
                LoadDataBagRusak();
            }
            else if (checkBox_tanggal.Checked)
            {
                LoadDataTgl();
            }
            else if (checkBox_jenis_rusak.Checked)
            {
                LoadDataJensiRusak();
            }
        }
  
        string UbahFormatDate(string tanggal)
        {
            return tanggal = DateTime.Parse(tanggal).ToString("yyyy-MM-dd");
        }

        void LoadData()
        {   
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset ";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        #region LoadDataFilter
        string tgl;
        void LoadDataTgl()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_m_tgl_laprusak.tgl_laprusak = '"+ tgl +"'";

            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        string bgrus;
        void LoadDataBagRusak()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_m_aset.kode_kateg = '"+bgrus+"'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                         UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        string jnsrsk;
        void LoadDataJensiRusak()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_r_laprusak.jenis_kerusakan = '" + jnsrsk + "'";

            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadDataTJ()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_m_tgl_laprusak.tgl_laprusak = '" + tgl + "' AND tb_travel_r_laprusak.jenis_kerusakan = '" + jnsrsk + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadDataTB()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_m_tgl_laprusak.tgl_laprusak = '" + tgl + "' AND tb_travel_m_aset.kode_kateg = '" + bgrus + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadDataBJ()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_m_aset.kode_kateg = '" + bgrus + "' AND tb_travel_r_laprusak.jenis_kerusakan = '"+ jnsrsk +"'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadDataTBJ()
        {
            dGV1.Rows.Clear();
            string query = "SELECT tb_travel_m_tgl_laprusak.no_laprusak, tb_travel_m_tgl_laprusak.tgl_laprusak, tb_travel_r_laprusak.kode_aset, tb_travel_m_aset.kode_kateg, tb_travel_r_laprusak.tgl_mulai_perbaiki, tb_travel_r_laprusak.tgl_selesai_perbaikan, tb_travel_r_laprusak.jenis_kerusakan, tb_travel_r_laprusak.status_sesudah, tb_travel_m_tgl_laprusak.validasi_laprusak FROM tb_travel_m_tgl_laprusak INNER JOIN tb_travel_r_laprusak ON tb_travel_m_tgl_laprusak.no_laprusak = tb_travel_r_laprusak.no_laprusak INNER JOIN tb_travel_m_aset ON tb_travel_m_aset.kode_aset = tb_travel_r_laprusak.kode_aset WHERE tb_travel_m_tgl_laprusak.tgl_laprusak = '" + tgl + "' AND tb_travel_m_aset.kode_kateg = '" + bgrus + "' AND tb_travel_r_laprusak.jenis_kerusakan = '"+ jnsrsk +"'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), UbahFormatDate(r[1].ToString()), r[2].ToString(), r[3].ToString(), UbahFormatDate(r[4].ToString()),
                        UbahFormatDate(r[5].ToString()), r[6].ToString(), r[7].ToString(), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        #endregion

        void FillData()
        {
            txt_no_laporan.Text = dGV1.SelectedRows[0].Cells[0].Value.ToString();
            txt_tgl_laporan.Text = dGV1.SelectedRows[0].Cells[1].Value.ToString();

            string query = "SELECT nama_aset FROM tb_travel_m_aset WHERE kode_aset = '" + dGV1.SelectedRows[0].Cells[2].Value.ToString() + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txt_nama_aset.Text = r[0].ToString();
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            txt_mulai_perbaikan.Text = dGV1.SelectedRows[0].Cells[3].Value.ToString();
            txt_selesai_perbaikan.Text = dGV1.SelectedRows[0].Cells[4].Value.ToString();
            txt_status_perbaikan.Text = dGV1.SelectedRows[0].Cells[5].Value.ToString();
            txt_jenis_kerusakan.Text = dGV1.SelectedRows[0].Cells[6].Value.ToString();
            txt_validasi.Text = dGV1.SelectedRows[0].Cells[7].Value.ToString();
        }

        private void checkBox_tanggal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_tanggal.Checked)
            {
                cBox_filter_tgl.Enabled = true;
            }
            else
            {
                cBox_filter_tgl.Enabled = false;
                LoadData();
            }
        }

        private void checkBox_bagian_rusak_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_bagian_rusak.Checked)
            {
                cBox_bagian_kerusakan.Enabled = true;
            }
            else
            {
                cBox_bagian_kerusakan.Enabled = false;
                LoadData();
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            cBox_bagian_kerusakan.Enabled = false; cBox_filter_tgl.Enabled = false; cBox_jenis_kerusakan.Enabled = false;
            checkBox_bagian_rusak.Checked = false; checkBox_tanggal.Checked = false; checkBox_jenis_rusak.Checked = false;
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dGV1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FillData();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            
        }

        private void cBox_filter_tgl_SelectedIndexChanged(object sender, EventArgs e)
        {
            tgl = cBox_filter_tgl.SelectedItem.ToString();
            //MessageBox.Show(tgl);
            FilterData();
        }

        private void cBox_bagian_kerusakan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxhendler)
            {
                bgrus = cBox_bagian_kerusakan.SelectedValue.ToString();
                FilterData();    
            }
        }

        private void checkBox_jenis_rusak_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_jenis_rusak.Checked)
            {
                cBox_jenis_kerusakan.Enabled = true;
            }
            else
            {
                cBox_jenis_kerusakan.Enabled = false;
                LoadData();
            }
        }
        
        private void cBox_jenis_kerusakan_SelectedIndexChanged(object sender, EventArgs e)
        {
            jnsrsk = cBox_jenis_kerusakan.SelectedItem.ToString();
            //MessageBox.Show(jnsrsk);
            FilterData();
        }

        private void Lembar_Laporan_Kerusakan_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}
