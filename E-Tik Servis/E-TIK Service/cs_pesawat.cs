﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class cs_pesawat : Form
    {
        static MySqlConnection con;
        static MySqlCommand com;
        static MySqlDataAdapter adap;
        static string sjml_Bangku;
        static int jml_Bangku;
        static string kode_bangku;

        #region RegionComander
        void narikDataJmlBangkuPesawat()
        {
            string query = "SELECT * FROM tb_travel_m_jml_bangku_pesawat";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    sjml_Bangku = r[2].ToString();
                    jml_Bangku = int.Parse(sjml_Bangku);
                    
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void updateJumlahBangkuPesawat()
        {
            int sisaBangku;
            int nomorKursi = int.Parse(txtbox_nomor_kursi.Text);
            sisaBangku = jml_Bangku - nomorKursi;

            string query = "UPDATE tb_travel_m_jml_bangku_pesawat set jml_bangku_pesawat = '" + sisaBangku + "' WHERE kode_bangku_pesawat = '" + kode_bangku + "'";
            com = new MySqlCommand(query, con);
            try
            {
                con.Open();


                adap = new MySqlDataAdapter(com);
                adap.UpdateCommand = con.CreateCommand();
                adap.UpdateCommand.CommandText = query;

                if (adap.UpdateCommand.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("berhasil");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void insertData()
        {
            string query = "INSERT INTO tb_travel_r_pembelian_tiket_pesawat ( kode_pembelian, nama_penumpang, kode_pesawat, kode_bangku_pesawat, nomor_kursi, kode_keberangkatan_pesawat, kode_sesi_pesawat, tanggal_pembelian, harga) VALUES (@kpembelian,@np,@kpesawat,@kbp,@nk,@kkp,@ksp,@tp,@harga)";
            com = new MySqlCommand(query, con);
            com.Parameters.AddWithValue("@kpembelian", txt_kodepembelian.Text);
            com.Parameters.AddWithValue("@np",txt_namapenumpang.Text);
            com.Parameters.AddWithValue("@kpesawat", cBox_kd_pesawat.SelectedValue.ToString());
            com.Parameters.AddWithValue("@kbp",kode_bangku);
            com.Parameters.AddWithValue("@nk",txtbox_nomor_kursi.Text);
            com.Parameters.AddWithValue("@kkp", cBox_kd_kebrangkatan.SelectedItem.ToString());
            com.Parameters.AddWithValue("@ksp",cBox_kd_sesi.SelectedItem.ToString());
            com.Parameters.AddWithValue("@tp",dTP_tgl_pembelian.Value.Date.ToString("yyyy-MM-dd"));
            com.Parameters.AddWithValue("@harga",txt_harga.Text);

            try
            {
                con.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("OK!");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);    
            }

        }
        void getDataToCbbKk()
        {
            string query = "SELECT kode_keberangkatan_pesawat FROM tb_travel_m_keberangkatan_pesawat";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_kebrangkatan.Items.Add(r[0].ToString());
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToCbbKP()
        {
            
            string query = "SELECT * FROM tb_travel_m_pesawat";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_pesawat.DataSource = dt;
                    cBox_kd_pesawat.DisplayMember = "nama_pesawat";
                    cBox_kd_pesawat.ValueMember = "kode_pesawat";
                }

                cBox_kd_pesawat.Text = null;
                
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                con.Close();
            }
        }
        void getDataToSesi()
        {
            string query = "SELECT kode_sesi_pesawat FROM tb_travel_m_sesi_pesawat";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_sesi.Items.Add(r[0].ToString());
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToBangku()
        {
            if (radioButton_Eksekutif.Checked)
            {
                kode_bangku = "EKS";
            }
            else if (radioButton_VIP.Checked)
            {
                kode_bangku = "VIP";
            }
        }
        void getDataToTxtBox()
        {
            string query = "SELECT tb_travel_m_sesi_pesawat.kode_sesi_pesawat, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan FROM tb_travel_m_sesi_pesawat INNER JOIN tb_travel_m_keberangkatan_pesawat ON tb_travel_m_sesi_pesawat.kode_keberangkatan_pesawat = tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_keberangkatan_pesawat.kode_asal = tb_travel_m_kota_asal.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_keberangkatan_pesawat.kode_tujuan = tb_travel_m_keberangkatan_pesawat.kode_tujuan ";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    if (r[1].ToString() == cBox_kd_sesi.SelectedValue.ToString())
                    {
                        txtbox_asal.Text = r[2].ToString();
                        txtbox_tujuan.Text = r[3].ToString();
                    }
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        public cs_pesawat()
        {
            Constanta.Server();
            con = new MySqlConnection(Constanta.conString);
            InitializeComponent();
        }

        private void cs_pesawat_Load(object sender, EventArgs e)
        {
            getDataToCbbKP();
            getDataToSesi();
            //getDataToTxtBox();
            getDataToCbbKk();
            narikDataJmlBangkuPesawat();
            //MessageBox.Show(sjml_Bangku);
        }

        private void cs_pesawat_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_cs.IsAnak = !mdi_cs.IsAnak;
        }

        void Clear()
        {
            txtbox_nomor_kursi.Clear(); txt_harga.Clear(); txt_kodepembelian.Clear(); txt_namapenumpang.Clear();
            txtbox_asal.Clear(); txtbox_tujuan.Clear(); textBox_berangkat.Clear(); textBox_sampai.Clear();
        }

        private void btn_beli_Click(object sender, EventArgs e)
        {
            getDataToBangku();
            //MessageBox.Show(kode_bangku + " " + cBox_kd_pesawat.SelectedValue.ToString());
            insertData();
            updateJumlahBangkuPesawat();
            Clear();
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cBox_kd_kebrangkatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan FROM tb_travel_m_keberangkatan_pesawat INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_pesawat.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_keberangkatan_pesawat.kode_tujuan = tb_travel_m_kota_tujuan.kode_tujuan WHERE tb_travel_m_keberangkatan_pesawat.kode_keberangkatan_pesawat = '"+ cBox_kd_kebrangkatan.SelectedItem.ToString().Substring(0,5)+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txtbox_asal.Text = r[1].ToString();
                    txtbox_tujuan.Text = r[2].ToString();
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cBox_kd_sesi_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT jam_berangkat, jam_keluar FROM tb_travel_m_sesi_pesawat WHERE kode_sesi_pesawat = '" + cBox_kd_sesi.SelectedItem.ToString() + "'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    textBox_berangkat.Text = r[0].ToString();
                    textBox_sampai.Text = r[1].ToString();
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
