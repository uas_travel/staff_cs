﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class cs_kl : Form
    {
        static MySqlConnection con;
        static MySqlCommand com;
        static MySqlDataAdapter adap;
        static string sjml_bangku;
        static int jml_bangku;
        static int sisa_bangku;
        static string kode_bangku;
        static bool hasil = false;
        public cs_kl()
        {
            InitializeComponent();
            Constanta.Server();
            con = new MySqlConnection(Constanta.conString);
        }
        #region regioncomander
        void inputdata()
        {
            string query = "INSERT INTO tb_travel_r_pembelian_tiket_kapal (kode_pembelian, nama_penumpang, kode_kl, kode_bangku_kl, nomor_kursi, kode_keberangkatan_kl, kode_sesi_kl, tanggal_pembelian, harga) VALUES (@a, @b, @c, @d, @e, @f, @g, @h, @i)";
            com = new MySqlCommand(query, con);
            com.Parameters.AddWithValue("@a", txt_kodepembelian.Text);
            com.Parameters.AddWithValue("@b", txt_namapenumpang.Text);
            com.Parameters.AddWithValue("@c", cBox_kd_kapal_laut.SelectedValue.ToString());
            com.Parameters.AddWithValue("@d", kode_bangku);
            com.Parameters.AddWithValue("@e", txt_bangku.Text);
            com.Parameters.AddWithValue("@f", cBox_kd_kebrangkatan.SelectedItem.ToString().Substring(0,5));
            com.Parameters.AddWithValue("@g", cBox_kd_sesi.SelectedItem.ToString().Substring(0,3));
            com.Parameters.AddWithValue("@h", dTP_tgl_pembelian.Value.Date.ToString("yyyy-MM-dd"));
            com.Parameters.AddWithValue("@i", txt_harga.Text);

            try
            {
                con.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inputed!");
                }

                con.Close();
                }
                catch (Exception ex)
                {
                MessageBox.Show(ex.Message);
                }
            }
        void getDataToCbbKdKl()
        {
            
            string query = "SELECT kode_kl, nama_kl FROM tb_travel_m_kapal_laut";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_kapal_laut.DataSource = dt;
                    cBox_kd_kapal_laut.DisplayMember = "nama_kl";
                    cBox_kd_kapal_laut.ValueMember = "kode_kl";
                }

                cBox_kd_kapal_laut.Text = null;
                
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToKS()
        { 
            string query = "SELECT kode_sesi_kl FROM tb_travel_m_sesi_kl";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_sesi.Items.Add(r[0].ToString());
                }
                

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void getDataToCbbKk()
        {
            string query = "SELECT kode_keberangkatan_kl FROM tb_travel_m_keberangkatan_kl";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_kd_kebrangkatan.Items.Add(r[0].ToString());
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void narikDataJmlKursi()
        {
            string query = "SELECT * FROM tb_travel_m_jml_bangku_kl";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    sjml_bangku = r[2].ToString();
                    jml_bangku = int.Parse(sjml_bangku);
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void updateJmlBangku()
        {
            int i = int.Parse(txt_bangku.Text);
            sisa_bangku = jml_bangku - i;
            string query = "UPDATE tb_travel_m_jml_bangku_kl SET jml_bangku_kl = '"+sisa_bangku+"' WHERE kode_bangku_kl = '"+kode_bangku+"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                adap = new MySqlDataAdapter(com);
                adap.UpdateCommand = con.CreateCommand();
                adap.UpdateCommand.CommandText = query;

                if (adap.UpdateCommand.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Bisa!");
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        void Clear()
        {
            txt_bangku.Clear(); txt_harga.Clear(); txt_kodepembelian.Clear(); txt_namapenumpang.Clear();
            txtbox_asal.Clear(); txtbox_tujuan.Clear(); textBox_berangkat.Clear(); textBox_sampai.Clear();
        }

        private void cs_kl_Load(object sender, EventArgs e)
        {
            narikDataJmlKursi();
            getDataToCbbKdKl();
            getDataToCbbKk();
            getDataToKS();
        }

        private void cs_kl_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_cs.IsAnak = !mdi_cs.IsAnak;
        }

        void getradiobtn()
        {
            if (radioButton_Ekonomi.Checked)
            {
                kode_bangku = "EKN";
            }
            else if (radioButton_Eksekutif.Checked)
            {
                kode_bangku = "EKS";
            }
        }

        private void btn_beli_Click(object sender, EventArgs e)
        {
            getradiobtn();
            //MessageBox.Show(cBox_kd_kapal_laut.SelectedValue.ToString());
            //int i = int.Parse(txt_bangku.Text);
            //sisa_bangku = jml_bangku - i;
            updateJmlBangku();
            inputdata();
            Clear();
        }

        private void radioButton_Ekonomi_CheckedChanged(object sender, EventArgs e)
        {
            
            //if (hasil != true)
            //{
            //    hasil = true;
            //    kode_bangku = "EKS";
            //    //MessageBox.Show("asdfasdf");
            //}
        }

        private void radioButton_Eksekutif_CheckedChanged(object sender, EventArgs e)
        {        
            //if (hasil == true)
            //{
            //    hasil = false;
            //    kode_bangku = "EKN";
            //    //MessageBox.Show("111111");
            //}            
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
            //MessageBox.Show(sjml_bangku);
        }

        private void cBox_kd_kebrangkatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT tb_travel_m_keberangkatan_kl.kode_keberangkatan_kl, tb_travel_m_kota_asal.nama_kota_asal, tb_travel_m_kota_tujuan.nama_tujuan FROM tb_travel_m_keberangkatan_kl INNER JOIN tb_travel_m_kota_asal ON tb_travel_m_kota_asal.kode_asal = tb_travel_m_keberangkatan_kl.kode_asal INNER JOIN tb_travel_m_kota_tujuan ON tb_travel_m_keberangkatan_kl.kode_tujuan = tb_travel_m_kota_tujuan.kode_tujuan WHERE tb_travel_m_keberangkatan_kl.kode_keberangkatan_kl = '"+ cBox_kd_kebrangkatan.SelectedItem.ToString() +"'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    txtbox_asal.Text = r[1].ToString();
                    txtbox_tujuan.Text = r[2].ToString();
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cBox_kd_sesi_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT jam_berangkat_kl, jam_keluar_kl FROM tb_travel_m_sesi_kl WHERE kode_sesi_kl = '" + cBox_kd_sesi.SelectedItem.ToString() + "'";
            com = new MySqlCommand(query, con);

            try
            {
                con.Open();

                DataTable dt = new DataTable();
                adap = new MySqlDataAdapter(com);
                adap.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    textBox_berangkat.Text = r[0].ToString();
                    textBox_sampai.Text = r[1].ToString();
                }


                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
