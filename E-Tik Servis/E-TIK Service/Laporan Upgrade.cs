﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class laporanupgrade : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;

        public laporanupgrade()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void laporanupgrade_Load(object sender, EventArgs e)
        {
            LoadKategori();
        }
        bool cboxhandler = false;
        private void LoadKategori()
        {
            cboxhandler = false;
            string query = "SELECT kode_kateg, nama_kateg FROM tb_travel_m_kategpengajuan";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_bag_upgrade.DisplayMember = "nama_kateg";
                    cBox_bag_upgrade.ValueMember = "kode_kateg";
                    cBox_bag_upgrade.DataSource = dt;
                }
                cBox_bag_upgrade.Text = null;
                cboxhandler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
           
        }

        private void cBox_bag_upgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxhandler)
            {
                //MessageBox.Show(cBox_bag_upgrade.SelectedValue.ToString());
            }
        }

        string InsertMaster(string no_laporan, string tgl, string validasi)
        {
            string query = "INSERT INTO tb_travel_m_tgl_lappengajuan (no_pengajuan, tgl_pengajuan,validasi_pengajuan) VALUES (@no_pengajuan, @tgl, @validasi)";
            cmd = new MySqlCommand(query, kon);

            cmd.Parameters.AddWithValue("@no_pengajuan", no_laporan);
            cmd.Parameters.AddWithValue("@tgl", tgl);
            cmd.Parameters.AddWithValue("@validasi", validasi);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted To Master!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            return no_laporan;
        }

        string InsertRelasi(string nolap, string kategori, string alasan, string estimasi)
        {
            string query = "INSERT INTO tb_travel_r_lappengajuan (no_pengajuan, kode_kateg, alasan, estimasi_harga) VALUES (@1,@2,@3,@4)";

            cmd = new MySqlCommand(query, kon);

            cmd.Parameters.AddWithValue("@1", nolap);
            cmd.Parameters.AddWithValue("@2", kategori);
            cmd.Parameters.AddWithValue("@3", alasan);
            cmd.Parameters.AddWithValue("@4", estimasi);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted To Relation!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            return nolap;
        }

        private void btn_simpan_Click(object sender, EventArgs e)
        {
            InsertMaster(txt_no_laporan.Text, dateTimePicker1.Value.Date.ToString("yyyy-MM-dd"), txt_nip_plapor.Text);
            InsertRelasi(txt_no_laporan.Text, cBox_bag_upgrade.SelectedValue.ToString(), txt_Alasan.Text, txt_harga.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_no_laporan_TextChanged(object sender, EventArgs e)
        {

        }

        private void laporanupgrade_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }
    }
}
