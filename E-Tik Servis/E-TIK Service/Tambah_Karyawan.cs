﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Tambah_Karyawan : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Tambah_Karyawan()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Tambah_Karyawan_Load(object sender, EventArgs e)
        {
            LoadJabatan();
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_simpan_Click(object sender, EventArgs e)
        {
            InputMasterPosisi(txt_Kode_posisi.Text,txt_nama_posisi.Text);
            InputMasterKaryawan(txt_nip.Text,jabatan,txt_Kode_posisi.Text,txt_nama.Text,txt_pwd.Text);
            //MessageBox.Show(jabatan);
        }

        bool cboxhendler = false;
        void LoadJabatan()
        {
            cboxhendler = false;
            string query = "SELECT kode_jabatan, nama_jabatan FROM tb_travel_m_jabkaryawan";
            cmd = new MySqlCommand(query,kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_jabatan.ValueMember = "kode_jabatan";
                    cBox_jabatan.DisplayMember = "nama_jabatan";
                    cBox_jabatan.DataSource = dt;
                }
                cBox_jabatan.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        void FilterPoisi()
        {
            string query = "SELECT kode_posisi, nama_posisi FROM tb_travel_m_posisi WHERE kode_posisi = '"+txt_Kode_posisi.Text+"'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        lbl_kode_posisi.Text = r[0].ToString() + " Sudah Ada!";
                        lbl_nama_posisi.Text = r[1].ToString() + " Sudah Ada!";
                        txt_nama_posisi.Enabled = false;
                    }
                }
                else
                {
                    txt_nama_posisi.Enabled = true;
                    lbl_kode_posisi.Text = txt_Kode_posisi.Text + " Poisi Tersedia";
                    lbl_nama_posisi.Text = "Poisi Tersedia";
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Peringatan!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void FilterNIP()
        {
            string query = "SELECT NIP, nama_karyawan FROM tb_travel_m_karyawan WHERE NIP = '"+txt_nip.Text+"'";
            cmd = new MySqlCommand(query,kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        lbl_nip.Text = r[0].ToString() + " Sudah Ada!";
                        lbl_nama.Text = r[1].ToString();
                        txt_nama.Enabled = false;
                    }
                }
                else
                {
                    txt_nama.Enabled = true;
                    lbl_nip.Text = txt_nip.Text + " Tersedia!";
                    lbl_nama.Text = "Nama Tersedia";

                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string InputMasterPosisi(string kode_poisi, string nama_posisi)
        {
            string query = "INSERT INTO tb_travel_m_posisi (kode_posisi, nama_posisi) VALUES (@1,@2)";
            cmd = new MySqlCommand(query, kon);
            cmd.Parameters.AddWithValue("@1",kode_poisi);
            cmd.Parameters.AddWithValue("@2",nama_posisi);
            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Posisi di Terisi!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return kode_poisi;
        }

        string InputMasterKaryawan(string nip, string jabatan, string posisi, string nama, string pwd)
        {
            string query = "INSERT INTO tb_travel_m_karyawan (NIP, kode_jabatan, kode_posisi, nama_karyawan, password) VALUES (@1,@2,@3,@4,@5)";
            cmd = new MySqlCommand(query, kon);
            cmd.Parameters.AddWithValue("@1",nip);
            cmd.Parameters.AddWithValue("@2",jabatan);
            cmd.Parameters.AddWithValue("@3",posisi);
            cmd.Parameters.AddWithValue("@4",nama);
            cmd.Parameters.AddWithValue("@5",pwd);
            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Karyawan Terdaftar!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return nip;
        }

        string jabatan;
        private void cBox_jabatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxhendler)
            {
                jabatan = cBox_jabatan.SelectedValue.ToString();
            }
        }

        private void txt_posisi_Leave(object sender, EventArgs e)
        {
            if (txt_Kode_posisi.Text != "")
            {
                FilterPoisi();
            }
            else
            { }
        }

        private void txt_nip_Leave(object sender, EventArgs e)
        {
            if (txt_nip.Text != "")
            {

                FilterNIP();
            }
            else
            { }
        }

        private void btn_baru_Click(object sender, EventArgs e)
        {
            txt_Kode_posisi.Clear(); txt_nama.Clear(); txt_nama_posisi.Clear(); lbl_nama_posisi.Text = "Nama Posisi";
            txt_nip.Clear(); txt_pwd.Clear(); lbl_kode_posisi.Text = "Kode Posisi"; lbl_nama.Text = "Nama Karyawan"; lbl_nip.Text = "NIP";
        }
    }
}
