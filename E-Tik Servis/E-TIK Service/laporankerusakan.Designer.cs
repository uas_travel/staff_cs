﻿namespace E_TIK_Service
{
    partial class laporankerusakan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_stauts_perbaikan = new System.Windows.Forms.TextBox();
            this.dTP_mulai_perbaikan = new System.Windows.Forms.DateTimePicker();
            this.dTP_selsesai_perbaikan = new System.Windows.Forms.DateTimePicker();
            this.cBox_bagian_kerusakan = new System.Windows.Forms.ComboBox();
            this.cBox_nama_aset = new System.Windows.Forms.ComboBox();
            this.cBox_jenis_kerusakan = new System.Windows.Forms.ComboBox();
            this.btn_simpan = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.dTP_tgl_laporan = new System.Windows.Forms.DateTimePicker();
            this.txt_nip_operator = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_no_laporan = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bagian Kerusakan:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama Aset:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Jenis Kerusakan:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 272);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(197, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tanggal Mulai Perbaikan:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 307);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tanggal Selesai Perbaikan:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 342);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(206, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Status Setelah di Perbaiki:";
            // 
            // txt_stauts_perbaikan
            // 
            this.txt_stauts_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_stauts_perbaikan.Location = new System.Drawing.Point(233, 339);
            this.txt_stauts_perbaikan.Name = "txt_stauts_perbaikan";
            this.txt_stauts_perbaikan.Size = new System.Drawing.Size(209, 24);
            this.txt_stauts_perbaikan.TabIndex = 9;
            // 
            // dTP_mulai_perbaikan
            // 
            this.dTP_mulai_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_mulai_perbaikan.Location = new System.Drawing.Point(233, 267);
            this.dTP_mulai_perbaikan.Name = "dTP_mulai_perbaikan";
            this.dTP_mulai_perbaikan.Size = new System.Drawing.Size(209, 24);
            this.dTP_mulai_perbaikan.TabIndex = 10;
            // 
            // dTP_selsesai_perbaikan
            // 
            this.dTP_selsesai_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_selsesai_perbaikan.Location = new System.Drawing.Point(233, 302);
            this.dTP_selsesai_perbaikan.Name = "dTP_selsesai_perbaikan";
            this.dTP_selsesai_perbaikan.Size = new System.Drawing.Size(209, 24);
            this.dTP_selsesai_perbaikan.TabIndex = 11;
            // 
            // cBox_bagian_kerusakan
            // 
            this.cBox_bagian_kerusakan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_bagian_kerusakan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_bagian_kerusakan.FormattingEnabled = true;
            this.cBox_bagian_kerusakan.Location = new System.Drawing.Point(233, 164);
            this.cBox_bagian_kerusakan.Name = "cBox_bagian_kerusakan";
            this.cBox_bagian_kerusakan.Size = new System.Drawing.Size(209, 26);
            this.cBox_bagian_kerusakan.TabIndex = 12;
            this.cBox_bagian_kerusakan.SelectedIndexChanged += new System.EventHandler(this.cBox_bagian_kerusakan_SelectedIndexChanged);
            // 
            // cBox_nama_aset
            // 
            this.cBox_nama_aset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_nama_aset.Enabled = false;
            this.cBox_nama_aset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_nama_aset.FormattingEnabled = true;
            this.cBox_nama_aset.Location = new System.Drawing.Point(233, 199);
            this.cBox_nama_aset.Name = "cBox_nama_aset";
            this.cBox_nama_aset.Size = new System.Drawing.Size(209, 26);
            this.cBox_nama_aset.TabIndex = 13;
            // 
            // cBox_jenis_kerusakan
            // 
            this.cBox_jenis_kerusakan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_jenis_kerusakan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_jenis_kerusakan.FormattingEnabled = true;
            this.cBox_jenis_kerusakan.Items.AddRange(new object[] {
            "Kecil",
            "Sedang",
            "Besar",
            "Masive"});
            this.cBox_jenis_kerusakan.Location = new System.Drawing.Point(233, 234);
            this.cBox_jenis_kerusakan.Name = "cBox_jenis_kerusakan";
            this.cBox_jenis_kerusakan.Size = new System.Drawing.Size(209, 26);
            this.cBox_jenis_kerusakan.TabIndex = 14;
            this.cBox_jenis_kerusakan.SelectedIndexChanged += new System.EventHandler(this.cBox_jenis_kerusakan_SelectedIndexChanged);
            // 
            // btn_simpan
            // 
            this.btn_simpan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_simpan.Location = new System.Drawing.Point(285, 382);
            this.btn_simpan.Name = "btn_simpan";
            this.btn_simpan.Size = new System.Drawing.Size(75, 26);
            this.btn_simpan.TabIndex = 15;
            this.btn_simpan.Text = "Simpan";
            this.btn_simpan.UseVisualStyleBackColor = true;
            this.btn_simpan.Click += new System.EventHandler(this.btn_simpan_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(96, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(315, 37);
            this.label7.TabIndex = 16;
            this.label7.Text = "Laporan Kerusakan";
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(367, 382);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 26);
            this.btn_tutup.TabIndex = 17;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // dTP_tgl_laporan
            // 
            this.dTP_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_tgl_laporan.Location = new System.Drawing.Point(233, 57);
            this.dTP_tgl_laporan.Name = "dTP_tgl_laporan";
            this.dTP_tgl_laporan.Size = new System.Drawing.Size(209, 24);
            this.dTP_tgl_laporan.TabIndex = 21;
            // 
            // txt_nip_operator
            // 
            this.txt_nip_operator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nip_operator.Location = new System.Drawing.Point(233, 129);
            this.txt_nip_operator.Name = "txt_nip_operator";
            this.txt_nip_operator.Size = new System.Drawing.Size(209, 24);
            this.txt_nip_operator.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "NIP Opeartor:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "Tanggal Laporan:";
            // 
            // txt_no_laporan
            // 
            this.txt_no_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_no_laporan.Location = new System.Drawing.Point(233, 94);
            this.txt_no_laporan.Name = "txt_no_laporan";
            this.txt_no_laporan.Size = new System.Drawing.Size(209, 24);
            this.txt_no_laporan.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(131, 18);
            this.label10.TabIndex = 22;
            this.label10.Text = "Nomor Laporan:";
            // 
            // laporankerusakan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 416);
            this.Controls.Add(this.txt_no_laporan);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dTP_tgl_laporan);
            this.Controls.Add(this.txt_nip_operator);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_simpan);
            this.Controls.Add(this.cBox_jenis_kerusakan);
            this.Controls.Add(this.cBox_nama_aset);
            this.Controls.Add(this.cBox_bagian_kerusakan);
            this.Controls.Add(this.dTP_selsesai_perbaikan);
            this.Controls.Add(this.dTP_mulai_perbaikan);
            this.Controls.Add(this.txt_stauts_perbaikan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "laporankerusakan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "laporankerusakan";
            this.Load += new System.EventHandler(this.laporankerusakan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_stauts_perbaikan;
        private System.Windows.Forms.DateTimePicker dTP_mulai_perbaikan;
        private System.Windows.Forms.DateTimePicker dTP_selsesai_perbaikan;
        private System.Windows.Forms.ComboBox cBox_bagian_kerusakan;
        private System.Windows.Forms.ComboBox cBox_nama_aset;
        private System.Windows.Forms.ComboBox cBox_jenis_kerusakan;
        private System.Windows.Forms.Button btn_simpan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.DateTimePicker dTP_tgl_laporan;
        private System.Windows.Forms.TextBox txt_nip_operator;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_no_laporan;
        private System.Windows.Forms.Label label10;
    }
}