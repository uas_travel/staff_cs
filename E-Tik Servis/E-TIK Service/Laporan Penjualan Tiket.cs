﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class Laporan_Penjualan_Tiket : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataAdapter adapter;
        public static MySqlDataReader dtread;

        public Laporan_Penjualan_Tiket()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void Laporan_Penjualan_Tiket_FormClosing(object sender, FormClosingEventArgs e)
        {
            mdi_staff.IsAnak = !mdi_staff.IsAnak;
        }

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_simpan_Click(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
            {
                vendor = cBox_vendor.SelectedValue.ToString();
                SumKursiKA(); SumBayarKA();
                //MessageBox.Show(jmljual.ToString() + " " + jmlbyr.ToString());
                InsertMasterKA(txt_no_laporan.Text, dTP_tgl_laporan.Value.Date.ToString("yyyy-MM-dd"), txt_operator.Text);
                InsertRelasiKA();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
            {
                vendor = cBox_vendor.SelectedValue.ToString();
                SumKursiKL(); SumBayarKL();
                //MessageBox.Show(jmljual.ToString() + " " + jmlbyr.ToString());
                InsertMasterKL(txt_no_laporan.Text, dTP_tgl_laporan.Value.Date.ToString("yyyy-MM-dd"), txt_operator.Text);
                InsertRelasiKL();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
            {
                vendor = cBox_vendor.SelectedValue.ToString();
                SumBayarPSW(); SumKursiPSW();
                //MessageBox.Show(jmljual.ToString() + " " + jmlbyr.ToString());
                InsertMasterPSW(txt_no_laporan.Text, dTP_tgl_laporan.Value.Date.ToString("yyyy-MM-dd"), txt_operator.Text);
                InsertRelasiPSW();
            }
        }

        private void Laporan_Penjualan_Tiket_Load(object sender, EventArgs e)
        {
            Persiapan();
        }

        void Persiapan()
        {
            dGV1.ColumnCount = 9;
            dGV1.Columns[0].Name = "Kode Pembelian";
            dGV1.Columns[1].Name = "Nama Penumpang";
            dGV1.Columns[2].Name = "Kode Kereta";
            dGV1.Columns[3].Name = "Kode Bangku";
            dGV1.Columns[4].Name = "Nomor Kursi";
            dGV1.Columns[5].Name = "Kode Keberangkatan";
            dGV1.Columns[6].Name = "Kode Sesi";
            dGV1.Columns[7].Name = "Tanggal Pemebelian";
            dGV1.Columns[8].Name = "Harga";

            dGV1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dGV1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dGV1.MultiSelect = false;
            dGV1.AllowUserToAddRows = false;
            dGV1.ReadOnly = true;
        }

        string UbahFormatDate(string tanggal)
        {
            return tanggal = DateTime.Parse(tanggal).ToString("yyyy-MM-dd");
        }

        bool cboxhendler = false;

        #region Load Data PerCS //clear
        void LoadDataCSKA()
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_r_pembelian_tiket_ka WHERE kode_kereta = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try 
	        {	
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString(),
                            r[5].ToString(), r[6].ToString(), UbahFormatDate(r[7].ToString()), r[8].ToString());
                    }   
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
	        }
	        catch (Exception ex)
	        {
                MessageBox.Show(ex.Message);
	        }
            
        }
        void LoadDataCSKL()
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_r_pembelian_tiket_kapal WHERE kode_kl = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString(),
                            r[5].ToString(), r[6].ToString(), UbahFormatDate(r[7].ToString()), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadDataCSPSW()
        {
            dGV1.Rows.Clear();
            string query = "SELECT * FROM tb_travel_r_pembelian_tiket_pesawat WHERE kode_pesawat = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        dGV1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString(),
                            r[5].ToString(), r[6].ToString(), UbahFormatDate(r[7].ToString()), r[8].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Data tidak ditemukan dikategori yang Anda cari!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Load comboBox Vendor //clear
        void LoadVendorKA()
        {
            cboxhendler = false;
            cBox_vendor.DataSource = null;
            string query = "SELECT kode_kereta, nama_kereta FROM tb_travel_m_kereta";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_vendor.ValueMember = "kode_kereta";
                    cBox_vendor.DisplayMember = "nama_kereta";
                    cBox_vendor.DataSource = dt;
                }

                cBox_vendor.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void LoadVendorKL()
        {
            cboxhendler = false;
            cBox_vendor.DataSource = null;
            string query = "SELECT kode_kl, nama_kl FROM tb_travel_m_kapal_laut";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_vendor.ValueMember = "kode_kl";
                    cBox_vendor.DisplayMember = "nama_kl";
                    cBox_vendor.DataSource = dt;
                }

                cBox_vendor.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadVendorPSW()
        {
            cboxhendler = false;
            cBox_vendor.DataSource = null;
            string query = "SELECT kode_pesawat, nama_pesawat FROM tb_travel_m_pesawat";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cBox_vendor.ValueMember = "kode_pesawat";
                    cBox_vendor.DisplayMember = "nama_pesawat";
                    cBox_vendor.DataSource = dt;
                }

                cBox_vendor.Text = null;
                cboxhendler = true;
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        int jmljual, jmlbyr;

        private void cBox_ktegori_tiket_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
            {
                cBox_vendor.Enabled = true;
                LoadVendorKA();
                dGV1.Rows.Clear();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
            {
                cBox_vendor.Enabled = true;
                LoadVendorKL();
                dGV1.Rows.Clear();
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
            {
                cBox_vendor.Enabled = true;
                LoadVendorPSW();
                dGV1.Rows.Clear();
            }
        }

        #region Insert CS KA //clear
        void SumKursiKA()
        {
            string query = "SELECT SUM(nomor_kursi) FROM tb_travel_r_pembelian_tiket_ka WHERE kode_kereta = '"+vendor+"' ";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jmljual = int.Parse(r[0].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void SumBayarKA()
        {
            string query = "SELECT SUM(harga) FROM tb_travel_r_pembelian_tiket_ka WHERE kode_kereta = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jmlbyr = int.Parse(r[0].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        string InsertMasterKA(string no, string tgl, string val)
        {
            string query = "INSERT INTO tb_travel_m_lapbeli_ka (kode_lapbeli_tiket_ka, tanggal_lapbeli_tiket_ka, validasi) VALUES (@1, @2, @3)";
            cmd = new MySqlCommand(query, kon);

            cmd.Parameters.AddWithValue("@1", no);
            cmd.Parameters.AddWithValue("@2", tgl);
            cmd.Parameters.AddWithValue("@3", val);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("OK!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return no;
        }
        void InsertRelasiKA()
        {
            string query = "INSERT INTO tb_travel_r_lapbeli_tiket_ka (kode_lapbeli_tiket_ka, kode_kereta, kode_sesi, jumlah_yang_terjual, jumlah_Pembayaran) SELECT tb_travel_m_lapbeli_ka.kode_lapbeli_tiket_ka, tb_travel_r_pembelian_tiket_ka.kode_kereta, tb_travel_r_pembelian_tiket_ka.kode_sesi, '" + jmljual + "', '" + jmlbyr + "' FROM tb_travel_m_lapbeli_ka JOIN tb_travel_r_pembelian_tiket_ka WHERE kode_kereta = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Inser CS KL //claear
        void SumKursiKL()
        {
            string query = "SELECT SUM(nomor_kursi) FROM tb_travel_r_pembelian_tiket_kapal WHERE kode_kl = '" + vendor + "' ";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jmljual = int.Parse(r[0].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void SumBayarKL()
        {
            string query = "SELECT SUM(harga) FROM tb_travel_r_pembelian_tiket_kapal WHERE kode_kl = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jmlbyr = int.Parse(r[0].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        string InsertMasterKL(string no, string tgl, string val)
        {
            string query = "INSERT INTO tb_travel_m_lapbeli_kl (kode_lapbeli_tiket_kl, tanggal_lapbeli_tiket_kl, validasi) VALUES (@1, @2, @3)";
            cmd = new MySqlCommand(query, kon);

            cmd.Parameters.AddWithValue("@1", no);
            cmd.Parameters.AddWithValue("@2", tgl);
            cmd.Parameters.AddWithValue("@3", val);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("OK!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return no;
        }
        void InsertRelasiKL()
        {
            string query = "INSERT INTO tb_travel_r_lapbeli_tiket_kl (kode_lapbeli_tiket_kl, kode_kl, kode_sesi_kl, jumlah_yang_terjual, jumlah_Pembayaran) SELECT tb_travel_m_lapbeli_kl.kode_lapbeli_tiket_kl, tb_travel_r_pembelian_tiket_kapal.kode_kl, tb_travel_r_pembelian_tiket_kapal.kode_sesi_kl, '" + jmljual + "', '" + jmlbyr + "' FROM tb_travel_m_lapbeli_kl JOIN tb_travel_r_pembelian_tiket_kapal WHERE kode_kl = '" + vendor + "' ";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Inssert CS PSW //clear
        void SumKursiPSW()
        {
            string query = "SELECT SUM(nomor_kursi) FROM tb_travel_r_pembelian_tiket_pesawat WHERE kode_pesawat = '" + vendor + "' ";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jmljual = int.Parse(r[0].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void SumBayarPSW()
        {
            string query = "SELECT SUM(harga) FROM tb_travel_r_pembelian_tiket_pesawat WHERE kode_pesawat = '" + vendor + "'";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                adapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jmlbyr = int.Parse(r[0].ToString());
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        string InsertMasterPSW(string no, string tgl, string val)
        {
            string query = "INSERT INTO tb_travel_m_lapbeli_pesawat (kode_lapbeli_tiket_pesawat, tanggal_lapbeli_tiket_pesawat, validasi) VALUES (@1, @2, @3)";
            cmd = new MySqlCommand(query, kon);

            cmd.Parameters.AddWithValue("@1", no);
            cmd.Parameters.AddWithValue("@2", tgl);
            cmd.Parameters.AddWithValue("@3", val);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("OK!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return no;
        }
        void InsertRelasiPSW()
        {
            string query = "INSERT INTO tb_travel_r_lapbeli_tiket_pesawat (kode_lapbeli_tiket_pesawat, kode_pesawat, kode_sesi_pesawat, jumlah_yang_terjual, jumlah_Pembayaran) SELECT tb_travel_m_lapbeli_pesawat.kode_lapbeli_tiket_pesawat, tb_travel_r_pembelian_tiket_pesawat.kode_pesawat, tb_travel_r_pembelian_tiket_pesawat.kode_sesi_pesawat, '" + jmljual + "', '" + jmlbyr + "' FROM tb_travel_m_lapbeli_pesawat JOIN tb_travel_r_pembelian_tiket_pesawat WHERE kode_pesawat = '" + vendor + "' ";
            cmd = new MySqlCommand(query, kon);

            try
            {
                kon.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Inserted!");
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        string vendor;
        private void cBox_vendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kereta Api")
            {
                if (cboxhendler)
                {
                    vendor = cBox_vendor.SelectedValue.ToString();
                    txt_no_laporan.Clear();
                    LoadDataCSKA();
                }
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Kapal Laut")
            {
                if (cboxhendler)
                {
                    vendor = cBox_vendor.SelectedValue.ToString();
                    txt_no_laporan.Clear();
                    LoadDataCSKL();
                }
            }
            else if (cBox_ktegori_tiket.SelectedItem.ToString() == "Pesawat Terbang")
            {
                if (cboxhendler)
                {
                    vendor = cBox_vendor.SelectedValue.ToString();
                    txt_no_laporan.Clear();
                    LoadDataCSPSW();
                }
            }
        }
    }
}
