﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace E_TIK_Service
{
    public partial class login_cs : Form
    {
        public static MySqlCommand cmd;
        public static MySqlConnection kon;
        public static MySqlDataReader dtread;

        public login_cs()
        {
            InitializeComponent();
            Constanta.Server();
            kon = new MySqlConnection(Constanta.conString);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            persiapan();
        }

        private void login_cs_Load(object sender, EventArgs e)
        {

        }

        private void persiapan()
        {
            string query = "SELECT * FROM tb_travel_m_karyawan WHERE NIP = '" + txt_user.Text + "' AND password = '" + txt_pwd.Text + "' ";
            cmd = new MySqlCommand(query, kon);
            try
            {
                kon.Open();
                dtread = cmd.ExecuteReader();
                if (dtread.HasRows)
                {
                    mdi_cs mdi_cs = new mdi_cs();
                    mdi_cs.Show();
                    this.Hide();
                    MessageBox.Show("Selamat Datan di Halaman CS.", "Informasi.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Password atau Ussername Salah!", "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                kon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Peringatan!!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            main main = new main();
            main.Show();
            this.Close();
        }
    }
}
