﻿namespace E_TIK_Service
{
    partial class Lembar_Laporan_Kerusakan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cBox_filter_tgl = new System.Windows.Forms.ComboBox();
            this.checkBox_tanggal = new System.Windows.Forms.CheckBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_tutup = new System.Windows.Forms.Button();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.txt_jenis_kerusakan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_selesai_perbaikan = new System.Windows.Forms.TextBox();
            this.txt_mulai_perbaikan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_no_laporan = new System.Windows.Forms.TextBox();
            this.txt_tgl_laporan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dGV1 = new System.Windows.Forms.DataGridView();
            this.checkBox_bagian_rusak = new System.Windows.Forms.CheckBox();
            this.cBox_bagian_kerusakan = new System.Windows.Forms.ComboBox();
            this.txt_status_perbaikan = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_validasi = new System.Windows.Forms.TextBox();
            this.txt_nama_aset = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBox_jenis_rusak = new System.Windows.Forms.CheckBox();
            this.cBox_jenis_kerusakan = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // cBox_filter_tgl
            // 
            this.cBox_filter_tgl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_filter_tgl.Enabled = false;
            this.cBox_filter_tgl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_filter_tgl.FormattingEnabled = true;
            this.cBox_filter_tgl.Location = new System.Drawing.Point(117, 81);
            this.cBox_filter_tgl.Name = "cBox_filter_tgl";
            this.cBox_filter_tgl.Size = new System.Drawing.Size(151, 26);
            this.cBox_filter_tgl.TabIndex = 1;
            this.cBox_filter_tgl.SelectedIndexChanged += new System.EventHandler(this.cBox_filter_tgl_SelectedIndexChanged);
            // 
            // checkBox_tanggal
            // 
            this.checkBox_tanggal.AutoSize = true;
            this.checkBox_tanggal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_tanggal.Location = new System.Drawing.Point(13, 83);
            this.checkBox_tanggal.Name = "checkBox_tanggal";
            this.checkBox_tanggal.Size = new System.Drawing.Size(87, 22);
            this.checkBox_tanggal.TabIndex = 0;
            this.checkBox_tanggal.Text = "Tanggal: ";
            this.checkBox_tanggal.UseVisualStyleBackColor = true;
            this.checkBox_tanggal.CheckedChanged += new System.EventHandler(this.checkBox_tanggal_CheckedChanged);
            // 
            // btn_clear
            // 
            this.btn_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clear.Location = new System.Drawing.Point(963, 79);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(84, 28);
            this.btn_clear.TabIndex = 6;
            this.btn_clear.Text = "Clear";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 18);
            this.label7.TabIndex = 54;
            this.label7.Text = "Lihat Berdasarkan: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(294, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(421, 37);
            this.label5.TabIndex = 53;
            this.label5.Text = "Rekap Laporan Kerusakan";
            // 
            // btn_tutup
            // 
            this.btn_tutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tutup.Location = new System.Drawing.Point(963, 454);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(85, 31);
            this.btn_tutup.TabIndex = 16;
            this.btn_tutup.Text = "Tutup";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refresh.Location = new System.Drawing.Point(874, 454);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(85, 31);
            this.btn_refresh.TabIndex = 15;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // txt_jenis_kerusakan
            // 
            this.txt_jenis_kerusakan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jenis_kerusakan.Location = new System.Drawing.Point(874, 382);
            this.txt_jenis_kerusakan.Name = "txt_jenis_kerusakan";
            this.txt_jenis_kerusakan.ReadOnly = true;
            this.txt_jenis_kerusakan.Size = new System.Drawing.Size(173, 24);
            this.txt_jenis_kerusakan.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(710, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 18);
            this.label6.TabIndex = 49;
            this.label6.Text = "Jenis Kerusakan: ";
            // 
            // txt_selesai_perbaikan
            // 
            this.txt_selesai_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_selesai_perbaikan.Location = new System.Drawing.Point(510, 417);
            this.txt_selesai_perbaikan.Name = "txt_selesai_perbaikan";
            this.txt_selesai_perbaikan.ReadOnly = true;
            this.txt_selesai_perbaikan.Size = new System.Drawing.Size(173, 24);
            this.txt_selesai_perbaikan.TabIndex = 11;
            // 
            // txt_mulai_perbaikan
            // 
            this.txt_mulai_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mulai_perbaikan.Location = new System.Drawing.Point(510, 382);
            this.txt_mulai_perbaikan.Name = "txt_mulai_perbaikan";
            this.txt_mulai_perbaikan.ReadOnly = true;
            this.txt_mulai_perbaikan.Size = new System.Drawing.Size(173, 24);
            this.txt_mulai_perbaikan.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(346, 423);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 18);
            this.label3.TabIndex = 46;
            this.label3.Text = "Selesai Perbaikan: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(346, 385);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 18);
            this.label4.TabIndex = 45;
            this.label4.Text = "Mulai Perbaikan: ";
            // 
            // txt_no_laporan
            // 
            this.txt_no_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_no_laporan.Location = new System.Drawing.Point(150, 385);
            this.txt_no_laporan.Name = "txt_no_laporan";
            this.txt_no_laporan.ReadOnly = true;
            this.txt_no_laporan.Size = new System.Drawing.Size(173, 24);
            this.txt_no_laporan.TabIndex = 7;
            // 
            // txt_tgl_laporan
            // 
            this.txt_tgl_laporan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tgl_laporan.Location = new System.Drawing.Point(150, 420);
            this.txt_tgl_laporan.Name = "txt_tgl_laporan";
            this.txt_tgl_laporan.ReadOnly = true;
            this.txt_tgl_laporan.Size = new System.Drawing.Size(173, 24);
            this.txt_tgl_laporan.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 388);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 18);
            this.label2.TabIndex = 42;
            this.label2.Text = "Nomor Laporan:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 423);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 18);
            this.label1.TabIndex = 41;
            this.label1.Text = "Tanggal Laporan:";
            // 
            // dGV1
            // 
            this.dGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV1.Location = new System.Drawing.Point(12, 119);
            this.dGV1.Name = "dGV1";
            this.dGV1.Size = new System.Drawing.Size(1035, 257);
            this.dGV1.TabIndex = 40;
            this.dGV1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGV1_CellClick);
            // 
            // checkBox_bagian_rusak
            // 
            this.checkBox_bagian_rusak.AutoSize = true;
            this.checkBox_bagian_rusak.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_bagian_rusak.Location = new System.Drawing.Point(299, 85);
            this.checkBox_bagian_rusak.Name = "checkBox_bagian_rusak";
            this.checkBox_bagian_rusak.Size = new System.Drawing.Size(155, 22);
            this.checkBox_bagian_rusak.TabIndex = 2;
            this.checkBox_bagian_rusak.Text = "Bagian Kerusakan: ";
            this.checkBox_bagian_rusak.UseVisualStyleBackColor = true;
            this.checkBox_bagian_rusak.CheckedChanged += new System.EventHandler(this.checkBox_bagian_rusak_CheckedChanged);
            // 
            // cBox_bagian_kerusakan
            // 
            this.cBox_bagian_kerusakan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_bagian_kerusakan.Enabled = false;
            this.cBox_bagian_kerusakan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_bagian_kerusakan.FormattingEnabled = true;
            this.cBox_bagian_kerusakan.Location = new System.Drawing.Point(458, 83);
            this.cBox_bagian_kerusakan.Name = "cBox_bagian_kerusakan";
            this.cBox_bagian_kerusakan.Size = new System.Drawing.Size(151, 26);
            this.cBox_bagian_kerusakan.TabIndex = 3;
            this.cBox_bagian_kerusakan.SelectedIndexChanged += new System.EventHandler(this.cBox_bagian_kerusakan_SelectedIndexChanged);
            // 
            // txt_status_perbaikan
            // 
            this.txt_status_perbaikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_status_perbaikan.Location = new System.Drawing.Point(510, 456);
            this.txt_status_perbaikan.Name = "txt_status_perbaikan";
            this.txt_status_perbaikan.ReadOnly = true;
            this.txt_status_perbaikan.Size = new System.Drawing.Size(173, 24);
            this.txt_status_perbaikan.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(346, 459);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(146, 18);
            this.label9.TabIndex = 66;
            this.label9.Text = "Status Perbaikan: ";
            // 
            // txt_validasi
            // 
            this.txt_validasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_validasi.Location = new System.Drawing.Point(874, 420);
            this.txt_validasi.Name = "txt_validasi";
            this.txt_validasi.ReadOnly = true;
            this.txt_validasi.Size = new System.Drawing.Size(173, 24);
            this.txt_validasi.TabIndex = 14;
            // 
            // txt_nama_aset
            // 
            this.txt_nama_aset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nama_aset.Location = new System.Drawing.Point(150, 457);
            this.txt_nama_aset.Name = "txt_nama_aset";
            this.txt_nama_aset.ReadOnly = true;
            this.txt_nama_aset.Size = new System.Drawing.Size(173, 24);
            this.txt_nama_aset.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(710, 423);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(142, 18);
            this.label10.TabIndex = 63;
            this.label10.Text = "Validasi Laporan: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 460);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 18);
            this.label11.TabIndex = 62;
            this.label11.Text = "Nama Aset: ";
            // 
            // checkBox_jenis_rusak
            // 
            this.checkBox_jenis_rusak.AutoSize = true;
            this.checkBox_jenis_rusak.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_jenis_rusak.Location = new System.Drawing.Point(636, 85);
            this.checkBox_jenis_rusak.Name = "checkBox_jenis_rusak";
            this.checkBox_jenis_rusak.Size = new System.Drawing.Size(145, 22);
            this.checkBox_jenis_rusak.TabIndex = 4;
            this.checkBox_jenis_rusak.Text = "Jenis Kerusakan: ";
            this.checkBox_jenis_rusak.UseVisualStyleBackColor = true;
            this.checkBox_jenis_rusak.CheckedChanged += new System.EventHandler(this.checkBox_jenis_rusak_CheckedChanged);
            // 
            // cBox_jenis_kerusakan
            // 
            this.cBox_jenis_kerusakan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBox_jenis_kerusakan.Enabled = false;
            this.cBox_jenis_kerusakan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBox_jenis_kerusakan.FormattingEnabled = true;
            this.cBox_jenis_kerusakan.Items.AddRange(new object[] {
            "Kecil",
            "Sedang",
            "Besar",
            "Masive"});
            this.cBox_jenis_kerusakan.Location = new System.Drawing.Point(795, 83);
            this.cBox_jenis_kerusakan.Name = "cBox_jenis_kerusakan";
            this.cBox_jenis_kerusakan.Size = new System.Drawing.Size(151, 26);
            this.cBox_jenis_kerusakan.TabIndex = 5;
            this.cBox_jenis_kerusakan.SelectedIndexChanged += new System.EventHandler(this.cBox_jenis_kerusakan_SelectedIndexChanged);
            // 
            // Lembar_Laporan_Kerusakan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1071, 529);
            this.Controls.Add(this.checkBox_jenis_rusak);
            this.Controls.Add(this.cBox_jenis_kerusakan);
            this.Controls.Add(this.txt_status_perbaikan);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_validasi);
            this.Controls.Add(this.txt_nama_aset);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.checkBox_bagian_rusak);
            this.Controls.Add(this.cBox_bagian_kerusakan);
            this.Controls.Add(this.cBox_filter_tgl);
            this.Controls.Add(this.checkBox_tanggal);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.txt_jenis_kerusakan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_selesai_perbaikan);
            this.Controls.Add(this.txt_mulai_perbaikan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_no_laporan);
            this.Controls.Add(this.txt_tgl_laporan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dGV1);
            this.Name = "Lembar_Laporan_Kerusakan";
            this.Text = "Lembar_Laporan_Kerusakan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Lembar_Laporan_Kerusakan_FormClosing);
            this.Load += new System.EventHandler(this.Lembar_Laporan_Kerusakan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cBox_filter_tgl;
        private System.Windows.Forms.CheckBox checkBox_tanggal;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_tutup;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.TextBox txt_jenis_kerusakan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_selesai_perbaikan;
        private System.Windows.Forms.TextBox txt_mulai_perbaikan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_no_laporan;
        private System.Windows.Forms.TextBox txt_tgl_laporan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dGV1;
        private System.Windows.Forms.CheckBox checkBox_bagian_rusak;
        private System.Windows.Forms.ComboBox cBox_bagian_kerusakan;
        private System.Windows.Forms.TextBox txt_status_perbaikan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_validasi;
        private System.Windows.Forms.TextBox txt_nama_aset;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBox_jenis_rusak;
        private System.Windows.Forms.ComboBox cBox_jenis_kerusakan;
    }
}