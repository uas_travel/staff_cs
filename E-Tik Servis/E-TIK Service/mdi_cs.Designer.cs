﻿namespace E_TIK_Service
{
    partial class mdi_cs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.csToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesawatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1328, 256);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 18);
            this.label2.TabIndex = 4;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.csToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(10, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1019, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // csToolStripMenuItem
            // 
            this.csToolStripMenuItem.Checked = true;
            this.csToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.csToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kAToolStripMenuItem,
            this.kLToolStripMenuItem,
            this.pesawatToolStripMenuItem});
            this.csToolStripMenuItem.Name = "csToolStripMenuItem";
            this.csToolStripMenuItem.Size = new System.Drawing.Size(117, 19);
            this.csToolStripMenuItem.Text = "Costumer Service";
            // 
            // kAToolStripMenuItem
            // 
            this.kAToolStripMenuItem.Name = "kAToolStripMenuItem";
            this.kAToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.kAToolStripMenuItem.Text = "KA";
            this.kAToolStripMenuItem.Click += new System.EventHandler(this.kAToolStripMenuItem_Click);
            // 
            // kLToolStripMenuItem
            // 
            this.kLToolStripMenuItem.Name = "kLToolStripMenuItem";
            this.kLToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.kLToolStripMenuItem.Text = "KL";
            this.kLToolStripMenuItem.Click += new System.EventHandler(this.kLToolStripMenuItem_Click);
            // 
            // pesawatToolStripMenuItem
            // 
            this.pesawatToolStripMenuItem.Name = "pesawatToolStripMenuItem";
            this.pesawatToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.pesawatToolStripMenuItem.Text = "Pesawat";
            this.pesawatToolStripMenuItem.Click += new System.EventHandler(this.pesawatToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(12, 19);
            // 
            // mdi_cs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 408);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "mdi_cs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu Costumer Service";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.mdi_cs_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem csToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesawatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

