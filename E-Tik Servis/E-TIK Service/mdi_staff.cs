﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E_TIK_Service
{
    public partial class mdi_staff : Form
    {
        public static bool IsAnak = false;

        public mdi_staff()
        {
            InitializeComponent();
        }

        private void mdi_staff_Load(object sender, EventArgs e)
        {

        }

        private void kursiKeretaApiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Penambahan_Kusi_Kereta PKR = new Penambahan_Kusi_Kereta();
                PKR.MdiParent = this;
                PKR.Show();
            }
        }

        private void kursiKapalLautToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Penambahan_Kursi_Kapal PKK = new Penambahan_Kursi_Kapal();
                PKK.MdiParent = this;
                PKK.Show();
            }
        }

        private void maskapaiPenerbanganToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                penambahan_maskapai PM = new penambahan_maskapai();
                PM.MdiParent = this;
                PM.Show();
            }
        }

        private void pengajuanUpgradeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                laporanupgrade LU = new laporanupgrade();
                LU.MdiParent = this;
                LU.Show();
            }
        }

        private void laporanKerusakanToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                laporankerusakan LK = new laporankerusakan();
                LK.MdiParent = this;
                LK.Show();
            }
        }

        private void lembarKinerjaKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Lembar_Kinerja_Karyawan LembarKK = new Lembar_Kinerja_Karyawan();
                LembarKK.MdiParent = this;
                LembarKK.Show();
            }
        }

        private void laporanKinerjaKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Laporan_Kinerja_Karyawan LaporanKK = new Laporan_Kinerja_Karyawan();
                LaporanKK.MdiParent = this;
                LaporanKK.Show();
            }
        }
        private void laporanAbsensiKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Laporan_Absensi_Karyawan LAK = new Laporan_Absensi_Karyawan();
                LAK.MdiParent = this;
                LAK.Show();
            }
        }

        private void lembarAbsensiKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Lembar_Absensi_Karyawan LAK = new Lembar_Absensi_Karyawan();
                LAK.MdiParent = this;
                LAK.Show();
            }
        }

        private void absensiKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Absensi_Karyawan AK = new Absensi_Karyawan();
                AK.MdiParent = this;
                AK.Show();
            }
        }

        private void lembarLaporanKerusakanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Lembar_Laporan_Kerusakan LLK = new Lembar_Laporan_Kerusakan();
                LLK.MdiParent = this;
                LLK.Show();
            }
            
        }

        private void lembarLaporanUpgradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Lembar_Laporan_Upgrade LLU = new Lembar_Laporan_Upgrade();
                LLU.MdiParent = this;
                LLU.Show();
            }
        }

        private void masterKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!IsAnak)
            {
                IsAnak = !IsAnak;
                Master_Karyawan MK = new Master_Karyawan();
                MK.MdiParent = this;
                MK.Show();
            }
        }
    }
}
